MoveUp = "W"
MoveDown = "S"

ScoreIncrease="D"
ScoreDecrease="A"

-- Main Menu
STATE_PLAY={posY=380,scaleX=200,scaleY=150}
STATE_HIGHSCORE={posY=300, scaleX=250,scaleY=350}
STATE_EXIT={posY=220,scaleX=200,scaleY=150}

-- Highscore
SAVE_BUTTON={posY=200,scaleX=200,scaleY=75}
RETURN_BUTTON={posY=100,scaleX=200,scaleY=75}

-- Pause
RESUME_BUTTON={posY=400,scaleX=200,scaleY=75}
QUIT_BUTTON={posY=200,scaleX=200,scaleY=75}

-- Gameover
STATE_MAIN_MENU={posY=300,scaleX=600,scaleY=600}
STATE_QUIT_GAME={posY=200,scaleX=400,scaleY=400}

--Options
CHANGE_RESOLUTION_BUTTON={posY=400,scaleX=200,scaleY=75}
OPTION_SAVE_BUTTON={posY=300,scaleX=200,scaleY=75}
OPTION_RETURN_BUTTON={posY=200,scaleX=200,scaleY=75}

function SaveResolution(outputString, overwrite)
   print("SaveToLuaFile...")
   local f;						-- The file
   if overwrite == 1 then		-- Wipe the contents with new data
      f = assert(io.open("Image/UserConfig.lua", "w"))
   elseif overwrite == 0 then	-- Append with new data
      f = assert(io.open("Image/UserConfig.lua", "a"))
   end
   -- Write to the file
   f:write(outputString)
   -- Close the file
   f:close()
   print("OK")
end

function SaveHighscore(outputString, overwrite)
   print("SaveToLuaFile...")
   local f;						-- The file
   if overwrite == 1 then		-- Wipe the contents with new data
      f = assert(io.open("Image/SP3_HighScore.lua", "w"))
   elseif overwrite == 0 then	-- Append with new data
      f = assert(io.open("Image/SP3_HighScore.lua", "a"))
   end
   -- Write to the file
   f:write(outputString)
   -- Close the file
   f:close()
   print("OK")
end