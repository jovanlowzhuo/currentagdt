function SaveToGameFile(outputString, overwrite)
   print("SaveToLuaFile...")
   local f;						-- The file
   if overwrite == 1 then		-- Wipe the contents with new data
      f = assert(io.open("Image/GameSaveData.lua", "w"))
   elseif overwrite == 0 then	-- Append with new data
      f = assert(io.open("Image/GameSaveData.lua", "a"))
   end
   -- Write to the file
   f:write(outputString)
   -- Close the file
   f:close()
   print("OK")
end

function CalculateDistanceSquare(x1,y1,z1,x2,y2,z2)
	local distanceSquare = (x2 - x1)*(x2 - x1) + (y2 - y1)*(y2 - y1) + (z2 - z1)*(z2 - z1)
	print(distanceSquare)
	return distanceSquare
end

title = "SP3 SCRIPTING"

-- Keyboard Inputs
moveForward  = "W"
moveBackward = "S"
moveLeft     = "A"
moveRight    = "D"

SaveGame = "L"
MinutesLeft = 3
SecondsLeft = 0

--Ground Entity
 GroundPos={0, -10, 0}
 GroundScale={100.0, 100.0, 100.0}
 GroundGrids={10.0, 1.0, 10.0}

--Game Objects
 Axes="reference"
 Crosshair="crosshair"

 quad={Name="quad", length=1.0, Texture="Image//calibri.tga"}
 quadColor="white"

 text={Name="text", row=16, column=16, Texture="Image//calibri.tga"}
 textMaterialRGB={r=1.0,g=0.0,b=0.0}
 
 Pistol={Name="Pistol", Obj="OBJ//pistol.obj", Texture="Image//pistol.tga"}
 weaponMaterialRGB={r=0.7,g=0.7,b=0.7}
 
 Famas={Name="Famas", Obj="OBJ//famas.obj", Texture="Image//famas.tga"}
 weaponMaterialRGB={r=0.7,g=0.7,b=0.7}
 
 Minigun={Name="Minigun", Obj="OBJ//minigun.obj", Texture="Image//minigun.tga"}
 weaponMaterialRGB={r=0.7,g=0.7,b=0.7}
 
 Shotgun={Name="Shotgun", Obj="OBJ//shotgun.obj", Texture="Image//shotgun.tga"}
 weaponMaterialRGB={r=0.7,g=0.7,b=0.7}
 
 Grenade={Name="Grenade", Obj="OBJ//grenade.obj", Texture="Image//grenade.tga"}
 weaponMaterialRGB={r=0.7,g=0.7,b=0.7}

 lightball={Name="lightball", ColorName="white", stacks=18.0, slices=36.0};
 
 FloorTile={Name="FLOOR", ColorName="white", Texture="Image//Floor.tga"}

 SkyboxFront={Name="SKYBOX_FRONT", ColorName="white", Texture="Image//SkyBox//city_ft.TGA"}
 SkyboxBack={Name="SKYBOX_BACK", ColorName="white", Texture="Image//SkyBox//city_bk.TGA"}
 SkyboxLeft={Name="SKYBOX_LEFT", ColorName="white", Texture="Image//SkyBox//city_lf.TGA"}
 SkyboxRight={Name="SKYBOX_RIGHT", ColorName="white", Texture="Image//SkyBox//city_rt.TGA"}
 SkyboxTop={Name="SKYBOX_TOP", ColorName="white", Texture="Image//SkyBox//city_up.TGA"}
 SkyboxBottom={Name="SKYBOX_BOTTOM", ColorName="white", Texture="Image//SkyBox//city_dn.TGA"}

 GridMesh={Name="GRIDMESH", ColorName="white", length=1}
 GridMeshSize={xGridSize=100, zGridSize=100, xNumofGrid=10, zNumofGrid=10}
 GridLoD={distance_High2Mid = 40000, distance_Mid2Low=160000}