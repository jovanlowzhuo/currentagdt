#include "EntityEntity.h"
#include "MeshBuilder.h"
#include "GraphicsManager.h"
#include "EntityManager.h"
#include "RenderHelper.h"

EntityEntity::EntityEntity(Mesh* _modelMesh)
	: modelMesh(_modelMesh)
{
}

EntityEntity::~EntityEntity()
{
}

void EntityEntity::Update(double _dt)
{
	// Does nothing here, can inherit & override or create your own version of this class :D
}

void EntityEntity::Render()
{
	MS& modelStack = GraphicsManager::GetInstance()->GetModelStack();
	modelStack.PushMatrix();
	modelStack.Translate(position.x, position.y, position.z);
	modelStack.Scale(scale.x, scale.y, scale.z);
	if (GetLODStatus() == true)
	{
		if (theDetailLevel != NO_DETAILS)
			RenderHelper::RenderMesh(GetLODMesh());
	}
	else
		RenderHelper::RenderMesh(modelMesh);
	modelStack.PopMatrix();
}

// Set the maxAABB and minAABB
void EntityEntity::SetAABB(Vector3 maxAABB, Vector3 minAABB)
{
	this->maxAABB = maxAABB;
	this->minAABB = minAABB;
}

EntityEntity* Create::EntityType(const std::string& _meshName, const Vector3& _position, const Vector3& _scale, const bool bAddToEntityManager)
{
	Mesh* modelMesh = MeshBuilder::GetInstance()->GetMesh(_meshName);
	if (modelMesh == nullptr)
		return nullptr;

	EntityEntity* result = new EntityEntity(modelMesh);
	result->SetPosition(_position);
	result->SetScale(_scale);
	result->SetCollider(false);
	result->EntityTypeEnum = EntityBase::ENTITY_TYPE;
	if (bAddToEntityManager)
		EntityManager::GetInstance()->AddEntity(result);
	return result;
}

EntityEntity * Create::Asset(const std::string & _meshName, const Vector3 & _position, const Vector3 & _scale, const bool bAddToEntityManager)
{
	Mesh* modelMesh = MeshBuilder::GetInstance()->GetMesh(_meshName);
	if (modelMesh == nullptr)
		return nullptr;

	EntityEntity* result = new EntityEntity(modelMesh);
	result->SetPosition(_position);
	result->SetScale(_scale);
	result->SetCollider(false);
	result->EntityTypeEnum = EntityBase::ASSET_TYPE;
	if (bAddToEntityManager)
		EntityManager::GetInstance()->AddEntity(result);
	return result;
}
