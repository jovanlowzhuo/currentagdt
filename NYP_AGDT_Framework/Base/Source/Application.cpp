#include "Application.h"
#include "MouseController.h"
#include "KeyboardController.h"
#include "SceneManager.h"
#include "GraphicsManager.h"
#include "GameStateManagement\MainMenu.h"
#include "GameStateManagement\PauseState.h"
#include "GameStateManagement\Highscore.h"
#include "GameStateManagement\GameOver.h"
#include "SceneText.h"
#include "Lua\LuaInterface.h"
#include "FPSCounter.h"
#include <GL/glew.h> //Include GLEW
#include <GLFW/glfw3.h> //Include GLFW
#include <stdio.h> //Include the standard C++ headers
#include <stdlib.h>

GLFWwindow* m_window;
const unsigned char FPS = 60; // FPS of this game
const unsigned int frameTime = 1000 / FPS; // time for each frame

//Define an error callback
static void error_callback(int error, const char* description)
{
	fputs(description, stderr);
	_fgetchar();
}

//Define the key input callback
static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);
}

void resize_callback(GLFWwindow* window, int w, int h)
{
	glViewport(0, 0, w, h);
}

bool Application::IsKeyPressed(unsigned short key)
{
    return ((GetAsyncKeyState(key) & 0x8001) != 0);
}

void Application::InitDisplay(void)
{
	currProg = GraphicsManager::GetInstance()->LoadShader("default", "Shader//GPass.vertexshader", "Shader//GPass.fragmentshader");
	currProg2 = GraphicsManager::GetInstance()->LoadShader("default", "Shader//Shadow.vertexshader", "Shader//Shadow.fragmentshader");

	// Tell the shader program to store these uniform locations
	currProg->AddUniform("MVP");
	currProg->AddUniform("MV");
	currProg->AddUniform("MV_inverse_transpose");
	currProg->AddUniform("material.kAmbient");
	currProg->AddUniform("material.kDiffuse");
	currProg->AddUniform("material.kSpecular");
	currProg->AddUniform("material.kShininess");
	currProg->AddUniform("lightEnabled");
	currProg->AddUniform("numLights");
	currProg->AddUniform("lights[0].type");
	currProg->AddUniform("lights[0].position_cameraspace");
	currProg->AddUniform("lights[0].color");
	currProg->AddUniform("lights[0].power");
	currProg->AddUniform("lights[0].kC");
	currProg->AddUniform("lights[0].kL");
	currProg->AddUniform("lights[0].kQ");
	currProg->AddUniform("lights[0].spotDirection");
	currProg->AddUniform("lights[0].cosCutoff");
	currProg->AddUniform("lights[0].cosInner");
	currProg->AddUniform("lights[0].exponent");
	currProg->AddUniform("lights[1].type");
	currProg->AddUniform("lights[1].position_cameraspace");
	currProg->AddUniform("lights[1].color");
	currProg->AddUniform("lights[1].power");
	currProg->AddUniform("lights[1].kC");
	currProg->AddUniform("lights[1].kL");
	currProg->AddUniform("lights[1].kQ");
	currProg->AddUniform("lights[1].spotDirection");
	currProg->AddUniform("lights[1].cosCutoff");
	currProg->AddUniform("lights[1].cosInner");
	currProg->AddUniform("lights[1].exponent");
	currProg->AddUniform("colorTextureEnabled");
	currProg->AddUniform("colorTexture");
	currProg->AddUniform("textEnabled");
	currProg->AddUniform("textColor");

	currProg2->AddUniform("MVP");
	currProg2->AddUniform("MV");
	currProg2->AddUniform("MV_inverse_transpose");
	currProg2->AddUniform("material.kAmbient");
	currProg2->AddUniform("material.kDiffuse");
	currProg2->AddUniform("material.kSpecular");
	currProg2->AddUniform("material.kShininess");
	currProg2->AddUniform("lightEnabled");
	currProg2->AddUniform("numLights");
	currProg2->AddUniform("lights[0].type");
	currProg2->AddUniform("lights[0].position_cameraspace");
	currProg2->AddUniform("lights[0].color");
	currProg2->AddUniform("lights[0].power");
	currProg2->AddUniform("lights[0].kC");
	currProg2->AddUniform("lights[0].kL");
	currProg2->AddUniform("lights[0].kQ");
	currProg2->AddUniform("lights[0].spotDirection");
	currProg2->AddUniform("lights[0].cosCutoff");
	currProg2->AddUniform("lights[0].cosInner");
	currProg2->AddUniform("lights[0].exponent");
	currProg2->AddUniform("lights[1].type");
	currProg2->AddUniform("lights[1].position_cameraspace");
	currProg2->AddUniform("lights[1].color");
	currProg2->AddUniform("lights[1].power");
	currProg2->AddUniform("lights[1].kC");
	currProg2->AddUniform("lights[1].kL");
	currProg2->AddUniform("lights[1].kQ");
	currProg2->AddUniform("lights[1].spotDirection");
	currProg2->AddUniform("lights[1].cosCutoff");
	currProg2->AddUniform("lights[1].cosInner");
	currProg2->AddUniform("lights[1].exponent");
	currProg2->AddUniform("colorTextureEnabled");
	currProg2->AddUniform("colorTexture");
	currProg2->AddUniform("textEnabled");
	currProg2->AddUniform("textColor");

	// Tell the graphics manager to use the shader we just loaded
	GraphicsManager::GetInstance()->SetActiveShader("default");

	currProg->UpdateInt("numLights", 1);
	currProg->UpdateInt("textEnabled", 0);
	currProg2->UpdateInt("numLights", 1);
	currProg2->UpdateInt("textEnabled", 0);
}

Application::Application()
	:m_window_width(640)
	, m_window_height(480)
	, Active(true)
{
}

Application::~Application()
{
}

void Application::Init()
{
	// Initialise the Lua system
	CLuaInterface::GetInstance()->Init();
	//Get the OpenGL resolution
	CLuaInterface::GetInstance()->SetLuaFile("Image//UserConfig.lua", CLuaInterface::GetInstance()->theLuaState);
	m_window_width = CLuaInterface::GetInstance()->getIntValue("width");
	m_window_height = CLuaInterface::GetInstance()->getIntValue("height");
	CLuaInterface::GetInstance()->SetLuaFile("Image//SP3.lua", CLuaInterface::GetInstance()->theLuaState);
	CLuaInterface::GetInstance()->Run();

	//Set the error callback
	glfwSetErrorCallback(error_callback);

	//Initialize GLFW
	if (!glfwInit())
	{
		exit(EXIT_FAILURE);
	}

	//Set the GLFW window creation hints - these are optional
	glfwWindowHint(GLFW_SAMPLES, 4); //Request 4x antialiasing
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); //Request a specific OpenGL version
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3); //Request a specific OpenGL version
	//glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); //We don't want the old OpenGL 

	//Create a window and create its OpenGL context
	m_window = glfwCreateWindow(m_window_width, m_window_height, "NYP Framework", nullptr, nullptr);

	//If the window couldn't be created
	if (!m_window)
	{
		fprintf( stderr, "Failed to open GLFW window.\n" );
		glfwTerminate();
		exit(EXIT_FAILURE);
	}
	// Set windows position
	glfwSetWindowPos(m_window, 10, 30);

	//This function makes the context of the specified window current on the calling thread. 
	glfwMakeContextCurrent(m_window);

	//Sets the key callback
	//glfwSetKeyCallback(m_window, key_callback);
	glfwSetWindowSizeCallback(m_window, resize_callback);

	glewExperimental = true; // Needed for core profile
	//Initialize GLEW
	GLenum err = glewInit();

	//If GLEW hasn't initialized
	if (err != GLEW_OK) 
	{
		fprintf(stderr, "Error: %s\n", glewGetErrorString(err));
		//return -1;
	}

	// Hide the cursor
	glfwSetInputMode(m_window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
	//glfwSetInputMode(m_window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	glfwSetMouseButtonCallback(m_window, &Application::MouseButtonCallbacks);
	glfwSetScrollCallback(m_window, &Application::MouseScrollCallbacks);

	// Init systems
	GraphicsManager::GetInstance()->Init();

	SceneManager::GetInstance()->AddScene("MainMenuState", new CMainMenuState());
	SceneManager::GetInstance()->AddScene("GameState", new SceneText());
	SceneManager::GetInstance()->AddScene("PauseState", new CPauseState());
	SceneManager::GetInstance()->AddScene("HighScoreState", new CHighScore());
	SceneManager::GetInstance()->AddScene("GameOverState", new CGameOverState());
	SceneManager::GetInstance()->SetActiveScene("MainMenuState");
}

void Application::Run()
{
	// Init the FPS Counter
	CFPSCounter::GetInstance()->Init();
	double dElaspedTime = 0.0f;


	InitDisplay();
	m_timer.startTimer(); // Start timer to calculate how long it takes to render this frame
	while ((!glfwWindowShouldClose(m_window) && (!IsKeyPressed(VK_ESCAPE))) && Active)
	{
		glfwPollEvents();
		UpdateInput();

		dElaspedTime = m_timer.getElapsedTime();
		CFPSCounter::GetInstance()->Update(dElaspedTime);
		SceneManager::GetInstance()->Update(dElaspedTime);
		SceneManager::GetInstance()->Render();
		//Swap buffers
		glfwSwapBuffers(m_window);
		//Get and organize events, like keyboard and mouse input, window resizing, etc...
        m_timer.waitUntil(frameTime); // Frame rate limiter. Limits each frame to a specified time in ms.   
		PostInputUpdate();
	}

	CLuaInterface::GetInstance()->SetLuaFile("Image//GameSaveData.lua", CLuaInterface::GetInstance()->theLuaState);
	Vector3 resetPos, resetTarget;
	int resetWeapon = 0;
	float reset = 0;
	
	resetPos.Set(-26,12,-228);
	resetTarget.Set(0, 0, 10);

	if ((resetPos - resetTarget).IsZero() == true)
	{
		resetTarget = resetPos - 10;
	}
	
	CLuaInterface::GetInstance()->WriteVector("PlayerPos", resetPos, "SaveToGameFile", true);
	CLuaInterface::GetInstance()->WriteVector("PlayerTarget", resetTarget, "SaveToGameFile", false);
	CLuaInterface::GetInstance()->WriteInt("PlayerWeapon", (int)resetWeapon, "SaveToGameFile", false);
	CLuaInterface::GetInstance()->WriteInt("MinutesLeft", (int)reset, "SaveToGameFile", false);
	CLuaInterface::GetInstance()->WriteInt("SecondsLeft", (int)reset, "SaveToGameFile", false);
}

void Application::Exit()
{
	SceneManager::GetInstance()->Exit();
	CLuaInterface::GetInstance()->DropAll();
	//Close OpenGL window and terminate GLFW
	glfwDestroyWindow(m_window);
	//Finalize and clean up GLFW
	glfwTerminate();
}

void Application::UpdateInput()
{
	// Update Mouse Position
	double mouse_currX, mouse_currY;
	glfwGetCursorPos(m_window, &mouse_currX, &mouse_currY);
	MouseController::GetInstance()->UpdateMousePosition(mouse_currX, mouse_currY);
	// Update Keyboard Input
	for (int i = 0; i < KeyboardController::MAX_KEYS; ++i)
		KeyboardController::GetInstance()->UpdateKeyboardStatus(i, IsKeyPressed(i));
}

void Application::PostInputUpdate()
{
	// If mouse is centered, need to update the center position for next frame
	if (MouseController::GetInstance()->GetKeepMouseCentered())
	{
		double mouse_currX, mouse_currY;
		mouse_currX = m_window_width >> 1;
		mouse_currY = m_window_height >> 1;
		MouseController::GetInstance()->UpdateMousePosition(mouse_currX, mouse_currY);
		glfwSetCursorPos(m_window, mouse_currX, mouse_currY);
	}
	// Call input systems to update at end of frame
	MouseController::GetInstance()->EndFrameUpdate();
	KeyboardController::GetInstance()->EndFrameUpdate();
}

void Application::MouseButtonCallbacks(GLFWwindow* window, int button, int action, int mods)
{
	// Send the callback to the mouse controller to handle
	if (action == GLFW_PRESS)
		MouseController::GetInstance()->UpdateMouseButtonPressed(button);
	else
		MouseController::GetInstance()->UpdateMouseButtonReleased(button);
}

void Application::MouseScrollCallbacks(GLFWwindow* window, double xoffset, double yoffset)
{
	MouseController::GetInstance()->UpdateMouseScroll(xoffset, yoffset);
}

int Application::GetWindowHeight()
{
	return m_window_height;
}

int Application::GetWindowWidth()
{
	return m_window_width;
}

void Application::SetWindowHeight(const int & newWindowHeight)
{
	m_window_height = newWindowHeight;
	glfwSetWindowSize(m_window, m_window_width, m_window_height);
}

void Application::SetWindowWidth(const int & newWindowWidth)
{
	m_window_width = newWindowWidth;
	glfwSetWindowSize(m_window, m_window_width, m_window_height);
}

void Application::SetWindowSize(const int & newWindowWidth, const int & newWindowHeight)
{
	m_window_width = newWindowWidth;
	m_window_height = newWindowHeight;
	glfwSetWindowSize(m_window, m_window_width, m_window_height);
}

void Application::KillGame()
{
	Active = false;
}