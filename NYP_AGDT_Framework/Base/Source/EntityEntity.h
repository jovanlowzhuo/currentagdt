#ifndef ENTITY_ENTITY_H
#define ENTITY_ENTITY_H

#include "EntityBase.h"
#include <string>
#include "Collider/Collider.h"
#include "LevelOfDetail/LevelOfDetail.h"

class Mesh;

class EntityEntity : public EntityBase, public CCollider, public CLevelOfDetails
{
public:
	EntityEntity(Mesh* _modelMesh);
	virtual ~EntityEntity();

	virtual void Update(double _dt);
	virtual void Render();

	// Set the maxAABB and minAABB
	void SetAABB(Vector3 maxAABB, Vector3 minAABB);

private:
	Mesh* modelMesh;
};

namespace Create
{
	EntityEntity* EntityType(const std::string& _meshName,
							 const Vector3& _position,
							 const Vector3& _scale,
							 const bool bAddToEntityManager);
	EntityEntity* Asset(const std::string& _meshName,
						const Vector3& _position,
						const Vector3& _scale,
						const bool bAddToEntityManager);
};
#endif // ENTITY_ENTITY_H