#include "Controller.h"
#include <iostream>
using namespace std;
const bool _CONTROLLER_DEBUG = false;

CController::CController()
	: thePlayerInfo(NULL)
{
}

CController::~CController()
{
	// We just set thePlayerInfo to NULL without deleting. SceneText will delete this.
	if (thePlayerInfo)
		thePlayerInfo = NULL;
}

// Create this controller
bool CController::Create(CPlayerInfo* thePlayerInfo)
{
	if (_CONTROLLER_DEBUG)
	{
	}
	this->thePlayerInfo = thePlayerInfo;
	return false;
}

// Read from the controller
int CController::Read(const double deltaTime)
{
	if (_CONTROLLER_DEBUG)
	{
	}
	return 0;
}

// Detect and process front / back movement on the controller
bool CController::Move_FrontBack(const double deltaTime, const bool direction, const float speedMultiplier)
{
	if (_CONTROLLER_DEBUG)
	{
	}
	if (thePlayerInfo)
	{
		thePlayerInfo->Move_FrontBack(deltaTime, direction, speedMultiplier);
	}
	return false;
}

// Detect and process left / right movement on the controller
bool CController::Move_LeftRight(const double deltaTime, const bool direction)
{
	if (_CONTROLLER_DEBUG)
	{
	}
	if (thePlayerInfo)
	{
		thePlayerInfo->Move_LeftRight(deltaTime, direction);
	}
	return false;
}

// Detect and process look up / down on the controller
bool CController::Look_UpDown(const double deltaTime, const bool direction, const float speedMultiplier)
{
	if (_CONTROLLER_DEBUG)
	{
	}
	if (thePlayerInfo)
	{
		thePlayerInfo->Look_UpDown(deltaTime, direction, speedMultiplier);
	}
	return false;
}

// Detect and process look left / right on the controller
bool CController::Look_LeftRight(const double deltaTime, const bool direction, const float speedMultiplier)
{
	if (_CONTROLLER_DEBUG)
	{
	}
	if (thePlayerInfo)
	{
		thePlayerInfo->Look_LeftRight(deltaTime, direction, speedMultiplier);
	}
	return false;
}

bool CController::Roll_Left(const double deltaTime, const bool direction, const float speedMultiplier)
{
	if (_CONTROLLER_DEBUG)
	{
	}
	if (thePlayerInfo)
	{
		thePlayerInfo->Look_LeftRight(deltaTime, direction, speedMultiplier);
	}
	return false;
}

// Stop sway
bool CController::StopSway(const double deltaTime)
{
	if (_CONTROLLER_DEBUG)
	{
	}
	if (thePlayerInfo)
	{
		thePlayerInfo->StopSway(deltaTime);
	}
	return false;
}

// Jump
bool CController::Jump(const double deltaTime)
{
	if (_CONTROLLER_DEBUG)
	{
	}
	if (thePlayerInfo)
	{
		thePlayerInfo->SetToJumpUpwards(true);
	}
	return false;
}

// Reload current weapon
bool CController::Reload(const double deltaTime)
{
	if (_CONTROLLER_DEBUG)
	{
	}
	if (thePlayerInfo)
	{
		thePlayerInfo->ReloadWeapon();
	}
	return false;
}

// Change current weapon (for primary only)
bool CController::Change(const double deltaTime)
{
	if (_CONTROLLER_DEBUG)
	{
	}
	if (thePlayerInfo)
	{
		thePlayerInfo->ChangeWeapon();
	}
	return false;
}

// Fire primary weapon
bool CController::FirePrimary(const double deltaTime)
{
	if (_CONTROLLER_DEBUG)
	{
	}
	if (thePlayerInfo)
	{
		thePlayerInfo->DischargePrimaryWeapon(deltaTime);
	}
	return false;
}

// Fire secondary weapon
bool CController::FireSecondary(const double deltaTime)
{
	if (_CONTROLLER_DEBUG)
	{
	}
	if (thePlayerInfo)
	{
		thePlayerInfo->DischargeSecondaryWeapon(deltaTime);
	}
	return false;
}

bool CController::Crouch(const double deltaTime)
{
	if (_CONTROLLER_DEBUG)
	{
	}
	if (thePlayerInfo)
	{
		thePlayerInfo->Crouch(deltaTime);
	}
	return false;
}

bool CController::SlowMode(const double deltaTime)
{
	if (_CONTROLLER_DEBUG)
	{
	}
	if (thePlayerInfo)
	{
		thePlayerInfo->SlowMode(deltaTime);
	}
	return false;
}

bool CController::UpdateLoop(const double deltaTime)
{
	if (_CONTROLLER_DEBUG)
	{
	}
	if (thePlayerInfo)
	{
		thePlayerInfo->UpdateLoop(deltaTime);
	}
	return false;
}

// Reset the PlayerInfo
bool CController::Reset(void)
{
	if (_CONTROLLER_DEBUG)
	{
	}
	if (thePlayerInfo)
	{
		thePlayerInfo->Reset();
	}
	return false;
}