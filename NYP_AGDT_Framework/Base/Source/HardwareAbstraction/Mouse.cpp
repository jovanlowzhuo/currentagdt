#include "Mouse.h"
#include <iostream>
#include "MouseController.h"
#include "../PlayerInfo/PlayerInfo.h"
using namespace std;

CMouse::CMouse()
{
}

CMouse::~CMouse()
{
}

// Create this controller
bool CMouse::Create(CPlayerInfo* thePlayerInfo)
{
	CController::Create(thePlayerInfo);

	return false;
}

// Read from the controller
int CMouse::Read(const double deltaTime)
{
	CController::Read(deltaTime);
	double mouse_diff_x, mouse_diff_y;
	double mouseSensitivity_x, mouseSensitivity_y;
	MouseController::GetInstance()->GetMouseDelta(mouse_diff_x, mouse_diff_y);
	mouseSensitivity_x = 0.05;
	mouseSensitivity_y = 0.05;

	// Process the keys for customisation
	if (mouse_diff_x != 0.0)
	{
		Look_LeftRight((float)deltaTime, true, (float)mouse_diff_x * (float)mouseSensitivity_x);
	}

	if (mouse_diff_y != 0.0)
	{
		Look_UpDown((float)deltaTime, true, (float)mouse_diff_y * (float)mouseSensitivity_y);
	}

	if (MouseController::GetInstance()->GetMouseScrollStatus(MouseController::SCROLL_TYPE_YOFFSET) != thePlayerInfo->GetWeapon())
	{
		if (MouseController::GetInstance()->GetMouseScrollStatus(MouseController::SCROLL_TYPE_YOFFSET) != thePlayerInfo->m_iCurrentWeapon)
		{
			if ((MouseController::GetInstance()->GetMouseScrollStatus(MouseController::SCROLL_TYPE_YOFFSET) >= 0) &&
				(MouseController::GetInstance()->GetMouseScrollStatus(MouseController::SCROLL_TYPE_YOFFSET) < CPlayerInfo::WEAPONS::TOTAL_WEAPONS))
			{
				thePlayerInfo->m_iCurrentWeapon = (int)MouseController::GetInstance()->GetMouseScrollStatus(MouseController::SCROLL_TYPE_YOFFSET);
			}
		}
		Change(deltaTime);
	}

	// if Mouse Buttons were activated, then act on them
	if (MouseController::GetInstance()->IsButtonDown(MouseController::LMB))
	{
		FirePrimary(deltaTime);
	}

	return 0;
}