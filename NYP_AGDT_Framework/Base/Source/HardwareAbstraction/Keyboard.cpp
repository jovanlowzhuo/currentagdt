#include "Keyboard.h"
#include <iostream>
#include "KeyboardController.h"
#include "../PlayerInfo/PlayerInfo.h"
using namespace std;
const bool _CONTROLLER_KEYBOARD_DEBUG = false;

CKeyboard::CKeyboard()
{
}

CKeyboard::~CKeyboard()
{
}

// Create this controller
bool CKeyboard::Create(CPlayerInfo* thePlayerInfo)
{
	CController::Create(thePlayerInfo);
	return false;
}

// Read from the controller
int CKeyboard::Read(const double deltaTime)
{
	CController::Read(deltaTime);
	if (_CONTROLLER_KEYBOARD_DEBUG)
	{
	}

	// Jump
	if ((KeyboardController::GetInstance()->IsKeyDown(VK_SPACE)) && (thePlayerInfo->isOnGround()))
	{
		Jump(deltaTime);
		StopSway(deltaTime);
	}
	
	// Crouch
	if (KeyboardController::GetInstance()->IsKeyPressed(VK_CONTROL))
	{
		Crouch(deltaTime);
	}

	if (KeyboardController::GetInstance()->IsKeyReleased('G'))
	{
		SlowMode(deltaTime);
	}

	// Reload weapons
	if (KeyboardController::GetInstance()->IsKeyDown('R'))
	{
		Reload(deltaTime);
	}

	// Moveleft Camaera 
	if (KeyboardController::GetInstance()->IsKeyDown('E'))
	{
		Roll_Left(deltaTime, true, 3.f);
	}
	if (KeyboardController::GetInstance()->IsKeyDown('Q'))
	{
		Roll_Left(deltaTime, true, -3.f);
	}

	// Reset
	if (KeyboardController::GetInstance()->IsKeyDown('P'))
	{
		Reset();
	}
	
	//Update
	UpdateLoop(deltaTime);
	
	return 0;
}