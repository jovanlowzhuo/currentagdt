#pragma once
#include "../PlayerInfo/PlayerInfo.h"

class CController
{
protected:
	CPlayerInfo* thePlayerInfo;

public:
	CController();
	virtual ~CController();

	// Create this controller
	virtual bool Create(CPlayerInfo* thePlayerInfo = NULL);
	// Read from the controller
	virtual int Read(const double deltaTime);

	// Detect and process front / back movement on the controller
	virtual bool Move_FrontBack(const double deltaTime, const bool direction, const float speedMultiplier = 1.0f);
	// Detect and process left / right movement on the controller
	virtual bool Move_LeftRight(const double deltaTime, const bool direction);
	// Detect and process look up / down on the controller
	virtual bool Look_UpDown(const double deltaTime, const bool direction, const float speedMultiplier = 1.0f);
	// Detect and process look left / right on the controller
	virtual bool Look_LeftRight(const double deltaTime, const bool direction, const float speedMultiplier = 1.0f);
	// Detect and rollLeft
	virtual bool Roll_Left(const double deltaTime, const bool direction, const float speedMultiplier = 1.0f);
 
	bool StopSway(const double deltaTime);

	// Jump
	virtual bool Jump(const double deltaTime);

	// Reload current weapon
	virtual bool Reload(const double deltaTime);
	// Change current weapon (for primary only)
	virtual bool Change(const double deltaTime);
	// Fire primary weapon
	virtual bool FirePrimary(const double deltaTime);
	// Fire secondary weapon
	virtual bool FireSecondary(const double deltaTime);
	// Crouch buttn
	virtual bool Crouch(const double deltaTime);

	virtual bool SlowMode(const double deltaTime);

	virtual bool UpdateLoop(const double deltaTime);

	// Reset the PlayerInfo
	virtual bool Reset(void);
};