#include "Furniture.h"

#include "MeshBuilder.h"
#include "../EntityManager.h"
#include "GraphicsManager.h"
#include "RenderHelper.h"

CFurniture::CFurniture()
{
}

CFurniture::CFurniture(Mesh * _modelMesh)
	: modelMesh(_modelMesh)
	, m_bStatus(false)
	, CurrentOffset(0, 0, 0)
	, MinOffset(0, 0, 0)
	, MaxOffset(0, 20, 0)
{
}



CFurniture::~CFurniture()
{
}

// Activate the projectile. true == active, false == inactive
void CFurniture::SetStatus(const bool m_bStatus)
{
	this->m_bStatus = m_bStatus;
}

// get status of the projectile. true == active, false == inactive
bool CFurniture::GetStatus(void) const
{
	return m_bStatus;
}

void CFurniture::Update(double dt)
{
	if (IsLastFrame())
	{
		CurrentOffset = Math::UpdateLimitNo(CurrentOffset, MinOffset, MaxOffset, dt);
		position = OriginalPos - CurrentOffset;
		if (CurrentOffset == MaxOffset)
		{
			SetIsDone(true);
		}
	}
}

// Render this projectile
void CFurniture::Render(void)
{
	if (m_bStatus == false)
		return;

	MS& modelStack = GraphicsManager::GetInstance()->GetModelStack();
	modelStack.PushMatrix();
	modelStack.Translate(position.x, position.y, position.z);
	modelStack.Scale(scale.x, scale.y, scale.z);
	RenderHelper::RenderMesh(modelMesh);
	modelStack.PopMatrix();
}

// Create a projectile and add it into EntityManager
CFurniture* Create::Furniture(const std::string& _meshName,
							  const Vector3& _position,
							  const Vector3& _scale,
							  const bool bAddToEntityManager)
{
	Mesh* modelMesh = MeshBuilder::GetInstance()->GetMesh(_meshName);
	if (modelMesh == nullptr)
		return nullptr;

	CFurniture* result = new CFurniture(modelMesh);
	result->SetPosition(_position);
	result->OriginalPos = _position;
	result->SetScale(_scale);
	result->SetStatus(true);
	result->SetCollider(true);
	result->SetLastFrame(false);
	result->EntityTypeEnum = EntityBase::FURNITURE_TYPE;
	// Add to Entity Manager if bAddToEntityManager==true
	if (bAddToEntityManager)
		EntityManager::GetInstance()->AddEntity(result);

	return result;
}