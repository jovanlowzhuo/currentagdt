#include "EventBox.h"

#include "MeshBuilder.h"
#include "../EntityManager.h"
#include "GraphicsManager.h"
#include "RenderHelper.h"


CEventBox::CEventBox()
{
}

CEventBox::CEventBox(Mesh * _modelMesh)
	: modelMesh(_modelMesh)
	, m_bStatus(false)
{
}

CEventBox::~CEventBox()
{
}

// Activate the projectile. true == active, false == inactive
void CEventBox::SetStatus(const bool m_bStatus)
{
	this->m_bStatus = m_bStatus;
}

// get status of the projectile. true == active, false == inactive
bool CEventBox::GetStatus(void) const
{
	return m_bStatus;
}

void CEventBox::Update(double dt)
{
	if (m_bStatus == false)
		return;
}

// Render this projectile
void CEventBox::Render(void)
{
	if (m_bStatus == false)
		return;

	MS& modelStack = GraphicsManager::GetInstance()->GetModelStack();
	modelStack.PushMatrix();
	modelStack.Translate(position.x, position.y, position.z);
	modelStack.Scale(scale.x, scale.y, scale.z);
	RenderHelper::RenderMesh(modelMesh);
	modelStack.PopMatrix();
}

// Create a projectile and add it into EntityManager
CEventBox* Create::EventBox(const std::string& _meshName,
							const Vector3& _position,
							const Vector3& _scale,
							const bool bAddToEntityManager)
{
	Mesh* modelMesh = MeshBuilder::GetInstance()->GetMesh(_meshName);
	if (modelMesh == nullptr)
		return nullptr;

	CEventBox* result = new CEventBox(modelMesh);
	result->SetPosition(_position);
	result->SetScale(_scale);
	result->SetStatus(true);
	result->SetCollider(true);
	result->AssociatedTrigger = new CEventTrigger;
	result->EntityTypeEnum = EntityBase::EVENT_TYPE;
	// Add to Entity Manager if bAddToEntityManager==true
	if (bAddToEntityManager)
		EntityManager::GetInstance()->AddEntity(result);

	return result;
}