#pragma once
#include "EntityBase.h"
#include "Vector3.h"
#include "Collider/Collider.h"
using namespace std;

class Mesh;

class CFurniture : public EntityBase, public CCollider
{
public:
	CFurniture();
	CFurniture(Mesh * _modelMesh);
	~CFurniture();

	// Activate the projectile. true == active, false == inactive
	void SetStatus(const bool m_bStatus);
	// get status of the projectile. true == active, false == inactive
	bool GetStatus(void) const;

	// Update the status of this projectile
	virtual void Update(double dt = 0.0333f);
	// Render this projectile
	virtual void Render(void);

	Vector3 OriginalPos, CurrentOffset, MinOffset, MaxOffset;

protected:
	// The model mesh for this projectile
	Mesh* modelMesh;
	// Boolean flag to indicate if this projectile is active. If not active, then do not compute/update
	bool m_bStatus;


};

namespace Create
{
	CFurniture* Furniture(const std::string& _meshName,
						  const Vector3& _position,
						  const Vector3& _scale,
						  const bool bAddToEntityManager);
};