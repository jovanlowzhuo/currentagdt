#include "EntityManager.h"
#include "EntityBase.h"
#include "Collider/Collider.h"
#include <iostream>
#include "SceneGraph\SceneGraph.h"
using namespace std;

// Update all entities
void EntityManager::Update(double _dt)
{
	// Update all entities
	std::list<EntityBase*>::iterator it, end;
	end = entityList.end();
	for (it = entityList.begin(); it != end; ++it)
	{
		Vector3 tempPosition = (*it)->GetPosition();
		if ((*it)->GetUpdate() == true)
		{
			(*it)->Update(_dt);
			if (CheckForCollision(it, _dt))
			{
				if (((*it)->getEntityType() == EntityBase::PLAYER_TYPE) || ((*it)->getEntityType() == EntityBase::ENEMY_TYPE))
				{
					(*it)->SetPosition(tempPosition);
				}
			}
		}
	}




	// Clean up entities that are done
	it = entityList.begin();
	while (it != end)
	{
		if ((*it)->IsDone())
		{
			// Delete if done
			delete *it;
			it = entityList.erase(it);
		}
		else
		{
			// Move on otherwise
			++it;
		}
	}
}

// Render all entities
void EntityManager::Render()
{
	// Render all entities
	std::list<EntityBase*>::iterator it, end;
	end = entityList.end();
	for (it = entityList.begin(); it != end; ++it)
	{
		if ((*it)->IsDone())
		{
			continue;
		}
		if ((*it)->DoNotRender == false)
		{
			//if ((*it)->getEntityType() != EntityBase::EVENT_TYPE)
			//{
				(*it)->Render();
			//}
		}
	}

	//CSceneGraph::GetInstance()->Render();
}

// Render the UI entities
void EntityManager::RenderUI()
{
	// Render all entities UI
	std::list<EntityBase*>::iterator it, end;
	end = entityList.end();
	for (it = entityList.begin(); it != end; ++it)
	{
		if ((*it)->IsDone())
		{
			continue;
		}
		(*it)->RenderUI();
	}
}

// Add an entity to this EntityManager
void EntityManager::AddEntity(EntityBase* _newEntity, bool b_AddToSpatialParition)
{
	entityList.push_back(_newEntity);

	if (theSpatialPartition && b_AddToSpatialParition)
		theSpatialPartition->Add(_newEntity);
}

// Remove an entity from this EntityManager
bool EntityManager::RemoveEntity(EntityBase* _existingEntity)
{
	// Find the entity's iterator
	std::list<EntityBase*>::iterator findIter = std::find(entityList.begin(), entityList.end(), _existingEntity);

	// Delete the entity if found
	if (findIter != entityList.end())
	{
		delete *findIter;
		findIter = entityList.erase(findIter);

		if (CSceneGraph::GetInstance()->DeleteNode(_existingEntity) == false)
		{
			//cout << "EntityManager::RemoveEntity: Unable to remove this entity from Scene Graph" << endl;
		}
		else
		{
			if(theSpatialPartition)
				theSpatialPartition->Remove(_existingEntity);
		}
		return true;	
	}
	// Return false if not found
	return false;
}

// Constructor
EntityManager::EntityManager()
	: theSpatialPartition(NULL)
	, BulletCollideEnemy(false)
	, ScoreInput(0)
{
}

// Destructor
EntityManager::~EntityManager()
{
	// Clear out the Scene Graph
	CSceneGraph::GetInstance()->Destroy();
}

// Check for overlap
bool EntityManager::CheckOverlap(Vector3 thisMinAABB, Vector3 thisMaxAABB, Vector3 thatMinAABB, Vector3 thatMaxAABB)
{	
	return false;
}

bool EntityManager::MarkForDeletion(EntityBase* _existingEntity)
{
	// Find the entity's iterator
	std::list<EntityBase*>::iterator findIter = std::find(entityList.begin(), entityList.end(), _existingEntity);

	// Delete the entity if found
	if (findIter != entityList.end())
	{
		(*findIter)->SetIsDone(true);
		return true;
	}
	// Return false if not found
	return false;
}

// Check if this entity's bounding sphere collided with that entity's bounding sphere 
bool EntityManager::CheckSphereCollision(EntityBase *ThisEntity, EntityBase *ThatEntity)
{
	return false;
}

// Check if this entity collided with another entity, but both must have collider
bool EntityManager::CheckAABBCollision(EntityBase *ThisEntity, EntityBase *ThatEntity)
{
	Vector3 a = ThisEntity->GetPosition();
	Vector3 aScale = ThisEntity->GetScale() * 0.5f;
	Vector3 aMin = a - aScale;
	Vector3 aMax = a + aScale;

	Vector3 b = ThatEntity->GetPosition();
	Vector3 bScale = ThatEntity->GetScale() * 0.5f;
	Vector3 bMin = b - bScale;
	Vector3 bMax = b + bScale;

	return    (((aMin.x >= bMin.x && aMin.x <= bMax.x) || (bMin.x >= aMin.x && bMin.x <= aMax.x)) &&
		((aMax.x >= bMin.x && aMax.x <= bMax.x) || (bMax.x >= aMin.x && bMax.x <= aMax.x)) &&
		((aMin.y >= bMin.y && aMin.y <= bMax.y) || (bMin.y >= aMin.y && bMin.y <= aMax.y)) &&
		((aMax.y >= bMin.y && aMax.y <= bMax.y) || (bMax.y >= aMin.y && bMax.y <= aMax.y)) &&
		((aMin.z >= bMin.z && aMin.z <= bMax.z) || (bMin.z >= aMin.z && bMin.z <= aMax.z)) &&
		((aMax.z >= bMin.z && aMax.z <= bMax.z) || (bMax.z >= aMin.z && bMax.z <= aMax.z)));
}

bool EntityManager::CheckForBulletCollision(EntityBase *ThisEntity, EntityBase *ThatEntity, double dt)
{
	if ((ThatEntity->GetDirection().IsZero() != true) && (ThatEntity->GetSpeed() > 10.0f))
	{
		Vector3 DistanceVector;
		DistanceVector.SetZero();
		float speedTest = ThatEntity->GetSpeed();
		DistanceVector = ThisEntity->GetPosition() - ThatEntity->GetPosition();
		float ScaleDistance = (ThatEntity->GetScale() * 0.5f).Length() + (ThisEntity->GetScale() * 0.5f).Length();
		float distance = DistanceVector.Length() - (ScaleDistance * 0.75f);
		double predictedDelta = distance/speedTest;
		if ((predictedDelta < 0) || (predictedDelta > 0.2f))
		{
			return false;
		}
		if (predictedDelta <= dt)
		{
			ThatEntity->SetMidFrame(predictedDelta);
			return true;
		}
	}
	return false;
}

void EntityManager::SetSpatialPartition(CSpatialPartition *SetSpatialPartition)
{
	this->theSpatialPartition = SetSpatialPartition;
}

void EntityManager::ClearEntities()
{
	entityList.clear();
}

// Check if any Collider is colliding with another Collider
bool EntityManager::CheckForCollision(std::list<EntityBase*>::iterator dis, double dt)
{
	std::list<EntityBase*>::iterator dat, end;
	end = entityList.end();

	for (dat = dis; dat != end; ++dat)
	{
		if (dis != dat)
		{
			bool temp = (CheckForCollisionType((*dis), (*dat),dt));
			if (temp)
			{
				return temp;
			}
		}
	}
	return false;
}

bool EntityManager::CheckForCollisionType(EntityBase *ThisEntity, EntityBase *ThatEntity, double dt)
{
	if (((ThisEntity->HasCollider() == false) || (ThatEntity->HasCollider() == false)) ||
		((ThisEntity->IsDone() == true) || (ThatEntity->IsDone() == true)))
	{
		return false;
	}

	switch (ThisEntity->getEntityType())
	{
		case EntityBase::PLAYER_TYPE:
		{
			if ((ThatEntity)->getEntityType() == EntityBase::FURNITURE_TYPE)
			{
				if (CheckAABBCollision(ThisEntity, ThatEntity))
				{
					return true;
				}
			}

			if (ThatEntity->getEntityType() == EntityBase::EVENT_TYPE)
			{
				if (CheckAABBCollision(ThisEntity, ThatEntity))
				{
					(ThatEntity->AssociatedTrigger->SetEvent(true));
				}
			}
			break;
		}
		case EntityBase::FURNITURE_TYPE:
		{
			if ((ThatEntity)->getEntityType() == EntityBase::PLAYER_PROJECTILE_TYPE)
			{
				if (CheckAABBCollision(ThisEntity, ThatEntity))
				{
					(ThatEntity)->SetUpdate(false);
					(ThatEntity)->SetCollider(false);
					(ThatEntity)->SetIsDone(true);
				}
			}
			if ((ThatEntity)->getEntityType() == EntityBase::ENEMY_PROJECTILE_TYPE)
			{
				if (CheckForBulletCollision(ThisEntity, ThatEntity, dt))
				{
					(ThatEntity)->SetUpdate(false);
					(ThatEntity)->SetCollider(false);
				}
			}
			break;
		}
		case EntityBase::ENEMY_TYPE:
		{
			if ((ThatEntity)->getEntityType() == EntityBase::FURNITURE_TYPE)
			{
				if (CheckAABBCollision(ThisEntity, ThatEntity))
				{
					return true;
				}
			}
			if ((ThatEntity)->getEntityType() == EntityBase::PLAYER_PROJECTILE_TYPE)
			{
				if (CheckForBulletCollision(ThisEntity, ThatEntity, dt))
				{
					(ThatEntity)->SetIsDone(true);
					(ThisEntity)->SetIsDone(true);
					
					ScoreInput += 1000;
				}
			}
			break;
		}
	default:
		break;
	}
	return false;
}

bool EntityManager::BuyingCheck(int Cost)
{
	if ((ScoreInput - Cost) >= 0)
	{
		ScoreInput -= Cost;
		return true;
	}
	return false;
}

void EntityManager::ClearProjectile()
{
	std::list<EntityBase*>::iterator it, end;
	end = entityList.end();

	for (it = entityList.begin(); it != end; ++it)
	{
		if ((((*it)->getEntityType() == EntityBase::PLAYER_PROJECTILE_TYPE) || ((*it)->getEntityType() == EntityBase::ENEMY_PROJECTILE_TYPE)) && ((*it)->IsDone() == false))
		{
			(*it)->SetIsDone(true);
		}
	}
}