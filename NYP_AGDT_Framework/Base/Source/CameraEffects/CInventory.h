#ifndef INVENTORY_H
#define INVENTORY_H

#include "EntityBase.h"
#include "SingletonTemplate.h"
#include "Mesh.h"
#include "MouseController.h"
#include "../WeaponInfo/WeaponInfo.h"

class CInventory : public EntityBase, public Singleton<CInventory>
{
public:
	enum SPRITE_RENDERMODE
	{
		MODE_2D,
		MODE_3D,
		NUM_MODE
	};

	CInventory(void);
	virtual ~CInventory(void);

	Mesh* m_CameraEffects_InventoryPistolBorderScreen;
	Mesh* m_CameraEffects_InventoryRifleBorderScreen;
	Mesh* m_CameraEffects_InventoryMinigunBorderScreen;
	Mesh* m_CameraEffects_InventoryShotgunBorderScreen;
	Mesh* m_CameraEffects_InventoryGrenadeBorderScreen;
	Mesh* m_CameraEffects_InventoryBlankBorderScreen;

	// Initialise this class instance
	bool Init(void);

	// Set the InventoryPistolBorderScreen
	bool SetInventoryPistolBorderScreen(Mesh* aTarget);
	// Get the InventoryPistolBorderScreen
	Mesh* GetInventoryPistolBorderScreen(void) const;

	// Set the InventoryRifleBorderScreen
	bool SetInventoryRifleBorderScreen(Mesh* aTarget);
	// Get the InventoryRifleBorderScreen
	Mesh* GetInventoryRifleBorderScreen(void) const;

	// Set the InventoryMinigunBorderScreen
	bool SetInventoryMinigunBorderScreen(Mesh* aTarget);
	// Get the InventoryMinigunBorderScreen
	Mesh* GetInventoryMinigunBorderScreen(void) const;

	// Set the InventoryShotgunBorderScreen
	bool SetInventoryShotgunBorderScreen(Mesh* aTarget);
	// Get the InventoryShotgunBorderScreen
	Mesh* GetInventoryShotgunBorderScreen(void) const;

	// Set the InventoryGrenadeBorderScreen
	bool SetInventoryGrenadeBorderScreen(Mesh* aTarget);
	// Get the InventoryGrenadeBorderScreen
	Mesh* GetInventoryGrenadeBorderScreen(void) const;

	// Set the InventoryBlankBorderScreen
	bool SetInventoryBlankBorderScreen(Mesh* aTarget);
	// Get the InventoryBlankBorderScreen
	Mesh* GetInventoryBlankBorderScreen(void) const;

	// Set rotation angle of CCameraEffect
	bool SetAngle(const int angle);
	// Get rotation angle of CCameraEffect
	int GetAngle(void) const;

	// Set boolean flag for InventoryPistolBorderScreen
	bool SetStatus_InventoryPistolBorderScreen(const bool m_bStatusInventoryPistolBorderScreen);

	// Set boolean flag for InventoryRifleBorderScreen
	bool SetStatus_InventoryRifleBorderScreen(const bool m_bStatusInventoryRifleBorderScreen);

	// Set boolean flag for InventoryMinigunBorderScreen
	bool SetStatus_InventoryMinigunBorderScreen(const bool m_bStatusInventoryMinigunBorderScreen);

	// Set boolean flag for InventoryShotBorderScreen
	bool SetStatus_InventoryShotgunBorderScreen(const bool m_bStatusInventoryShotgunBorderScreen);

	// Set boolean flag for InventoryGrenadeBorderScreen
	bool SetStatus_InventoryGrenadeBorderScreen(const bool m_bStatusInventoryGrenadeBorderScreen);

	// Set boolean flag for InventoryBlankBorderScreen
	bool SetStatus_InventoryBlankBorderScreen(const bool m_bStatusInventoryBlankBorderScreen);

	// Set Countdown Timer for InventoryPistolBorderScreen
	bool SetTimer_InventoryBorderScreen(const float m_fCountdownTimer_InventoryPistolBorderScreen = 2.0f);
	// Get Countdown Timer for InventoryPistolBorderScreen
	float GetTimer_InventoryBorderScreen(void) const;
	CWeaponInfo** Dependent;
	bool SetTrigger(CWeaponInfo** playerInventory);

	// Update the camera effects
	virtual void Update(const float dt = 0.0333f);
	// Render the UI
	virtual void RenderUI();

protected:
	// Rotation from First Angle
	int m_iAngle;
	
	// Boolean flag to indicate if the InventoryPistolBorderScreen is rendered
	bool m_bStatusInventoryPistolBorderScreen;
	// Boolean flag to indicate if the InventoryRifleBorderScreen is rendered
	bool m_bStatusInventoryRifleBorderScreen;
	// Boolean flag to indicate if the InventoryMinigunBorderScreen is rendered
	bool m_bStatusInventoryMinigunBorderScreen;
	// Boolean flag to indicate if the InventoryShotgunBorderScreen is rendered
	bool m_bStatusInventoryShotgunBorderScreen;
	// Boolean flag to indicate if the InventoryGrenadeBorderScreen is rendered
	bool m_bStatusInventoryGrenadeBorderScreen;
	// Boolean flag to indicate if the InventoryBlankBorderScreen is rendered
	bool m_bStatusInventoryBlankBorderScreen;

	// Countdown Timer for InventoryBorderScreen
	float m_fCountdownTimer_InventoryBorderScreen;

	int LastScrollLoc;

	SPRITE_RENDERMODE mode;
};

namespace Create
{
	CInventory* InventoryEffects(const bool m_bAddToLibrary);
};
#endif