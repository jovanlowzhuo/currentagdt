#include "CInventory.h"
#include "GraphicsManager.h"
#include "RenderHelper.h"
#include "../EntityManager.h"
#include "GL\glew.h"

CInventory::CInventory(void)
	: m_iAngle(-90)
	, m_bStatusInventoryPistolBorderScreen(false)
	, m_CameraEffects_InventoryPistolBorderScreen(NULL)
	, m_bStatusInventoryRifleBorderScreen(false)
	, m_CameraEffects_InventoryRifleBorderScreen(NULL)
	, m_bStatusInventoryMinigunBorderScreen(false)
	, m_CameraEffects_InventoryMinigunBorderScreen(NULL)
	, m_bStatusInventoryShotgunBorderScreen(false)
	, m_CameraEffects_InventoryShotgunBorderScreen(NULL)
	, m_bStatusInventoryGrenadeBorderScreen(false)
	, m_CameraEffects_InventoryGrenadeBorderScreen(NULL)
	, m_bStatusInventoryBlankBorderScreen(false)
	, m_CameraEffects_InventoryBlankBorderScreen(NULL)
	, m_fCountdownTimer_InventoryBorderScreen(2.0f)
	, mode(MODE_2D)
{
	Init();
}

CInventory::~CInventory(void)
{
	if (m_CameraEffects_InventoryPistolBorderScreen)
	{
		delete m_CameraEffects_InventoryPistolBorderScreen;
		m_CameraEffects_InventoryPistolBorderScreen = NULL;
	}
	if (m_CameraEffects_InventoryRifleBorderScreen)
	{
		delete m_CameraEffects_InventoryRifleBorderScreen;
		m_CameraEffects_InventoryRifleBorderScreen = NULL;
	}
	if (m_CameraEffects_InventoryMinigunBorderScreen)
	{
		delete m_CameraEffects_InventoryMinigunBorderScreen;
		m_CameraEffects_InventoryMinigunBorderScreen = NULL;
	}
	if (m_CameraEffects_InventoryShotgunBorderScreen)
	{
		delete m_CameraEffects_InventoryShotgunBorderScreen;
		m_CameraEffects_InventoryShotgunBorderScreen = NULL;
	}
	if (m_CameraEffects_InventoryGrenadeBorderScreen)
	{
		delete m_CameraEffects_InventoryGrenadeBorderScreen;
		m_CameraEffects_InventoryGrenadeBorderScreen = NULL;
	}
	if (m_CameraEffects_InventoryBlankBorderScreen)
	{
		delete m_CameraEffects_InventoryBlankBorderScreen;
		m_CameraEffects_InventoryBlankBorderScreen = NULL;
	}
}

// Initialise this class instance
bool CInventory::Init(void)
{
	m_iAngle = -90;
	position.Set(0.0f, 0.0f, 0.0f);
	scale.Set(800.0f, 600.0f, 100.0f);
	LastScrollLoc = 0;
	return true;
}

bool CInventory::SetInventoryPistolBorderScreen(Mesh* aInventoryPistolBorderScreen)
{
	if (aInventoryPistolBorderScreen != NULL)
	{
		m_CameraEffects_InventoryPistolBorderScreen = aInventoryPistolBorderScreen;
		return true;
	}
	return false;
}
Mesh* CInventory::GetInventoryPistolBorderScreen(void) const
{
	return m_CameraEffects_InventoryPistolBorderScreen;
}

bool CInventory::SetInventoryRifleBorderScreen(Mesh * aInventoryRifleBorderScreen)
{
	if (aInventoryRifleBorderScreen != NULL)
	{
		m_CameraEffects_InventoryRifleBorderScreen = aInventoryRifleBorderScreen;
		return true;
	}
	return false;
}
Mesh * CInventory::GetInventoryRifleBorderScreen(void) const
{
	return m_CameraEffects_InventoryRifleBorderScreen;
}

bool CInventory::SetInventoryMinigunBorderScreen(Mesh * aInventoryMinigunBorderScreen)
{
	if (aInventoryMinigunBorderScreen != NULL)
	{
		m_CameraEffects_InventoryMinigunBorderScreen = aInventoryMinigunBorderScreen;
		return true;
	}
	return false;
}
Mesh* CInventory::GetInventoryMinigunBorderScreen(void) const
{
	return m_CameraEffects_InventoryMinigunBorderScreen;
}

bool CInventory::SetInventoryShotgunBorderScreen(Mesh * aInventoryShotgunBorderScreen)
{
	if (aInventoryShotgunBorderScreen != NULL)
	{
		m_CameraEffects_InventoryShotgunBorderScreen = aInventoryShotgunBorderScreen;
		return true;
	}
	return false;
}
Mesh * CInventory::GetInventoryShotgunBorderScreen(void) const
{
	return m_CameraEffects_InventoryShotgunBorderScreen;
}

bool CInventory::SetInventoryGrenadeBorderScreen(Mesh * aInventoryGrenadeBorderScreen)
{
	if (aInventoryGrenadeBorderScreen != NULL)
	{
		m_CameraEffects_InventoryGrenadeBorderScreen = aInventoryGrenadeBorderScreen;
		return true;
	}
	return false;
}
Mesh * CInventory::GetInventoryGrenadeBorderScreen(void) const
{
	return m_CameraEffects_InventoryGrenadeBorderScreen;
}

bool CInventory::SetInventoryBlankBorderScreen(Mesh * aInventoryBlankBorderScreen)
{
	if (aInventoryBlankBorderScreen != NULL)
	{
		m_CameraEffects_InventoryBlankBorderScreen = aInventoryBlankBorderScreen;
		return true;
	}
	return false;
}
Mesh * CInventory::GetInventoryBlankBorderScreen(void) const
{
	return m_CameraEffects_InventoryBlankBorderScreen;
}

// Set m_iAngle of avatar
bool CInventory::SetAngle(const int m_iAngle)
{
	this->m_iAngle = m_iAngle;
	return true;
}

// Get m_iAngle
int CInventory::GetAngle(void) const
{
	return m_iAngle;
}

// Set boolean flag for InventoryPistolBorderScreen
bool CInventory::SetStatus_InventoryPistolBorderScreen(const bool m_bStatusInventoryPistolBorderScreen)
{
	this->m_bStatusInventoryPistolBorderScreen = m_bStatusInventoryPistolBorderScreen;
	return true;
}

// Set boolean flag for InventoryRifleBorderScreen
bool CInventory::SetStatus_InventoryRifleBorderScreen(const bool m_bStatusInventoryRifleBorderScreen)
{
	this->m_bStatusInventoryRifleBorderScreen = m_bStatusInventoryRifleBorderScreen;
	return true;
}

// Set boolean flag for InventoryMinigunBorderScreen
bool CInventory::SetStatus_InventoryMinigunBorderScreen(const bool m_bStatusInventoryMinigunBorderScreen)
{
	this->m_bStatusInventoryMinigunBorderScreen = m_bStatusInventoryMinigunBorderScreen;
	return true;
}

// Set boolean flag for InventoryShotgunBorderScreen
bool CInventory::SetStatus_InventoryShotgunBorderScreen(const bool m_bStatusInventoryShotgunBorderScreen)
{
	this->m_bStatusInventoryShotgunBorderScreen = m_bStatusInventoryShotgunBorderScreen;
	return true;
}

// Set boolean flag for InventoryGrenadeBorderScreen
bool CInventory::SetStatus_InventoryGrenadeBorderScreen(const bool m_bStatusInventoryGrenadeBorderScreen)
{
	this->m_bStatusInventoryGrenadeBorderScreen = m_bStatusInventoryGrenadeBorderScreen;
	return true;
}

// Set boolean flag for InventoryBlankBorderScreen
bool CInventory::SetStatus_InventoryBlankBorderScreen(const bool m_bStatusInventoryBlankBorderScreen)
{
	this->m_bStatusInventoryBlankBorderScreen = m_bStatusInventoryBlankBorderScreen;
	return true;
}

// Set Countdown Timer for InventoryBorderScreen
bool CInventory::SetTimer_InventoryBorderScreen(const float m_fCountdownTimer_InventoryBorderScreen)
{
	this->m_fCountdownTimer_InventoryBorderScreen = m_fCountdownTimer_InventoryBorderScreen;
	return true;
}

// Get Countdown Timer for InventoryBorderScreen
float CInventory::GetTimer_InventoryBorderScreen(void) const
{
	return m_fCountdownTimer_InventoryBorderScreen;
}

bool CInventory::SetTrigger(CWeaponInfo** playerInventory)
{
	this->Dependent = playerInventory;
	return false;
}

// Update the camera effects
void CInventory::Update(const float dt)
{
	int ScrollNumber = (int)MouseController::GetInstance()->GetMouseScrollStatus(MouseController::SCROLL_TYPE_YOFFSET);
	if (LastScrollLoc != ScrollNumber)
	{
		//update
		switch (ScrollNumber)
		{
		case 0:
		case 1:
		case 2:
		case 3:
			SetStatus_InventoryPistolBorderScreen(true);
			SetStatus_InventoryRifleBorderScreen(true);
			SetStatus_InventoryMinigunBorderScreen(true);
			SetStatus_InventoryShotgunBorderScreen(true);
			SetStatus_InventoryGrenadeBorderScreen(true);
			SetStatus_InventoryBlankBorderScreen(true);
			break;
		default:
			break;
		}
		LastScrollLoc = ScrollNumber;
		m_fCountdownTimer_InventoryBorderScreen = 2.0f;
	}
	else
	{
		if (m_bStatusInventoryPistolBorderScreen || m_bStatusInventoryRifleBorderScreen || m_bStatusInventoryMinigunBorderScreen || m_bStatusInventoryShotgunBorderScreen || m_bStatusInventoryGrenadeBorderScreen || m_bStatusInventoryBlankBorderScreen)
		{
			m_fCountdownTimer_InventoryBorderScreen -= dt;
			if (m_fCountdownTimer_InventoryBorderScreen <= 0.0f)
			{
				m_bStatusInventoryPistolBorderScreen = false;
				m_bStatusInventoryRifleBorderScreen = false;
				m_bStatusInventoryMinigunBorderScreen = false;
				m_bStatusInventoryShotgunBorderScreen = false;
				m_bStatusInventoryGrenadeBorderScreen = false;
				m_bStatusInventoryBlankBorderScreen = false;
			}
		}
	}
}

// Render the UI
void CInventory::RenderUI()
{
	glEnable(GL_DEPTH_TEST);
	if (mode == MODE_3D)
		return;

	MS& modelStack = GraphicsManager::GetInstance()->GetModelStack();
	Vector3 defaultScale, enlargeScale, pistolDefaultpos, rifleDefaultpos, minigunDefaultpos, shotgunDefaultpos, grenadeDefaultpos;
	defaultScale.Set(100.f, 50.f, 100.f);
	enlargeScale.Set(200.f, 100.f, 200.f);
	pistolDefaultpos.Set(-350.f, 275.f, 1.f);
	rifleDefaultpos.Set(-225.f, 275.f, 1.f);
	minigunDefaultpos.Set(-100.f, 275.f, 1.f);
	shotgunDefaultpos.Set(125.f, 275.f, 1.f);
	grenadeDefaultpos.Set(250.f, 275.f, 1.f);

	if (MouseController::GetInstance()->GetMouseScrollStatus(MouseController::SCROLL_TYPE_YOFFSET) == 0)
	{
		if (m_bStatusInventoryPistolBorderScreen && m_CameraEffects_InventoryPistolBorderScreen)
		{
			modelStack.PushMatrix();
			modelStack.Translate(pistolDefaultpos.x + 50.f, pistolDefaultpos.y - 25.f, pistolDefaultpos.z);
			modelStack.Scale(enlargeScale.x, enlargeScale.y, enlargeScale.z);
			// compress the whole shit
			switch (Dependent[0]->WeaponType_Enum)
			{
			case CWeaponInfo::SEMI_AUTO:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryPistolBorderScreen);
				break;
			}
			case CWeaponInfo::FULL_AUTO:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryMinigunBorderScreen);
				break;
			}
			case CWeaponInfo::BURST:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryRifleBorderScreen);
				break;
			}
			case CWeaponInfo::SPREAD:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryShotgunBorderScreen);
				break;
			}
			case CWeaponInfo::GRENADE:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryGrenadeBorderScreen);
				break;
			}
			case CWeaponInfo::BLANK:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryBlankBorderScreen);
				break;
			}
			default:
				break;
			}
			modelStack.PopMatrix();
		}
		if (m_bStatusInventoryRifleBorderScreen && m_CameraEffects_InventoryRifleBorderScreen)
		{
			modelStack.PushMatrix();
			modelStack.Translate(rifleDefaultpos.x + 100.f, rifleDefaultpos.y, rifleDefaultpos.z);
			modelStack.Scale(defaultScale.x, defaultScale.y, defaultScale.z);
			switch (Dependent[1]->WeaponType_Enum)
			{
			case CWeaponInfo::SEMI_AUTO:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryPistolBorderScreen);
				break;
			}
			case CWeaponInfo::FULL_AUTO:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryMinigunBorderScreen);
				break;
			}
			case CWeaponInfo::BURST:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryRifleBorderScreen);
				break;
			}
			case CWeaponInfo::SPREAD:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryShotgunBorderScreen);
				break;
			}
			case CWeaponInfo::GRENADE:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryGrenadeBorderScreen);
				break;
			}
			case CWeaponInfo::BLANK:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryBlankBorderScreen);
				break;
			}
			default:
				break;
			}
			modelStack.PopMatrix();
		}
		if (m_bStatusInventoryMinigunBorderScreen && m_CameraEffects_InventoryMinigunBorderScreen)
		{
			modelStack.PushMatrix();
			modelStack.Translate(minigunDefaultpos.x + 100.f, minigunDefaultpos.y, minigunDefaultpos.z);
			modelStack.Scale(defaultScale.x, defaultScale.y, defaultScale.z);
			switch (Dependent[2]->WeaponType_Enum)
			{
			case CWeaponInfo::SEMI_AUTO:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryPistolBorderScreen);
				break;
			}
			case CWeaponInfo::FULL_AUTO:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryMinigunBorderScreen);
				break;
			}
			case CWeaponInfo::BURST:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryRifleBorderScreen);
				break;
			}
			case CWeaponInfo::SPREAD:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryShotgunBorderScreen);
				break;
			}
			case CWeaponInfo::GRENADE:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryGrenadeBorderScreen);
				break;
			}
			case CWeaponInfo::BLANK:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryBlankBorderScreen);
				break;
			}
			default:
				break;
			}
			modelStack.PopMatrix();
		}
		if (m_bStatusInventoryShotgunBorderScreen && m_CameraEffects_InventoryShotgunBorderScreen)
		{
			modelStack.PushMatrix();
			modelStack.Translate(shotgunDefaultpos.x, shotgunDefaultpos.y, shotgunDefaultpos.z);
			modelStack.Scale(defaultScale.x, defaultScale.y, defaultScale.z);
			switch (Dependent[3]->WeaponType_Enum)
			{
			case CWeaponInfo::SEMI_AUTO:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryPistolBorderScreen);
				break;
			}
			case CWeaponInfo::FULL_AUTO:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryMinigunBorderScreen);
				break;
			}
			case CWeaponInfo::BURST:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryRifleBorderScreen);
				break;
			}
			case CWeaponInfo::SPREAD:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryShotgunBorderScreen);
				break;
			}
			case CWeaponInfo::GRENADE:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryGrenadeBorderScreen);
				break;
			}
			case CWeaponInfo::BLANK:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryBlankBorderScreen);
				break;
			}
			default:
				break;
			}
			modelStack.PopMatrix();
		}
		if (m_bStatusInventoryGrenadeBorderScreen && m_CameraEffects_InventoryGrenadeBorderScreen)
		{
			modelStack.PushMatrix();
			modelStack.Translate(grenadeDefaultpos.x, grenadeDefaultpos.y, grenadeDefaultpos.z);
			modelStack.Scale(defaultScale.x, defaultScale.y, defaultScale.z);
			switch (Dependent[4]->WeaponType_Enum)
			{
			case CWeaponInfo::SEMI_AUTO:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryPistolBorderScreen);
				break;
			}
			case CWeaponInfo::FULL_AUTO:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryMinigunBorderScreen);
				break;
			}
			case CWeaponInfo::BURST:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryRifleBorderScreen);
				break;
			}
			case CWeaponInfo::SPREAD:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryShotgunBorderScreen);
				break;
			}
			case CWeaponInfo::GRENADE:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryGrenadeBorderScreen);
				break;
			}
			case CWeaponInfo::BLANK:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryBlankBorderScreen);
				break;
			}
			default:
				break;
			}
			modelStack.PopMatrix();
		}
	}
	else if (MouseController::GetInstance()->GetMouseScrollStatus(MouseController::SCROLL_TYPE_YOFFSET) == 1)
	{
		if (m_bStatusInventoryPistolBorderScreen && m_CameraEffects_InventoryPistolBorderScreen)
		{
			modelStack.PushMatrix();
			modelStack.Translate(pistolDefaultpos.x, pistolDefaultpos.y, pistolDefaultpos.z);
			modelStack.Scale(defaultScale.x, defaultScale.y, defaultScale.z);
			switch (Dependent[0]->WeaponType_Enum)
			{
			case CWeaponInfo::SEMI_AUTO:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryPistolBorderScreen);
				break;
			}
			case CWeaponInfo::FULL_AUTO:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryMinigunBorderScreen);
				break;
			}
			case CWeaponInfo::BURST:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryRifleBorderScreen);
				break;
			}
			case CWeaponInfo::SPREAD:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryShotgunBorderScreen);
				break;
			}
			case CWeaponInfo::GRENADE:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryGrenadeBorderScreen);
				break;
			}
			case CWeaponInfo::BLANK:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryBlankBorderScreen);
				break;
			}
			default:
				break;
			}
			modelStack.PopMatrix();
		}
		if (m_bStatusInventoryRifleBorderScreen && m_CameraEffects_InventoryRifleBorderScreen)
		{
			modelStack.PushMatrix();
			modelStack.Translate(rifleDefaultpos.x + 50.f, rifleDefaultpos.y - 25.f, rifleDefaultpos.z);
			modelStack.Scale(enlargeScale.x, enlargeScale.y, enlargeScale.z);
			switch (Dependent[1]->WeaponType_Enum)
			{
			case CWeaponInfo::SEMI_AUTO:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryPistolBorderScreen);
				break;
			}
			case CWeaponInfo::FULL_AUTO:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryMinigunBorderScreen);
				break;
			}
			case CWeaponInfo::BURST:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryRifleBorderScreen);
				break;
			}
			case CWeaponInfo::SPREAD:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryShotgunBorderScreen);
				break;
			}
			case CWeaponInfo::GRENADE:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryGrenadeBorderScreen);
				break;
			}
			case CWeaponInfo::BLANK:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryBlankBorderScreen);
				break;
			}
			default:
				break;
			}
			modelStack.PopMatrix();
		}
		if (m_bStatusInventoryMinigunBorderScreen && m_CameraEffects_InventoryMinigunBorderScreen)
		{
			modelStack.PushMatrix();
			modelStack.Translate(minigunDefaultpos.x + 100.f, minigunDefaultpos.y, minigunDefaultpos.z);
			modelStack.Scale(defaultScale.x, defaultScale.y, defaultScale.z);
			switch (Dependent[2]->WeaponType_Enum)
			{
			case CWeaponInfo::SEMI_AUTO:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryPistolBorderScreen);
				break;
			}
			case CWeaponInfo::FULL_AUTO:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryMinigunBorderScreen);
				break;
			}
			case CWeaponInfo::BURST:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryRifleBorderScreen);
				break;
			}
			case CWeaponInfo::SPREAD:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryShotgunBorderScreen);
				break;
			}
			case CWeaponInfo::GRENADE:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryGrenadeBorderScreen);
				break;
			}
			case CWeaponInfo::BLANK:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryBlankBorderScreen);
				break;
			}
			default:
				break;
			}
			modelStack.PopMatrix();
		}
		if (m_bStatusInventoryShotgunBorderScreen && m_CameraEffects_InventoryShotgunBorderScreen)
		{
			modelStack.PushMatrix();
			modelStack.Translate(shotgunDefaultpos.x, shotgunDefaultpos.y, shotgunDefaultpos.z);
			modelStack.Scale(defaultScale.x, defaultScale.y, defaultScale.z);
			switch (Dependent[3]->WeaponType_Enum)
			{
			case CWeaponInfo::SEMI_AUTO:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryPistolBorderScreen);
				break;
			}
			case CWeaponInfo::FULL_AUTO:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryMinigunBorderScreen);
				break;
			}
			case CWeaponInfo::BURST:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryRifleBorderScreen);
				break;
			}
			case CWeaponInfo::SPREAD:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryShotgunBorderScreen);
				break;
			}
			case CWeaponInfo::GRENADE:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryGrenadeBorderScreen);
				break;
			}
			case CWeaponInfo::BLANK:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryBlankBorderScreen);
				break;
			}
			default:
				break;
			}
			modelStack.PopMatrix();
		}
		if (m_bStatusInventoryGrenadeBorderScreen && m_CameraEffects_InventoryGrenadeBorderScreen)
		{
			modelStack.PushMatrix();
			modelStack.Translate(grenadeDefaultpos.x, grenadeDefaultpos.y, grenadeDefaultpos.z);
			modelStack.Scale(defaultScale.x, defaultScale.y, defaultScale.z);
			switch (Dependent[4]->WeaponType_Enum)
			{
			case CWeaponInfo::SEMI_AUTO:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryPistolBorderScreen);
				break;
			}
			case CWeaponInfo::FULL_AUTO:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryMinigunBorderScreen);
				break;
			}
			case CWeaponInfo::BURST:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryRifleBorderScreen);
				break;
			}
			case CWeaponInfo::SPREAD:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryShotgunBorderScreen);
				break;
			}
			case CWeaponInfo::GRENADE:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryGrenadeBorderScreen);
				break;
			}
			case CWeaponInfo::BLANK:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryBlankBorderScreen);
				break;
			}
			default:
				break;
			}
			modelStack.PopMatrix();
		}
	}
	else if (MouseController::GetInstance()->GetMouseScrollStatus(MouseController::SCROLL_TYPE_YOFFSET) == 2)
	{
		if (m_bStatusInventoryPistolBorderScreen && m_CameraEffects_InventoryPistolBorderScreen)
		{
			modelStack.PushMatrix();
			modelStack.Translate(pistolDefaultpos.x, pistolDefaultpos.y, pistolDefaultpos.z);
			modelStack.Scale(defaultScale.x, defaultScale.y, defaultScale.z);
			switch (Dependent[0]->WeaponType_Enum)
			{
			case CWeaponInfo::SEMI_AUTO:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryPistolBorderScreen);
				break;
			}
			case CWeaponInfo::FULL_AUTO:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryMinigunBorderScreen);
				break;
			}
			case CWeaponInfo::BURST:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryRifleBorderScreen);
				break;
			}
			case CWeaponInfo::SPREAD:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryShotgunBorderScreen);
				break;
			}
			case CWeaponInfo::GRENADE:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryGrenadeBorderScreen);
				break;
			}
			case CWeaponInfo::BLANK:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryBlankBorderScreen);
				break;
			}
			default:
				break;
			}
			modelStack.PopMatrix();
		}
		if (m_bStatusInventoryRifleBorderScreen && m_CameraEffects_InventoryRifleBorderScreen)
		{
			modelStack.PushMatrix();
			modelStack.Translate(rifleDefaultpos.x, rifleDefaultpos.y, rifleDefaultpos.z);
			modelStack.Scale(defaultScale.x, defaultScale.y, defaultScale.z);
			switch (Dependent[1]->WeaponType_Enum)
			{
			case CWeaponInfo::SEMI_AUTO:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryPistolBorderScreen);
				break;
			}
			case CWeaponInfo::FULL_AUTO:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryMinigunBorderScreen);
				break;
			}
			case CWeaponInfo::BURST:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryRifleBorderScreen);
				break;
			}
			case CWeaponInfo::SPREAD:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryShotgunBorderScreen);
				break;
			}
			case CWeaponInfo::GRENADE:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryGrenadeBorderScreen);
				break;
			}
			case CWeaponInfo::BLANK:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryBlankBorderScreen);
				break;
			}
			default:
				break;
			}
			modelStack.PopMatrix();
		}
		if (m_bStatusInventoryMinigunBorderScreen && m_CameraEffects_InventoryMinigunBorderScreen)
		{
			modelStack.PushMatrix();
			modelStack.Translate(minigunDefaultpos.x + 50.f, minigunDefaultpos.y - 25.f, minigunDefaultpos.z);
			modelStack.Scale(enlargeScale.x, enlargeScale.y, enlargeScale.z);
			switch (Dependent[2]->WeaponType_Enum)
			{
			case CWeaponInfo::SEMI_AUTO:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryPistolBorderScreen);
				break;
			}
			case CWeaponInfo::FULL_AUTO:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryMinigunBorderScreen);
				break;
			}
			case CWeaponInfo::BURST:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryRifleBorderScreen);
				break;
			}
			case CWeaponInfo::SPREAD:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryShotgunBorderScreen);
				break;
			}
			case CWeaponInfo::GRENADE:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryGrenadeBorderScreen);
				break;
			}
			case CWeaponInfo::BLANK:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryBlankBorderScreen);
				break;
			}
			default:
				break;
			}
			modelStack.PopMatrix();
		}
		if (m_bStatusInventoryShotgunBorderScreen && m_CameraEffects_InventoryShotgunBorderScreen)
		{
			modelStack.PushMatrix();
			modelStack.Translate(shotgunDefaultpos.x, shotgunDefaultpos.y, shotgunDefaultpos.z);
			modelStack.Scale(defaultScale.x, defaultScale.y, defaultScale.z);
			switch (Dependent[3]->WeaponType_Enum)
			{
			case CWeaponInfo::SEMI_AUTO:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryPistolBorderScreen);
				break;
			}
			case CWeaponInfo::FULL_AUTO:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryMinigunBorderScreen);
				break;
			}
			case CWeaponInfo::BURST:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryRifleBorderScreen);
				break;
			}
			case CWeaponInfo::SPREAD:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryShotgunBorderScreen);
				break;
			}
			case CWeaponInfo::GRENADE:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryGrenadeBorderScreen);
				break;
			}
			case CWeaponInfo::BLANK:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryBlankBorderScreen);
				break;
			}
			default:
				break;
			}
			modelStack.PopMatrix();
		}
		if (m_bStatusInventoryGrenadeBorderScreen && m_CameraEffects_InventoryGrenadeBorderScreen)
		{
			modelStack.PushMatrix();
			modelStack.Translate(grenadeDefaultpos.x, grenadeDefaultpos.y, grenadeDefaultpos.z);
			modelStack.Scale(defaultScale.x, defaultScale.y, defaultScale.z);
			switch (Dependent[4]->WeaponType_Enum)
			{
			case CWeaponInfo::SEMI_AUTO:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryPistolBorderScreen);
				break;
			}
			case CWeaponInfo::FULL_AUTO:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryMinigunBorderScreen);
				break;
			}
			case CWeaponInfo::BURST:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryRifleBorderScreen);
				break;
			}
			case CWeaponInfo::SPREAD:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryShotgunBorderScreen);
				break;
			}
			case CWeaponInfo::GRENADE:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryGrenadeBorderScreen);
				break;
			}
			case CWeaponInfo::BLANK:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryBlankBorderScreen);
				break;
			}
			default:
				break;
			}
			modelStack.PopMatrix();
		}
	}
	else if (MouseController::GetInstance()->GetMouseScrollStatus(MouseController::SCROLL_TYPE_YOFFSET) == 3)
	{
		if (m_bStatusInventoryPistolBorderScreen && m_CameraEffects_InventoryPistolBorderScreen)
		{
			modelStack.PushMatrix();
			modelStack.Translate(pistolDefaultpos.x, pistolDefaultpos.y, pistolDefaultpos.z);
			modelStack.Scale(defaultScale.x, defaultScale.y, defaultScale.z);
			switch (Dependent[0]->WeaponType_Enum)
			{
			case CWeaponInfo::SEMI_AUTO:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryPistolBorderScreen);
				break;
			}
			case CWeaponInfo::FULL_AUTO:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryMinigunBorderScreen);
				break;
			}
			case CWeaponInfo::BURST:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryRifleBorderScreen);
				break;
			}
			case CWeaponInfo::SPREAD:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryShotgunBorderScreen);
				break;
			}
			case CWeaponInfo::GRENADE:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryGrenadeBorderScreen);
				break;
			}
			case CWeaponInfo::BLANK:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryBlankBorderScreen);
				break;
			}
			default:
				break;
			}
			modelStack.PopMatrix();
		}
		if (m_bStatusInventoryRifleBorderScreen && m_CameraEffects_InventoryRifleBorderScreen)
		{
			modelStack.PushMatrix();
			modelStack.Translate(rifleDefaultpos.x, rifleDefaultpos.y, rifleDefaultpos.z);
			modelStack.Scale(defaultScale.x, defaultScale.y, defaultScale.z);
			switch (Dependent[1]->WeaponType_Enum)
			{
			case CWeaponInfo::SEMI_AUTO:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryPistolBorderScreen);
				break;
			}
			case CWeaponInfo::FULL_AUTO:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryMinigunBorderScreen);
				break;
			}
			case CWeaponInfo::BURST:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryRifleBorderScreen);
				break;
			}
			case CWeaponInfo::SPREAD:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryShotgunBorderScreen);
				break;
			}
			case CWeaponInfo::GRENADE:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryGrenadeBorderScreen);
				break;
			}
			case CWeaponInfo::BLANK:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryBlankBorderScreen);
				break;
			}
			default:
				break;
			}
			modelStack.PopMatrix();
		}
		if (m_bStatusInventoryMinigunBorderScreen && m_CameraEffects_InventoryMinigunBorderScreen)
		{
			modelStack.PushMatrix();
			modelStack.Translate(minigunDefaultpos.x, minigunDefaultpos.y, minigunDefaultpos.z);
			modelStack.Scale(defaultScale.x, defaultScale.y, defaultScale.z);
			switch (Dependent[2]->WeaponType_Enum)
			{
			case CWeaponInfo::SEMI_AUTO:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryPistolBorderScreen);
				break;
			}
			case CWeaponInfo::FULL_AUTO:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryMinigunBorderScreen);
				break;
			}
			case CWeaponInfo::BURST:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryRifleBorderScreen);
				break;
			}
			case CWeaponInfo::SPREAD:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryShotgunBorderScreen);
				break;
			}
			case CWeaponInfo::GRENADE:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryGrenadeBorderScreen);
				break;
			}
			case CWeaponInfo::BLANK:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryBlankBorderScreen);
				break;
			}
			default:
				break;
			}
			modelStack.PopMatrix();
		}
		if (m_bStatusInventoryShotgunBorderScreen && m_CameraEffects_InventoryShotgunBorderScreen)
		{
			modelStack.PushMatrix();
			modelStack.Translate(shotgunDefaultpos.x - 50.f, shotgunDefaultpos.y - 25.f, shotgunDefaultpos.z);
			modelStack.Scale(enlargeScale.x, enlargeScale.y, enlargeScale.z);
			switch (Dependent[3]->WeaponType_Enum)
			{
			case CWeaponInfo::SEMI_AUTO:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryPistolBorderScreen);
				break;
			}
			case CWeaponInfo::FULL_AUTO:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryMinigunBorderScreen);
				break;
			}
			case CWeaponInfo::BURST:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryRifleBorderScreen);
				break;
			}
			case CWeaponInfo::SPREAD:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryShotgunBorderScreen);
				break;
			}
			case CWeaponInfo::GRENADE:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryGrenadeBorderScreen);
				break;
			}
			case CWeaponInfo::BLANK:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryBlankBorderScreen);
				break;
			}
			default:
				break;
			}
			modelStack.PopMatrix();
		}
		if (m_bStatusInventoryGrenadeBorderScreen && m_CameraEffects_InventoryGrenadeBorderScreen)
		{
			modelStack.PushMatrix();
			modelStack.Translate(grenadeDefaultpos.x, grenadeDefaultpos.y, grenadeDefaultpos.z);
			modelStack.Scale(defaultScale.x, defaultScale.y, defaultScale.z);
			switch (Dependent[4]->WeaponType_Enum)
			{
			case CWeaponInfo::SEMI_AUTO:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryPistolBorderScreen);
				break;
			}
			case CWeaponInfo::FULL_AUTO:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryMinigunBorderScreen);
				break;
			}
			case CWeaponInfo::BURST:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryRifleBorderScreen);
				break;
			}
			case CWeaponInfo::SPREAD:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryShotgunBorderScreen);
				break;
			}
			case CWeaponInfo::GRENADE:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryGrenadeBorderScreen);
				break;
			}
			case CWeaponInfo::BLANK:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryBlankBorderScreen);
				break;
			}
			default:
				break;
			}
			modelStack.PopMatrix();
		}
	}
	else if (MouseController::GetInstance()->GetMouseScrollStatus(MouseController::SCROLL_TYPE_YOFFSET) == 4)
	{
		if (m_bStatusInventoryPistolBorderScreen && m_CameraEffects_InventoryPistolBorderScreen)
		{
			modelStack.PushMatrix();
			modelStack.Translate(pistolDefaultpos.x, pistolDefaultpos.y, pistolDefaultpos.z);
			modelStack.Scale(defaultScale.x, defaultScale.y, defaultScale.z);
			switch (Dependent[0]->WeaponType_Enum)
			{
			case CWeaponInfo::SEMI_AUTO:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryPistolBorderScreen);
				break;
			}
			case CWeaponInfo::FULL_AUTO:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryMinigunBorderScreen);
				break;
			}
			case CWeaponInfo::BURST:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryRifleBorderScreen);
				break;
			}
			case CWeaponInfo::SPREAD:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryShotgunBorderScreen);
				break;
			}
			case CWeaponInfo::GRENADE:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryGrenadeBorderScreen);
				break;
			}
			case CWeaponInfo::BLANK:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryBlankBorderScreen);
				break;
			}
			default:
				break;
			}
			modelStack.PopMatrix();
		}
		if (m_bStatusInventoryRifleBorderScreen && m_CameraEffects_InventoryRifleBorderScreen)
		{
			modelStack.PushMatrix();
			modelStack.Translate(rifleDefaultpos.x, rifleDefaultpos.y, rifleDefaultpos.z);
			modelStack.Scale(defaultScale.x, defaultScale.y, defaultScale.z);
			switch (Dependent[1]->WeaponType_Enum)
			{
			case CWeaponInfo::SEMI_AUTO:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryPistolBorderScreen);
				break;
			}
			case CWeaponInfo::FULL_AUTO:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryMinigunBorderScreen);
				break;
			}
			case CWeaponInfo::BURST:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryRifleBorderScreen);
				break;
			}
			case CWeaponInfo::SPREAD:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryShotgunBorderScreen);
				break;
			}
			case CWeaponInfo::GRENADE:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryGrenadeBorderScreen);
				break;
			}
			case CWeaponInfo::BLANK:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryBlankBorderScreen);
				break;
			}
			default:
				break;
			}
			modelStack.PopMatrix();
		}
		if (m_bStatusInventoryMinigunBorderScreen && m_CameraEffects_InventoryMinigunBorderScreen)
		{
			modelStack.PushMatrix();
			modelStack.Translate(minigunDefaultpos.x, minigunDefaultpos.y, minigunDefaultpos.z);
			modelStack.Scale(defaultScale.x, defaultScale.y, defaultScale.z);
			switch (Dependent[2]->WeaponType_Enum)
			{
			case CWeaponInfo::SEMI_AUTO:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryPistolBorderScreen);
				break;
			}
			case CWeaponInfo::FULL_AUTO:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryMinigunBorderScreen);
				break;
			}
			case CWeaponInfo::BURST:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryRifleBorderScreen);
				break;
			}
			case CWeaponInfo::SPREAD:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryShotgunBorderScreen);
				break;
			}
			case CWeaponInfo::GRENADE:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryGrenadeBorderScreen);
				break;
			}
			case CWeaponInfo::BLANK:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryBlankBorderScreen);
				break;
			}
			default:
				break;
			}
			modelStack.PopMatrix();
		}
		if (m_bStatusInventoryShotgunBorderScreen && m_CameraEffects_InventoryShotgunBorderScreen)
		{
			modelStack.PushMatrix();
			modelStack.Translate(shotgunDefaultpos.x - 100.f, shotgunDefaultpos.y, shotgunDefaultpos.z);
			modelStack.Scale(defaultScale.x, defaultScale.y, defaultScale.z);
			switch (Dependent[3]->WeaponType_Enum)
			{
			case CWeaponInfo::SEMI_AUTO:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryPistolBorderScreen);
				break;
			}
			case CWeaponInfo::FULL_AUTO:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryMinigunBorderScreen);
				break;
			}
			case CWeaponInfo::BURST:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryRifleBorderScreen);
				break;
			}
			case CWeaponInfo::SPREAD:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryShotgunBorderScreen);
				break;
			}
			case CWeaponInfo::GRENADE:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryGrenadeBorderScreen);
				break;
			}
			case CWeaponInfo::BLANK:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryBlankBorderScreen);
				break;
			}
			default:
				break;
			}
			modelStack.PopMatrix();
		}
		if (m_bStatusInventoryGrenadeBorderScreen && m_CameraEffects_InventoryGrenadeBorderScreen)
		{
			modelStack.PushMatrix();
			modelStack.Translate(grenadeDefaultpos.x - 50.f, grenadeDefaultpos.y - 25.f, grenadeDefaultpos.z);
			modelStack.Scale(enlargeScale.x, enlargeScale.y, enlargeScale.z);
			switch (Dependent[4]->WeaponType_Enum)
			{
			case CWeaponInfo::SEMI_AUTO:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryPistolBorderScreen);
				break;
			}
			case CWeaponInfo::FULL_AUTO:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryMinigunBorderScreen);
				break;
			}
			case CWeaponInfo::BURST:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryRifleBorderScreen);
				break;
			}
			case CWeaponInfo::SPREAD:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryShotgunBorderScreen);
				break;
			}
			case CWeaponInfo::GRENADE:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryGrenadeBorderScreen);
				break;
			}
			case CWeaponInfo::BLANK:
			{
				RenderHelper::RenderMesh(m_CameraEffects_InventoryBlankBorderScreen);
				break;
			}
			default:
				break;
			}
			modelStack.PopMatrix();
		}
	}
	else
	{
		modelStack.PushMatrix();
		if (m_CameraEffects_InventoryPistolBorderScreen)
		{
			modelStack.Translate(pistolDefaultpos.x, pistolDefaultpos.y, pistolDefaultpos.z);
		}
		else if (m_CameraEffects_InventoryRifleBorderScreen)
		{
			modelStack.Translate(rifleDefaultpos.x, rifleDefaultpos.y, rifleDefaultpos.z);
		}
		else if (m_CameraEffects_InventoryMinigunBorderScreen)
		{
			modelStack.Translate(minigunDefaultpos.x, minigunDefaultpos.y, minigunDefaultpos.z);
		}
		else if (m_CameraEffects_InventoryShotgunBorderScreen)
		{
			modelStack.Translate(shotgunDefaultpos.x, shotgunDefaultpos.y, shotgunDefaultpos.z);
		}
		else if (m_CameraEffects_InventoryGrenadeBorderScreen)
		{
			modelStack.Translate(grenadeDefaultpos.x, grenadeDefaultpos.y, grenadeDefaultpos.z);
		}
		modelStack.Scale(defaultScale.x, defaultScale.y, defaultScale.z);
		RenderHelper::RenderMesh(m_CameraEffects_InventoryPistolBorderScreen);
		modelStack.PopMatrix();
	}
}

CInventory* Create::InventoryEffects(const bool m_bAddToLibrary)
{
	CInventory* result = CInventory::GetInstance();
	if (m_bAddToLibrary)
		EntityManager::GetInstance()->AddEntity(result);
	return result;
}