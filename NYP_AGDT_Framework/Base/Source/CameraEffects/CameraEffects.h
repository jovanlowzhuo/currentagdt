#ifndef CAMERA_EFFECTS_H
#define CAMERA_EFFECTS_H

#include "EntityBase.h"
#include "SingletonTemplate.h"
#include "Mesh.h"

class CCameraEffects : public EntityBase, public Singleton<CCameraEffects>
{
public:
	enum SPRITE_RENDERMODE
	{
		MODE_2D,
		MODE_3D,
		NUM_MODE
	};

	CCameraEffects(void);
	virtual ~CCameraEffects(void);

	Mesh* m_CameraEffects_BloodyScreen;
	Mesh* m_CameraEffects_BloodyScreen2;
	Mesh* m_CameraEffects_BloodyScreen3;
	Mesh* m_CameraEffects_BloodyScreen4;
	Mesh* m_CameraEffects_textOnScreen;
	Mesh* m_CameraEffects_ScopeOnScreen;

	// Initialise this class instance
	bool Init(void);

	// Set the BloodScreen
	// 3 different stages for health at 70 50 and 30%
	bool SetBloodScreen(Mesh* aTarget);
	bool SetBloodScreen2(Mesh* aTarget);
	bool SetBloodScreen3(Mesh* aTarget);
	bool SetBloodScreen4(Mesh* aTarget);
	bool SetTextScreen(Mesh* aTarget);
	bool SetScopeScreen(Mesh* aTarget);
	// Get the BloodScreen
	Mesh* GetBloodScreen(void) const;
	Mesh* GetBloodScreen2(void) const;
	Mesh* GetBloodScreen3(void) const;
	Mesh* GetBloodScreen4(void) const;
	Mesh* GetTextScreen(void) const;
	Mesh* GetScopeScreen(void) const;

	// Set rotation angle of CCameraEffect
	bool SetAngle(const int angle);
	// Get rotation angle of CCameraEffect
	int GetAngle(void) const;

	// Set boolean flag for BloodScreen
	bool SetStatus_BloodScreen(const bool m_bStatusBloodScreen);
	bool SetStatus_BloodScreen2(const bool m_bStatusBloodScreen2);
	bool SetStatus_BloodScreen3(const bool m_bStatusBloodScreen3);
	bool SetStatus_BloodScreen4(const bool m_bStatusBloodScreen4);
	bool SetStatus_TextScreen(const bool m_bStatusTextScreen);
	bool SetStatus_ScopeScreen(const bool m_bStatusScopeScreen);
	// Set Countdown Timer for BloodScreen
	bool SetTimer_BloodScreen(const float m_fCountdownTimer_BloodScreen = 2.0f);
	// Get Countdown Timer for BloodScreen
	float GetTimer_BloodScreen(void) const;

	// Update the camera effects
	virtual void Update(const float dt = 0.0333f);
	// Render the UI
	virtual void RenderUI();

protected:
	// Rotation from First Angle
	int m_iAngle;
	// Boolean flag to indicate if the BloodScreen is rendered
	bool m_bStatusBloodScreen , m_bStatusBloodScreen2, m_bStatusBloodScreen3, m_bStatusBloodScreen4,
		 m_bStatusTextScreen  , m_bStatusScopeScreen;
	// Countdown Timer for BloodScreen
	float m_fCountdownTimer_BloodScreen;

	SPRITE_RENDERMODE mode;
};

namespace Create
{
	CCameraEffects* CameraEffects(const bool m_bAddToLibrary);
};
#endif