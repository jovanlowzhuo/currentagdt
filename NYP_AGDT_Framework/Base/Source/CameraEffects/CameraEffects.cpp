#include "CameraEffects.h"
#include "GraphicsManager.h"
#include "RenderHelper.h"
#include "../EntityManager.h"
#include "GL\glew.h"

CCameraEffects::CCameraEffects(void)
	: m_iAngle(-90)
	, m_bStatusBloodScreen(false)
	, m_bStatusBloodScreen2(false)
	, m_bStatusBloodScreen3(false)
	, m_bStatusBloodScreen4(false)
	, m_bStatusTextScreen(false)
	, m_bStatusScopeScreen(false)
	, m_CameraEffects_BloodyScreen(NULL)
	, m_CameraEffects_BloodyScreen2(NULL)
	, m_CameraEffects_BloodyScreen3(NULL)
	, m_CameraEffects_BloodyScreen4(NULL)
	, m_CameraEffects_textOnScreen(NULL)
	, m_CameraEffects_ScopeOnScreen(NULL)
	, m_fCountdownTimer_BloodScreen(2.0f)
	, mode(MODE_2D)
{
	Init();
}
CCameraEffects::~CCameraEffects(void)
{
	if (m_CameraEffects_BloodyScreen)
	{
		delete m_CameraEffects_BloodyScreen;
		m_CameraEffects_BloodyScreen = NULL;
	}
	if (m_CameraEffects_BloodyScreen2)
	{
		delete m_CameraEffects_BloodyScreen2;
		m_CameraEffects_BloodyScreen2 = NULL;
	}
	if (m_CameraEffects_BloodyScreen3)
	{
		delete m_CameraEffects_BloodyScreen3;
		m_CameraEffects_BloodyScreen3 = NULL;
	}
	if (m_CameraEffects_BloodyScreen4)
	{
		delete m_CameraEffects_BloodyScreen4;
		m_CameraEffects_BloodyScreen2 = NULL;
	}
	if (m_CameraEffects_textOnScreen)
	{
		delete m_CameraEffects_textOnScreen;
		m_CameraEffects_textOnScreen = NULL;
	}
	if (m_CameraEffects_ScopeOnScreen)
	{
		delete m_CameraEffects_ScopeOnScreen;
		m_CameraEffects_ScopeOnScreen = NULL;
	}

}

// Initialise this class instance
bool CCameraEffects::Init(void)
{
	m_iAngle = -90;
	position.Set(0.0f, 0.0f, 0.0f);
	scale.Set(800.0f, 600.0f, 100.0f);

	return true;
}

bool CCameraEffects::SetBloodScreen(Mesh* aBloodScreen)
{
	if (aBloodScreen != NULL)
	{
		m_CameraEffects_BloodyScreen = aBloodScreen;
		return true;
	}
	return false;
}
bool CCameraEffects::SetBloodScreen2(Mesh * aBloodScreen2)
{
	if (aBloodScreen2 != NULL)
	{
		m_CameraEffects_BloodyScreen2 = aBloodScreen2;
		return true;
	}
	return false;
}
bool CCameraEffects::SetBloodScreen3(Mesh * aBloodScreen3)
{
	if (aBloodScreen3 != NULL)
	{
		m_CameraEffects_BloodyScreen3 = aBloodScreen3;
		return true;
	}
	return false;
}
bool CCameraEffects::SetBloodScreen4(Mesh * aBloodScreen4)
{
	if (aBloodScreen4 != NULL)
	{
		m_CameraEffects_BloodyScreen4 = aBloodScreen4;
		return true;
	}
	return false;
}
bool CCameraEffects::SetTextScreen(Mesh * aTextScreen)
{
	if (aTextScreen != NULL)
	{
		m_CameraEffects_textOnScreen = aTextScreen;
		return true;
	}
	return false;
}
bool CCameraEffects::SetScopeScreen(Mesh * aScopeScreen)
{
	if (aScopeScreen != NULL)
	{
		m_CameraEffects_ScopeOnScreen = aScopeScreen;
		return true;
	}
	return false;
}
Mesh* CCameraEffects::GetBloodScreen(void) const
{
	return m_CameraEffects_BloodyScreen;
}

Mesh * CCameraEffects::GetBloodScreen2(void) const
{
	return m_CameraEffects_BloodyScreen2;
}

Mesh * CCameraEffects::GetBloodScreen3(void) const
{
	return m_CameraEffects_BloodyScreen3;
}

Mesh * CCameraEffects::GetBloodScreen4(void) const
{
	return m_CameraEffects_BloodyScreen4;
}

Mesh * CCameraEffects::GetTextScreen(void) const
{
	return m_CameraEffects_textOnScreen;
}

Mesh * CCameraEffects::GetScopeScreen(void) const
{
	return m_CameraEffects_ScopeOnScreen;
}


// Set m_iAngle of avatar
bool CCameraEffects::SetAngle(const int m_iAngle)
{
	this->m_iAngle = m_iAngle;
	return true;
}
// Get m_iAngle
int CCameraEffects::GetAngle(void) const
{
	return m_iAngle;
}

// Set boolean flag for BloodScreen
bool CCameraEffects::SetStatus_BloodScreen(const bool m_bStatusBloodScreen)
{
	this->m_bStatusBloodScreen = m_bStatusBloodScreen;
	return true;
}

bool CCameraEffects::SetStatus_BloodScreen2(const bool m_bStatusBloodScreen2)
{
	this->m_bStatusBloodScreen2 = m_bStatusBloodScreen2;
	return true;
}

bool CCameraEffects::SetStatus_BloodScreen3(const bool m_bStatusBloodScreen3)
{
	this->m_bStatusBloodScreen3 = m_bStatusBloodScreen3;
	return true;
}

bool CCameraEffects::SetStatus_BloodScreen4(const bool m_bStatusBloodScreen4)
{
	this->m_bStatusBloodScreen4= m_bStatusBloodScreen4;
	return true;
}

bool CCameraEffects::SetStatus_TextScreen(const bool m_bStatusTextScreen)
{
	this->m_bStatusTextScreen = m_bStatusTextScreen;
	return true;
}

bool CCameraEffects::SetStatus_ScopeScreen(const bool m_bStatusScopeScreen)
{
	this->m_bStatusScopeScreen = m_bStatusScopeScreen;
	return true;
}

// Set Countdown Timer for BloodScreen
bool CCameraEffects::SetTimer_BloodScreen(const float m_fCountdownTimer_BloodScreen)
{
	this->m_fCountdownTimer_BloodScreen = m_fCountdownTimer_BloodScreen;
	return true;
}

// Get Countdown Timer for BloodScreen
float CCameraEffects::GetTimer_BloodScreen(void) const
{
	return m_fCountdownTimer_BloodScreen;
}

// Update the camera effects
void CCameraEffects::Update(const float dt)
{
	if (m_bStatusBloodScreen)
	{
		m_fCountdownTimer_BloodScreen -= dt;
		if (m_fCountdownTimer_BloodScreen <= 0.0f)
		{
			// Set the bool flag for Render BloodScreen to false
			m_bStatusBloodScreen = false;
			// Reset the countdown timer to the default value
			m_fCountdownTimer_BloodScreen = 2.0f;
		}
	}
}

// Render the UI
void CCameraEffects::RenderUI()
{
	if (mode == MODE_3D)
		return;

	MS& modelStack = GraphicsManager::GetInstance()->GetModelStack();

	// Push the current transformation into the modelStack
	modelStack.PushMatrix();
	// Translate the current transformation
	modelStack.Translate(position.x, position.y, position.z +9.5f);
	// Scale the current transformation
	modelStack.Scale(scale.x, scale.y, scale.z);

	// Push the current transformation into the modelStack
	modelStack.PushMatrix();
	// Display the Avatar
	if ((m_bStatusBloodScreen) && (m_CameraEffects_BloodyScreen))
	{
		RenderHelper::RenderMesh(m_CameraEffects_BloodyScreen);
	}
	if ((m_bStatusBloodScreen2) && (m_CameraEffects_BloodyScreen2))
	{
		RenderHelper::RenderMesh(m_CameraEffects_BloodyScreen2);
	}
	if ((m_bStatusBloodScreen3) && (m_CameraEffects_BloodyScreen3))
	{
		RenderHelper::RenderMesh(m_CameraEffects_BloodyScreen3);
	}
	if ((m_bStatusBloodScreen4) && (m_CameraEffects_BloodyScreen4))
	{
		RenderHelper::RenderMesh(m_CameraEffects_BloodyScreen4);
	}
	if ((m_bStatusTextScreen) && (m_CameraEffects_textOnScreen))
	{
		RenderHelper::RenderMesh(m_CameraEffects_textOnScreen);
	}
	if ((m_bStatusScopeScreen) && (m_CameraEffects_ScopeOnScreen))
	{
		RenderHelper::RenderMesh(m_CameraEffects_ScopeOnScreen);
	}
	modelStack.PopMatrix();

	modelStack.PopMatrix();

}

CCameraEffects* Create::CameraEffects(const bool m_bAddToLibrary)
{
	CCameraEffects* result = CCameraEffects::GetInstance();
	if (m_bAddToLibrary)
		EntityManager::GetInstance()->AddEntity(result);
	return result;
}
