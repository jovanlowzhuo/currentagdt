#ifndef SCENE_TEXT_H
#define SCENE_TEXT_H

#include "Scene.h"
#include "Mtx44.h"
#include "PlayerInfo/PlayerInfo.h"
#include "GroundEntity.h"
#include "FPSCamera.h"
#include "Mesh.h"
#include "MatrixStack.h"
#include "GenericEntity.h"
#include "Particle.h"
#include "Enemy/Enemy3D.h"
#include "Projectile\Projectile.h"
#include "RenderHelper.h"
#include "HardwareAbstraction\Keyboard.h"
#include "HardwareAbstraction\Mouse.h"
#include "Minimap\Minimap.h"
#include "CameraEffects\CameraEffects.h"
#include "Collision\Furniture.h"
#include "Collision\EventBox.h"
#include "CameraEffects\CInventory.h"
#include "SpatialPartition/SpatialPartition.h"

class ShaderProgram;
class SceneManager;
class TextEntity;
class Light;
class SceneText : public Scene
{	
public:
	SceneText();
	~SceneText();

	virtual void Init();
	virtual void Update(double dt);
	virtual void Render();
	virtual void Exit();

	void UpdateParticles(double dt);
	void RenderParticles(ParticleObject *particle);
	ParticleObject* GetParticle(void);
	Vector3 GetVerticalPos(int columnInput, int rowInput);
	Vector3 GetHorizontalPos(int columnInput, int rowInput);
	
	void InventoryUpdate();
	void ShaderInit();
	void GameInit(); //Initialise all things that take values from GameSaveData.lua
	void WallInit();
	void EnemyInit();
	void PlayerGunRender();
	virtual void CreateEntities(void);

private:
	static SceneText* sInstance; // The pointer to the object that gets registered
	
	Light* lights[2];
	TextEntity* textObj[5];
	FPSCamera camera;
	ShaderProgram* currProg;
	ShaderProgram* currProg2;
	CPlayerInfo* playerInfo;
	GroundEntity* groundEntity;
	CCameraEffects* theCameraEffects;	// Camera Effects
	CInventory* theInventoryBorderEffects;	// Inventory User Interface
	CKeyboard* theKeyboard;				// Keyboard Abstraction
	CMouse* theMouse;					// Mouse Abstraction
	CEnemy3D* anEnemy3D;				// This is the CEnemy class for 3D use.
	CEventBox* theEventTrigger;			// Event Trigger
	CFurniture* chairThing;

	TextEntity* timertext[1]; // Display timer
	float seconds, minutes;
	float texttimer;
	bool getgun = false;
	float zoomValue; // Zoom
	char KeySaveGame;

	std::vector<ParticleObject*>particleList;

	std::vector<Vector3> tempposListEnemy;
	std::vector<Vector3> tempposListTarget;

	Vector3 m_gravity;
	int m_particleCount;
	unsigned int MAX_PARTICLE;
	
	void SaveGame();
	void GameObjectsInit();
};
#endif