
#include "PlayerInfo.h"
#include <iostream>
#include "MouseController.h"
#include "KeyboardController.h"
#include "Mtx44.h"
#include "../Projectile/Projectile.h"
#include "../WeaponInfo/Pistol.h"
#include "../WeaponInfo/Rifle.h"
#include "../WeaponInfo/Minigun.h"
#include "../WeaponInfo/Shotgun.h"
#include "../EntityManager.h"
#include "../WeaponInfo/GrenadeThrow.h"
#include "../WeaponInfo/Blank.h"
#include "../Lua/LuaInterface.h"

// Allocating and initializing CPlayerInfo's static data member.  
// The pointer is allocated but not the object's constructor.
CPlayerInfo *CPlayerInfo::s_instance = 0;

CPlayerInfo::CPlayerInfo(void)
	: m_dSpeed(40.0)
	, m_dAcceleration(10.0)
	, m_bJumpUpwards(false)
	, m_dJumpSpeed(30.0)
	, m_dJumpAcceleration(-10.0)
	, m_bFallDownwards(false)
	, m_dFallSpeed(0.0)
	, m_dFallAcceleration(-10.0)
	, m_dElapsedTime(0.0)
	, attachedCamera(NULL)
	, m_pTerrain(NULL)
	, theCurrentPosture(STAND)
	, SlowStatus(false)
	, lerpInterPolater(0.f)
	, HeightDiff(0.f)
	, theRecoilStatus(NORMAL)
	, GunPosition(0.f, 0.f, 0.f)
	, totalPitch(0.1f)
	, totalYaw(0.1f)
	, tempRight(0.f, 0.1f, 0.f)
	, m_fCameraSwayAngle(0.f)
	, m_fCameraSwayDeltaAngle(0.1f)
	, m_fCameraSwayAngle_LeftLimit(0.f)
	, m_fCameraSwayAngle_RightLimit(0.f)
	, m_fRollLeft(-100.f)
	, m_fRollRight(100.f)
	, m_bCameraSwayDirection(false)
	, weaponManager(NULL)
	, m_iCurrentWeapon(0)
	, weapons(PISTOL)
	, rifleShot(0)
	, rifleShotBool(false)
	, burstGap(0.0f)
	, keyMoveForward('W')
	, keyMoveBackward('S')
	, keyMoveLeft('A')
	, keyMoveRight('D')
	, FlightMode(false)
{
}

CPlayerInfo::~CPlayerInfo(void)
{
	if (weaponManager)
	{
		for (int i = 0; i < m_iNumOfWeapon; i++)
		{
			delete weaponManager[i];
		}
		delete[] weaponManager;
		weaponManager = NULL;
	}
	m_pTerrain = NULL;

	CSoundEngine::GetInstance()->RemoveSound("Image//pistol2.mp3");

	CSoundEngine::GetInstance()->RemoveSound("Image//shotgun.mp3");

	CSoundEngine::GetInstance()->RemoveSound("Image//rifle.mp3");

	CSoundEngine::GetInstance()->RemoveSound("Image//machinegun.mp3");

	CSoundEngine::GetInstance()->RemoveSound("Image//grenade.mp3");

}

// Initialise this class instance
void CPlayerInfo::Init(void)
{
	// Set the default values
	defaultPosition.Set(0, 0, 10);
	defaultTarget.Set(0, 0, 0);
	defaultUp.Set(0, 1, 0);

	// Set the current values
	position = CLuaInterface::GetInstance()->getVector3Values("PlayerPos");
	target = CLuaInterface::GetInstance()->getVector3Values("PlayerTarget");
	
	if ((position - target).IsZero() == true)
	{
		target = position - 10;
	}

	up.Set(0, 1, 0);

	// Set Boundary
	maxBoundary.Set(1, 1, 1);
	minBoundary.Set(-1, -1, -1);

	m_iCurrentWeapon = CLuaInterface::GetInstance()->getIntValue("PlayerWeapon");

	weaponManager = new CWeaponInfo*[TOTAL_WEAPONS];

	weaponManager[PISTOL] = new CPistol();
	weaponManager[PISTOL]->Init();

	for (int i = 1; i < TOTAL_WEAPONS; i++)
	{
		weaponManager[i] = new CBlank();
		weaponManager[i]->Init();
	}

	PostureHeight = 0.0f;

	m_fCameraSwayAngle = 0.0f;
	m_fCameraSwayDeltaAngle = 1.0f;
	m_bCameraSwayDirection = false;

	EntityManager::GetInstance()->AddEntity(this);
	this->SetPosition(GetPos());
	this->EntityTypeEnum = EntityBase::PLAYER_TYPE;
	this->SetScale(Vector3(3, 3, 3));
	this->SetCollider(true);

	/*************************************************************************
	MUSIC
	**************************************************************************/
	CSoundEngine::GetInstance()->Init();

	CSoundEngine::GetInstance()->AddSound("Pistol", "Image//pistol2.mp3");

	CSoundEngine::GetInstance()->AddSound("shotgun", "Image//shotgun.mp3");

	CSoundEngine::GetInstance()->AddSound("rifle", "Image//rifle.mp3");

	CSoundEngine::GetInstance()->AddSound("machinegun", "Image//machinegun.mp3");

	CSoundEngine::GetInstance()->AddSound("grenade", "Image//grenade.mp3");

}

void CPlayerInfo::InitKeyInputs(void)
{
	keyMoveForward = CLuaInterface::GetInstance()->getCharValue("moveForward");
	keyMoveBackward = CLuaInterface::GetInstance()->getCharValue("moveBackward");
	keyMoveLeft = CLuaInterface::GetInstance()->getCharValue("moveLeft");
	keyMoveRight = CLuaInterface::GetInstance()->getCharValue("moveRight");
}

GroundEntity* CPlayerInfo::GetTerrain(void)
{
	return m_pTerrain;
}

// Returns true if the player is on ground
bool CPlayerInfo::isOnGround(void)
{
	if (m_bJumpUpwards == false && m_bFallDownwards == false)
		return true;

	return false;
}

// Returns true if the player is jumping upwards
bool CPlayerInfo::isJumpUpwards(void)
{
	if (m_bJumpUpwards == true && m_bFallDownwards == false)
		return true;

	return false;
}

// Returns true if the player is on freefall
bool CPlayerInfo::isFreeFall(void)
{
	if (m_bJumpUpwards == false && m_bFallDownwards == true)
		return true;

	return false;
}

// Set the player's status to free fall mode
void CPlayerInfo::SetOnFreeFall(bool isOnFreeFall)
{
	if (isOnFreeFall == true)
	{
		m_bJumpUpwards = false;
		m_bFallDownwards = true;
		m_dFallSpeed = 0.0;
	}
}

// Set the player to jumping upwards
void CPlayerInfo::SetToJumpUpwards(bool isOnJumpUpwards)
{
	if (isOnJumpUpwards == true)
	{
		m_bJumpUpwards = true;
		m_bFallDownwards = false;
		m_dJumpSpeed = 10.0;
	}
}

// Set position
void CPlayerInfo::SetPos(const Vector3& pos)
{
	position = pos;
}

// Set target
void CPlayerInfo::SetTarget(const Vector3& target)
{
	this->target = target;
}

// Set position
void CPlayerInfo::SetUp(const Vector3& up)
{
	this->up = up;
}

// Set m_dJumpAcceleration of the player
void CPlayerInfo::SetJumpAcceleration(const double m_dJumpAcceleration)
{
	this->m_dJumpAcceleration = m_dJumpAcceleration;
}

// Set Fall Acceleration of the player
void CPlayerInfo::SetFallAcceleration(const double m_dFallAcceleration)
{
	this->m_dFallAcceleration = m_dFallAcceleration;
}

// Set the boundary for the player info
void CPlayerInfo::SetBoundary(Vector3 max, Vector3 min)
{
	maxBoundary = max;
	minBoundary = min;
}

// Set the terrain for the player info
void CPlayerInfo::SetTerrain(GroundEntity* m_pTerrain)
{
	if (m_pTerrain != NULL)
	{
		this->m_pTerrain = m_pTerrain;

		SetBoundary(this->m_pTerrain->GetMaxBoundary(), this->m_pTerrain->GetMinBoundary());
	}
}

// Stop the player's movement
void CPlayerInfo::StopVerticalMovement(void)
{
	m_bJumpUpwards = false;
	m_bFallDownwards = false;
}

// Reset this player instance to default
void CPlayerInfo::Reset(void)
{
	// Set the current values to default values
	position = defaultPosition;
	target = defaultTarget;
	up = defaultUp;

	// Stop vertical movement too
	StopVerticalMovement();
}

// Get position x of the player
Vector3 CPlayerInfo::GetPos(void) const
{
	return (position - Vector3(0, HeightDiff, 0));
}

// Get target
Vector3 CPlayerInfo::GetTarget(void) const
{
	return target;
}
// Get Up
Vector3 CPlayerInfo::GetUp(void) const
{
	return up;
}

Vector3 CPlayerInfo::GetGunPos(void) const
{
	return GunPosition;
}

// Get m_dJumpAcceleration of the player
double CPlayerInfo::GetJumpAcceleration(void) const
{
	return m_dJumpAcceleration;
}

// Update Jump Upwards
void CPlayerInfo::UpdateJumpUpwards(double dt)
{
	if (m_bJumpUpwards == false)
		return;

	// Update the jump's elapsed time
	m_dElapsedTime += dt;

	// Update position and target y values
	// Use SUVAT equation to update the change in position and target
	// s = u * t + 0.5 * a * t ^ 2
	position.y += (float)(m_dJumpSpeed * m_dElapsedTime + 0.5 * m_dJumpAcceleration * m_dElapsedTime * m_dElapsedTime);
	target.y += (float)(m_dJumpSpeed * m_dElapsedTime + 0.5 * m_dJumpAcceleration * m_dElapsedTime * m_dElapsedTime);
	// Use this equation to calculate final velocity, v
	// SUVAT: v = u + a * t; v is m_dJumpSpeed AFTER updating using SUVAT where u is 
	// the initial speed and is equal to m_dJumpSpeed
	m_dJumpSpeed = m_dJumpSpeed + m_dJumpAcceleration * m_dElapsedTime;
	// Check if the jump speed is less than zero, then it should be falling
	if (m_dJumpSpeed < 0.0)
	{
		m_dJumpSpeed = 0.0;
		m_bJumpUpwards = false;
		m_dFallSpeed = 0.0;
		if (FlightMode == true)
		{
			m_bFallDownwards = false;
		}
		else
		{
			m_bFallDownwards = true;
		}
		m_dElapsedTime = 0.0;
	}
}

// Update FreeFall
void CPlayerInfo::UpdateFreeFall(double dt)
{
	if (m_bFallDownwards == false)
		return;

	// Update the jump's elapsed time
	m_dElapsedTime += dt;

	// Update position and target y values.
	// Use SUVAT equation to update the change in position and target
	// s = u * t + 0.5 * a * t ^ 2
	position.y += (float)(m_dFallSpeed * m_dElapsedTime + 0.5 * m_dJumpAcceleration * m_dElapsedTime * m_dElapsedTime);
	target.y += (float)(m_dFallSpeed * m_dElapsedTime + 0.5 * m_dJumpAcceleration * m_dElapsedTime * m_dElapsedTime);
	// Use this equation to calculate final velocity, v
	// SUVAT: v = u + a * t;
	// v is m_dJumpSpeed AFTER updating using SUVAT where u is the initial speed and is equal to m_dJumpSpeed
	m_dFallSpeed = m_dFallSpeed + m_dFallAcceleration * m_dElapsedTime;
	// Check if the jump speed is below terrain, then it should be reset to terrain height
	if (position.y < m_pTerrain->GetTerrainHeight(position).y)
	{
		Vector3 viewDirection = target - position;
		position.y = m_pTerrain->GetTerrainHeight(position).y;
		target = position + viewDirection;
		m_dFallSpeed = 0.0;
		m_bFallDownwards = false;
		m_dElapsedTime = 0.0;
	}
}

/********************************************************************************
 Hero Update
 ********************************************************************************/
void CPlayerInfo::Update(double dt)
{
	// Update the weapons
	if (weaponManager[m_iCurrentWeapon])
	{
		weaponManager[m_iCurrentWeapon]->Update(dt);
	}

	Vector3 viewUV = (target - position).Normalized();
	Constrain();
	UpdateJumpUpwards(dt);
	UpdateFreeFall(dt);

	 //Do camera sway
	if (m_fCameraSwayAngle != 0.0f)
	{
		Mtx44 rotation;
		if (m_bCameraSwayDirection == false)
			rotation.SetToRotation(-m_fCameraSwayDeltaAngle, viewUV.x, viewUV.y, viewUV.z);
		else if (m_bCameraSwayDirection == true)
			rotation.SetToRotation(m_fCameraSwayDeltaAngle, viewUV.x, viewUV.y, viewUV.z);
		up = rotation * up;
	}

	//Crouch
	lerpInterPolater += 5.f * (float)dt;
	lerpInterPolater = Math::ResetForMore(lerpInterPolater, 0.1f, 0.5f);

	HeightDiff += Math::lerp(HeightDiff, PostureHeight, lerpInterPolater);
	switch (theCurrentPosture)
	{
	case STAND:
		if (HeightDiff < 0.1f)
		{
			HeightDiff = PostureHeight;
		}
		break;
	case CROUCH:
		if (HeightDiff > 4.9f)
		{
			HeightDiff = PostureHeight;
		}
		break;
	case PRONE:
		if (HeightDiff > 7.9f)
		{
			HeightDiff = PostureHeight;
		}
		break;
	default:
		break;
	}

	//Recoil
	switch (theRecoilStatus)
	{
	case RISE:
		RecoilValue += 5.f * (float)dt;
		if (RecoilValue > 0.3f)
		{
			RecoilValue = 0.3f;
			theRecoilStatus = RECOIL::FALL;
		}
		break;
	case FALL:
		RecoilValue -= 2.f * (float)dt;
		if (RecoilValue < 0.f)
		{
			RecoilValue = 0.f;
			theRecoilStatus = RECOIL::NORMAL;
		}
		break;
	case NORMAL:
		RecoilValue = 0.f;
		break;
	default:
		break;
	}

	GunPosition = GetPos() + viewUV.Normalized();

	CMinimap::GetInstance()->SetAngle((int)atan2(viewUV.z, viewUV.x) * (int)57.2883513685549146);
	SetPosition(GetPos());

	// If a camera is attached to this playerInfo class, then update it
	if (attachedCamera)
	{
		attachedCamera->SetCameraPos(position + Vector3(0.f, HeightDiff, 0.f));
		attachedCamera->SetCameraTarget(target + Vector3(0.f, HeightDiff + -RecoilValue, 0.f));
		attachedCamera->SetCameraUp(up);
	}

	// Update the position if the WASD buttons were activated
	if (KeyboardController::GetInstance()->IsKeyDown(keyMoveForward) || KeyboardController::GetInstance()->IsKeyDown(keyMoveLeft) || KeyboardController::GetInstance()->IsKeyDown(keyMoveBackward) || KeyboardController::GetInstance()->IsKeyDown(keyMoveRight))
	{
		Vector3 viewVector = target - position;
		Vector3 rightUV;
		if (KeyboardController::GetInstance()->IsKeyDown(keyMoveForward))
		{
			Move_FrontBack(dt, true);
		}
		else if (KeyboardController::GetInstance()->IsKeyDown(keyMoveBackward))
		{
			Move_FrontBack(dt, false);
			StopSway(dt);
		}
		if (KeyboardController::GetInstance()->IsKeyDown(keyMoveLeft))
		{
			Move_LeftRight(dt, true);
			StopSway(dt);
		}
		else if (KeyboardController::GetInstance()->IsKeyDown(keyMoveRight))
		{
			Move_LeftRight(dt, false);
			StopSway(dt);
		}
		// Constrain the position
		Constrain();
		// Update the target
		target = position + viewVector;
	}
	if ((KeyboardController::GetInstance()->IsKeyDown(keyMoveForward) && (KeyboardController::GetInstance()->IsKeyDown(VK_SHIFT))))
	{
		Move_FrontBack(dt, true, 2.f);
	}
	else if (KeyboardController::GetInstance()->IsKeyReleased(keyMoveForward))
	{
		StopSway(dt);
	}
}
bool CPlayerInfo::Roll_Left(const double deltaTime, const bool direction, const float speedMultiplier)
{
	if (m_bCameraSwayDirection == false)
	{
		m_fCameraSwayAngle -= m_fCameraSwayDeltaAngle;
		if (m_fCameraSwayAngle < m_fCameraSwayAngle_LeftLimit * speedMultiplier)
			m_bCameraSwayDirection = !m_bCameraSwayDirection;
	}
	else
	{
		m_fCameraSwayAngle += m_fCameraSwayDeltaAngle;
		if (m_fCameraSwayAngle > m_fCameraSwayAngle_RightLimit * speedMultiplier)
			m_bCameraSwayDirection = !m_bCameraSwayDirection;
	}
	Vector3 viewVector = (target - position).Normalized();
	if (direction)
	{
		position += viewVector * (float)m_dSpeed * speedMultiplier * (float)deltaTime;
		// Constrain the position
		Constrain();
		// Update the target
		target = position + viewVector;
		return true;
	}
	else
	{
		position -= viewVector * (float)m_dSpeed * (float)deltaTime;
		// Constrain the position
		Constrain();
		// Update the target
		target = position + viewVector;
		return true;
	}
	return false;
}

// Detect and process front / back movement on the controller
bool CPlayerInfo::Move_FrontBack(const double deltaTime, const bool direction, const float speedMultiplier)
{
	// Add camera sway
	if (m_bCameraSwayDirection == false)
	{
		m_fCameraSwayAngle -= m_fCameraSwayDeltaAngle;
		if (m_fCameraSwayAngle < m_fCameraSwayAngle_LeftLimit * speedMultiplier)
			m_bCameraSwayDirection = !m_bCameraSwayDirection;
	}
	else
	{
		m_fCameraSwayAngle += m_fCameraSwayDeltaAngle;
		if (m_fCameraSwayAngle > m_fCameraSwayAngle_RightLimit * speedMultiplier)
			m_bCameraSwayDirection = !m_bCameraSwayDirection;
	}

	Vector3 viewVector = (target - position).Normalized();
	if (direction)
	{
		position += viewVector * (float)m_dSpeed * speedMultiplier * (float)deltaTime;
		// Constrain the position
		Constrain();
		// Update the target
		target = position + viewVector;
		return true;
	}
	else
	{
		position -= viewVector * (float)m_dSpeed * (float)deltaTime;
		// Constrain the position
		Constrain();
		// Update the target
		target = position + viewVector;
		return true;
	}
	return false;
}

// Detect and process left / right movement on the controller
bool CPlayerInfo::Move_LeftRight(const double deltaTime, const bool direction, const float speedMultiplier)
{
	Vector3 viewVector = target - position;
	Vector3 rightUV;
	if (direction)
	{
		rightUV = (viewVector.Normalized()).Cross(up);
		rightUV.y = 0;
		rightUV.Normalize();
		position -= rightUV * (float)m_dSpeed *(float)deltaTime;
		// Update the target
		target = position + viewVector;
		return true;
	}
	else
	{
		rightUV = (viewVector.Normalized()).Cross(up);
		rightUV.y = 0;
		rightUV.Normalize();
		position += rightUV * (float)m_dSpeed * (float)deltaTime;
		// Update the target
		target = position + viewVector;
		return true;
	}
	return false;
}

// Detect and process look up / down on the controller
bool CPlayerInfo::Look_UpDown(const double deltaTime, const bool direction, const float speedMultiplier)
{
	Vector3 viewUV = (target - position).Normalized();
	Vector3 rightUV;
	float pitch;

	if (direction)
	{
		pitch = (float)(-m_dSpeed * speedMultiplier * (float)deltaTime);
	}
	else
	{
		pitch = (float)(m_dSpeed * speedMultiplier * (float)deltaTime);
	}

	totalPitch += pitch;
	rightUV = viewUV.Cross(up);
	rightUV.y = 0;
	rightUV.Normalize();
	up = rightUV.Cross(viewUV).Normalized();
	Mtx44 rotation;
	rotation.SetToRotation(pitch, rightUV.x, rightUV.y, rightUV.z);

	viewUV = rotation * viewUV;
	target = position + viewUV;
	tempRight = rightUV;
	return true;
}

// Detect and process look left / right on the controller
bool CPlayerInfo::Look_LeftRight(const double deltaTime, const bool direction, const float speedMultiplier)
{
	Vector3 viewUV = (target - position).Normalized();
	Vector3 rightUV;
	float yaw;

	if (direction)
	{
		yaw = (float)(-m_dSpeed * speedMultiplier * (float)deltaTime);
	}
	else
	{
		yaw = (float)(m_dSpeed * speedMultiplier * (float)deltaTime);
	}
	totalYaw += yaw;
	Mtx44 rotation;
	rotation.SetToRotation(yaw, 0, 1, 0);
	viewUV = rotation * viewUV;
	target = position + viewUV;
	rightUV = viewUV.Cross(up);
	rightUV.y = 0;
	rightUV.Normalize();
	up = rightUV.Cross(viewUV).Normalized();

	tempRight = rightUV;
	return true;
}

// Stop sway
bool CPlayerInfo::StopSway(const double deltaTime)
{
	m_bCameraSwayDirection = false;
	m_fCameraSwayAngle = 0.0f;
	up = Vector3(0.0f, 1.0f, 0.0f);
	return true;
}

// Constrain the position within the borders
void CPlayerInfo::Constrain(void)
{
	// Constrain player within the boundary
	if (position.x > maxBoundary.x - 1.0f)
		position.x = maxBoundary.x - 1.0f;
	if (FlightMode != true)
	{
		if (position.y > maxBoundary.y - 1.0f)
		{
			position.y = maxBoundary.y - 1.0f;
			m_dJumpSpeed = 0.0;
			m_bJumpUpwards = false;
			m_dFallSpeed = 0.0;
			m_bFallDownwards = true;
			m_dElapsedTime = 0.0;
		}
	}
	if (position.z > maxBoundary.z - 1.0f)
		position.z = maxBoundary.z - 1.0f;
	if (position.x < minBoundary.x + 1.0f)
		position.x = minBoundary.x + 1.0f;
	if (position.y < minBoundary.y + 1.0f)
		position.y = minBoundary.y + 1.0f;
	if (position.z < minBoundary.z + 1.0f)
		position.z = minBoundary.z + 1.0f;

	// if the player is not jumping nor falling, then adjust his y position
	if ((m_bJumpUpwards == false) && (m_bFallDownwards == false))
	{
		if (FlightMode != true)
		{
			// if the y position is not equal to terrain height at that position, then update y position to the terrain height
			if (position.y != m_pTerrain->GetTerrainHeight(position).y)
				(position.y = m_pTerrain->GetTerrainHeight(position).y);
		}

		Vector3 viewDirection = target - position;
		switch (theCurrentPosture)
		{
		case STAND:
			target = position + viewDirection;
			PostureHeight = 0.0f;
			m_dSpeed = 40.0f;
			break;
		case CROUCH:
			target = position + viewDirection;
			PostureHeight = 5.0f;
			m_dSpeed = 30.0f;
			break;
		case PRONE:
			target = position + viewDirection;
			PostureHeight = 8.0f;
			m_dSpeed = 10.0f;
			break;
		default:
			break;
		}
	}
}

// Reload current weapon
bool CPlayerInfo::ReloadWeapon(void)
{
	if (weaponManager[m_iCurrentWeapon])
	{
		weaponManager[m_iCurrentWeapon]->Reload();
		return true;
	}
	return false;
}

// Change current weapon
bool CPlayerInfo::ChangeWeapon(void)
{
	switch (m_iCurrentWeapon)
	{
	case PISTOL:
		CWeaponInfo::SEMI_AUTO;
		break;
	case RIFLE:
		CWeaponInfo::BURST;
		break;
	case MINIGUN:
		CWeaponInfo::FULL_AUTO;
		break;
	case SHOTGUN:
		CWeaponInfo::SPREAD;
		break;
	case GRENADE:
		CWeaponInfo::GRENADE;
		break;
	}
	return true;
}

// Get Current Weapon
int CPlayerInfo::GetWeapon(void) const
{
	return m_iCurrentWeapon;
}

// Discharge Primary Weapon
bool CPlayerInfo::DischargePrimaryWeapon(const double deltaTime)
{

	if (weaponManager[m_iCurrentWeapon])
	{
		if (weaponManager[m_iCurrentWeapon]->GetWeaponType() == CWeaponInfo::SEMI_AUTO)
		{
			if (weaponManager[m_iCurrentWeapon]->Discharge(position - Vector3(0, PostureHeight, 0), target - Vector3(0, PostureHeight, 0), this) == true)
			{
				theRecoilStatus = RECOIL::RISE;
				CSoundEngine::GetInstance()->PlayASound("Pistol");
			}
		}

		if ((weaponManager[m_iCurrentWeapon]->GetWeaponType() == CWeaponInfo::BURST) && (rifleShotBool != true) && (burstGap == 0.0f))
		{
			if (weaponManager[m_iCurrentWeapon]->Discharge(position - Vector3(0, PostureHeight, 0), target - Vector3(0, PostureHeight, 0), this) == true)
			{
				theRecoilStatus = RECOIL::RISE;
				rifleShotBool = true;
				CSoundEngine::GetInstance()->PlayASound("rifle");
				rifleShot++;
			}
		}

		if (weaponManager[m_iCurrentWeapon]->GetWeaponType() == CWeaponInfo::FULL_AUTO)
		{
			if (weaponManager[m_iCurrentWeapon]->Discharge(position - Vector3(0, PostureHeight, 0), target - Vector3(0, PostureHeight, 0), this) == true)
			{
				CSoundEngine::GetInstance()->PlayASound("machinegun");
			}
		}

		if (weaponManager[m_iCurrentWeapon]->GetWeaponType() == CWeaponInfo::SPREAD)
		{
			if (weaponManager[m_iCurrentWeapon]->DischargeSpread(position - Vector3(0, PostureHeight, 0), target - Vector3(0, PostureHeight, 0), this) == true)
			{
				theRecoilStatus = RECOIL::RISE;
				CSoundEngine::GetInstance()->PlayASound("shotgun");
			}
		}

		if (weaponManager[m_iCurrentWeapon]->GetWeaponType() == CWeaponInfo::GRENADE)
		{
			if (weaponManager[m_iCurrentWeapon]->DischargeGrenade(position - Vector3(0, PostureHeight, 0), target - Vector3(0, PostureHeight, 0), this) == true)
			{
				CSoundEngine::GetInstance()->PlayASound("grenade");
			}
		}
		return true;
	}
	return false;
}

// Discharge Secondary Weapon
bool CPlayerInfo::DischargeSecondaryWeapon(const double deltaTime)
{
	if (weaponManager[m_iCurrentWeapon])
	{
		weaponManager[m_iCurrentWeapon]->Discharge(position, target, this);
		return true;
	}
	return false;
}

// Crouch button
bool CPlayerInfo::Crouch(const double deltaTime)
{
	// Check if there is a need to change posture
	theCurrentPosture = (CURRENT_POSTURE)(theCurrentPosture + 1);
	if (theCurrentPosture == NUM_POSTURE)
		theCurrentPosture = STAND;
	return false;
}

bool CPlayerInfo::GrenadeTrigger(CEventBox * setTrigger)
{
	this->Dependent = setTrigger;
	return false;
}

//Time button
bool CPlayerInfo::SlowMode(const double deltaTime)
{
	if (SlowStatus == false)
	{
		SlowStatus = true;
	}
	else if (SlowStatus == true)
	{
		SlowStatus = false;
	}
	return false;
}

void CPlayerInfo::UpdateLoop(const double deltaTime)
{
	// Update rifle
	if (weaponManager[m_iCurrentWeapon])
	{
		weaponManager[m_iCurrentWeapon]->Update(deltaTime);
		if ((weaponManager[m_iCurrentWeapon]->GetWeaponType() == CWeaponInfo::BURST) && (rifleShotBool == true) && (weaponManager[m_iCurrentWeapon]->GetCanFire() == true))
		{
			if (weaponManager[m_iCurrentWeapon]->Discharge(position - Vector3(0, PostureHeight, 0), target - Vector3(0, PostureHeight + -RecoilValue, 0), this) == true)
			{
				theRecoilStatus = RECOIL::RISE;
				rifleShot++;
			}
		}
	}

	if (Dependent != NULL)
	{
		if (Dependent->AssociatedTrigger->GetEvent() == true)
		{
			if (weaponManager[GRENADE]->GetWeaponType() != CWeaponInfo::GRENADE)
			{
				weaponManager[GRENADE] = new CGrenadeThrow;
				weaponManager[GRENADE]->Init();
				Dependent->SetIsDone(true);
				Dependent = NULL;
			}
		}
	}

	Vector3 viewUV = (target - position).Normalized();
	Constrain();
	UpdateJumpUpwards(deltaTime);
	UpdateFreeFall(deltaTime);

	// Do camera sway
	if (m_fCameraSwayAngle != 0.0f)
	{
		Mtx44 rotation;
		if (m_bCameraSwayDirection == false)
			rotation.SetToRotation(-m_fCameraSwayDeltaAngle, viewUV.x, viewUV.y, viewUV.z);
		else if (m_bCameraSwayDirection == true)
			rotation.SetToRotation(m_fCameraSwayDeltaAngle, viewUV.x, viewUV.y, viewUV.z);
		up = rotation * up;
	}

	//Crouch
	lerpInterPolater += 5.f * (float)deltaTime;
	lerpInterPolater = Math::ResetForMore(lerpInterPolater, 0.0f, 0.25f);
	HeightDiff += Math::lerp(HeightDiff, PostureHeight, lerpInterPolater);
	switch (theCurrentPosture)
	{
	case STAND:
		if (HeightDiff < 0.1)
		{
			HeightDiff = PostureHeight;
		}
		break;
	case CROUCH:
		if (HeightDiff > 4.9)
		{
			HeightDiff = PostureHeight;
		}
		break;
	case PRONE:
		if (HeightDiff > 7.9)
		{
			HeightDiff = PostureHeight;
		}
		break;
	default:
		break;
	}

	if (rifleShot > 2)
	{
		rifleShot = 0;
		rifleShotBool = false;
		burstGap += deltaTime;
	}

	if (burstGap > 1.5f)
	{
		burstGap = 0.0f;
	}
	else if (burstGap != 0.0f)
	{
		burstGap += deltaTime;
	}

	GunPosition = GetPos() + viewUV.Normalized();
	SetPosition(GetPos());

	CMinimap::GetInstance()->SetAngle((int)atan2(viewUV.z, viewUV.x) * (int)57.2883513685549146);

	// If a camera is attached to this playerInfo class, then update it
	if (attachedCamera)
	{
		attachedCamera->SetCameraPos(position - Vector3(0, HeightDiff, 0));
		attachedCamera->SetCameraTarget(target - Vector3(0, HeightDiff + -RecoilValue, 0));
		attachedCamera->SetCameraUp(up);
	}
}

void CPlayerInfo::AddGun(int GunToAdd)
{
	switch (GunToAdd)
	{
	case 0:
	{
		if ((weaponManager[m_iCurrentWeapon]->GetWeaponType() != CWeaponInfo::SEMI_AUTO))
		{
			weaponManager[m_iCurrentWeapon] = new CPistol;
			weaponManager[m_iCurrentWeapon]->Init();
		}
		break;
	}
	case 1:
	{
		if ((weaponManager[m_iCurrentWeapon]->GetWeaponType() != CWeaponInfo::BURST))
		{
			weaponManager[m_iCurrentWeapon] = new CRifle;
			weaponManager[m_iCurrentWeapon]->Init();
		}
		break;
	}
	case 2:
	{
		if ((weaponManager[m_iCurrentWeapon]->GetWeaponType() != CWeaponInfo::FULL_AUTO))
		{
			weaponManager[m_iCurrentWeapon] = new CMinigun;
			weaponManager[m_iCurrentWeapon]->Init();
		}
		break;
	}
	case 3:
	{
		if ((weaponManager[m_iCurrentWeapon]->GetWeaponType() != CWeaponInfo::SPREAD))
		{
			weaponManager[m_iCurrentWeapon] = new CShotgun;
			weaponManager[m_iCurrentWeapon]->Init();
		}
		break;
	}
	case 4:
	{
		if ((weaponManager[m_iCurrentWeapon]->GetWeaponType() != CWeaponInfo::GRENADE))
		{
			weaponManager[m_iCurrentWeapon] = new CGrenadeThrow;
			weaponManager[m_iCurrentWeapon]->Init();
		}
		break;
	}
	default:
		break;
	}
}

void CPlayerInfo::AttachCamera(FPSCamera* _cameraPtr)
{
	attachedCamera = _cameraPtr;
}

void CPlayerInfo::DetachCamera()
{
	attachedCamera = nullptr;
}

CWeaponInfo CPlayerInfo::ReturnPrimaryWeaponData()
{
	return *weaponManager[m_iCurrentWeapon];
}

float CPlayerInfo::ReturnRatioBullet()
{
	return weaponManager[m_iCurrentWeapon]->MagRatio(*weaponManager[m_iCurrentWeapon]);
}

void CPlayerInfo::ToggleFlight()
{
	if (FlightMode == true)
	{
		FlightMode = false;
	}
	else
	{
		FlightMode = true;
	}
}