#pragma once
#include "Vector3.h"
#include "../FPSCamera.h"
#include "../GroundEntity.h"
#include "../WeaponInfo/WeaponInfo.h"
#include "../Minimap/Minimap.h"
#include "Collider\Collider.h"
#include "../CameraEffects/CInventory.h"
#include <fstream>
#include <string>
#include <sstream>
#include "../SoundEngine.h"
#include "../Collision/EventBox.h"
using namespace std;

class CPlayerInfo
	: public EntityBase
{
protected:
	static CPlayerInfo *s_instance;
	CPlayerInfo(void);

	// The postures of the FPS player/camera
	enum CURRENT_POSTURE
	{
		STAND,
		CROUCH,
		PRONE,
		NUM_POSTURE,
	};
	CURRENT_POSTURE theCurrentPosture;

	// The recoil of the FPS player/camera
	enum RECOIL
	{
		NORMAL,
		RISE,
		FALL,
	};
	RECOIL theRecoilStatus;

public:
	// The weapons selections
	enum WEAPONS
	{
		PISTOL,
		RIFLE,
		MINIGUN,
		SHOTGUN,
		GRENADE,
		TOTAL_WEAPONS
	};
	WEAPONS weapons;

	static CPlayerInfo *GetInstance()
	{
		if (!s_instance)
			s_instance = new CPlayerInfo;
		return s_instance;
	}
	static bool DropInstance()
	{
		if (s_instance)
		{
			delete s_instance;
			s_instance = NULL;
			return true;
		}
		return false;
	}
	~CPlayerInfo(void);

	// Initialise this class instance
	void Init(void);
	//Init key inputs
	void InitKeyInputs(void);
	// Returns true if the player is on ground
	bool isOnGround(void);
	// Returns true if the player is jumping upwards
	bool isJumpUpwards(void);
	// Returns true if the player is on freefall
	bool isFreeFall(void);
	// Set the player's status to free fall mode
	void SetOnFreeFall(bool isOnFreeFall);
	// Set the player to jumping upwards
	void SetToJumpUpwards(bool isOnJumpUpwards);
	// Stop the player's vertical movement
	void StopVerticalMovement(void);
	// Reset this player instance to default
	void Reset(void);
	GroundEntity* GetTerrain(void);
	// Set position
	void SetPos(const Vector3& pos);
	// Set target
	void SetTarget(const Vector3& target);
	// Set Up
	void SetUp(const Vector3& up);
	// Set m_dJumpSpeed of the player
	void SetJumpSpeed(const double m_dJumpSpeed);
	// Set m_dJumpAcceleration of the player
	void SetJumpAcceleration(const double m_dJumpAcceleration);
	// Set Fall Speed of the player
	void SetFallSpeed(const double m_dFallSpeed);
	// Set Fall Acceleration of the player
	void SetFallAcceleration(const double m_dFallAcceleration);
	// Set the boundary for the player info
	void SetBoundary(Vector3 max, Vector3 min);
	// Set the terrain for the player info
	void SetTerrain(GroundEntity* m_pTerrain);

	// Get position
	Vector3 GetPos(void) const;
	// Get target
	Vector3 GetTarget(void) const;
	// Get Up
	Vector3 GetUp(void) const;
	// Get Jump Speed of the player
	double GetJumpSpeed(void) const;
	// Get Jump Acceleration of the player
	double GetJumpAcceleration(void) const;
	// Get Fall Speed of the player
	double GetFallSpeed(void) const;
	// Get Fall Acceleration of the player
	double GetFallAcceleration(void) const;
	

	// Update Jump Upwards
	void UpdateJumpUpwards(double dt = 0.0333f);
	// Update FreeFall
	void UpdateFreeFall(double dt = 0.0333f);
	// Update
	void Update(double dt = 0.0333f);

	// Detect and process front / back movement on the controller
	bool Move_FrontBack(const double deltaTime, const bool direction, const float speedMultiplier = 1.0f);
	// Detect and process left / right movement on the controller
	bool Move_LeftRight(const double deltaTime, const bool direction, const float speedMultiplier = 1.0f);
	// Detect and process look up / down on the controller
	bool Look_UpDown(const double deltaTime, const bool direction, const float speedMultiplier = 1.0f);
	// Detect and process look left / right on the controller
	bool Look_LeftRight(const double deltaTime, const bool direction, const float speedMultiplier = 1.0f);
	// Detect and prcoess roll on the controller
	bool Roll_Left(const double deltaTime, const bool direction, const float speedMultiplier = 1.0f);
	// Stop sway
	bool StopSway(const double deltaTime);
	bool GrenadeTrigger(CEventBox* setTrigger);

	// Reload current weapon by clips
	bool ReloadWeapon(void);
	// Change current weapon
	bool ChangeWeapon(void);
	// Get Current Weapon
	int GetWeapon(void) const;
	// Discharge Primary Weapon
	bool DischargePrimaryWeapon(const double deltaTime);
	// Discharge Secondary Weapon
	bool DischargeSecondaryWeapon(const double deltaTime);

	// Constrain the position within the borders
	void Constrain(void);

	// Handling Camera
	void AttachCamera(FPSCamera* _cameraPtr);
	void DetachCamera();

	// Scrollable weapon switching
	int m_iCurrentWeapon;
	const int m_iNumOfWeapon = 3;
	CWeaponInfo** weaponManager;

	// Camera Sway
	float m_fCameraSwayAngle;
	float m_fCameraSwayDeltaAngle;
	float m_fCameraSwayAngle_LeftLimit, m_fCameraSwayAngle_RightLimit;	// The limits for left and right sway
	bool m_bCameraSwayDirection; // false = left, true = right
	float m_fRollLeft, m_fRollRight;
	bool m_bRollLeft, m_bRollRight;

	bool Crouch(const double deltaTime);

	bool SlowMode(const double deltaTime);

	void UpdateLoop(const double deltaTime);

	void AddGun(int GunToAdd);

	CWeaponInfo ReturnPrimaryWeaponData();

	float ReturnRatioBullet();

	void ToggleFlight();

	Vector3 GetGunPos(void) const;

	bool SlowStatus;
	Vector3 GunPosition;

	float totalPitch;
	float totalYaw;
	Vector3 tempRight;

	int rifleShot;
	bool rifleShotBool;
	double burstGap;

private:
	Vector3 defaultPosition, defaultTarget, defaultUp;
	Vector3 position, target, up;
	Vector3 maxBoundary, minBoundary;
	GroundEntity* m_pTerrain;

	double m_dSpeed;
	double m_dAcceleration;

	bool m_bJumpUpwards;
	double m_dJumpSpeed;
	double m_dJumpAcceleration;

	double m_dFallSpeed;
	bool m_bFallDownwards;
	double m_dFallAcceleration;

	double m_dElapsedTime;

	FPSCamera* attachedCamera;

	CEventBox* Dependent;

	float PostureHeight;

	float lerpInterPolater;
	float HeightDiff;

	float RecoilValue;
	bool FlightMode;

	// Key to move the player
	char keyMoveForward;
	char keyMoveBackward;
	char keyMoveLeft;
	char keyMoveRight;
};