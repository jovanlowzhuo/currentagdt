#ifndef ENTITY_MANAGER_H
#define ENTITY_MANAGER_H

#include "SingletonTemplate.h"
#include <list>
#include "Vector3.h"
#include "Projectile/Projectile.h"
#include "SpatialPartition/SpatialPartition.h"
#include "GameStateManagement/Highscore.h"

class EntityBase;

class EntityManager : public Singleton<EntityManager>
{
	friend Singleton<EntityManager>;
public:
	void Update(double _dt);
	void Render();
	void RenderUI();

	void AddEntity(EntityBase* _newEntity , bool bAddToSpatitialPartition = false);
	bool RemoveEntity(EntityBase* _existingEntity);

	void ClearProjectile();

	std::list<EntityBase*> entityList;
	bool CheckForCollision(std::list<EntityBase*>::iterator dis, double dt);

	void SetSpatialPartition(CSpatialPartition* SetSpatialPartition);
	void ClearEntities();
	bool BuyingCheck(int Cost);

	int ScoreInput;

private:
	EntityManager();
	virtual ~EntityManager();

	bool CheckForCollisionType(EntityBase * ThisEntity, EntityBase * ThatEntity, double dt);

	

	// Check for overlap
	bool CheckOverlap(Vector3 thisMinAABB, Vector3 thisMaxAABB, Vector3 thatMinAABB, Vector3 thatMaxAABB);
	bool MarkForDeletion(EntityBase * _existingEntity);
	// Check if this entity's bounding sphere collided with that entity's bounding sphere 
	bool CheckSphereCollision(EntityBase *ThisEntity, EntityBase *ThatEntity);
	// Check if this entity collided with another entity, but both must have collider
	bool CheckAABBCollision(EntityBase *ThisEntity, EntityBase *ThatEntity);
	bool CheckForBulletCollision(EntityBase * ThisEntity, EntityBase * ThatEntity, double dt);
	// Check if any Collider is colliding with another Collider
	//bool CheckForCollision(std::list<EntityBase*>::iterator dis);

	CSpatialPartition* theSpatialPartition;
	
	bool BulletCollideEnemy;
};
#endif // ENTITY_MANAGER_H