#include "SceneText.h"
#include "GL\glew.h"
#include "shader.hpp"
#include "MeshBuilder.h"
#include "Application.h"
#include "Utility.h"
#include "LoadTGA.h"
#include <sstream>
#include "KeyboardController.h"
#include "MouseController.h"
#include "SceneManager.h"
#include "GraphicsManager.h"
#include "ShaderProgram.h"
#include "EntityManager.h"
#include "GenericEntity.h"
#include "GroundEntity.h"
#include "TextEntity.h"
#include "SpriteEntity.h"
#include "Light.h"
#include "SkyBox/SkyBoxEntity.h"
#include "SceneGraph\SceneNode.h"
#include "SceneGraph\SceneGraph.h"
#include "SceneGraph/UpdateTransformation.h"
#include "EntityEntity.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <iostream>
#include "FPSCounter.h"
using namespace std;

SceneText::SceneText()
{
}

SceneText::~SceneText()
{
}

void SceneText::Init()
{
	CLuaInterface::GetInstance()->SetLuaFile("Image//SP3.lua", CLuaInterface::GetInstance()->theLuaState);
	EntityManager::GetInstance()->ClearEntities();

	ShaderInit();

	lights[0] = new Light();
	GraphicsManager::GetInstance()->AddLight("lights[0]", lights[0]);
	lights[0]->type = Light::LIGHT_DIRECTIONAL;
	lights[0]->position.Set(0, 20, 0);
	lights[0]->color.Set(1, 1, 1);
	lights[0]->power = 0.5;
	lights[0]->kC = 1.f;
	lights[0]->kL = 0.01f;
	lights[0]->kQ = 0.001f;
	lights[0]->cosCutoff = cos(Math::DegreeToRadian(45));
	lights[0]->cosInner = cos(Math::DegreeToRadian(30));
	lights[0]->exponent = 3.f;
	lights[0]->spotDirection.Set(0.f, 1.f, 0.f);
	lights[0]->name = "lights[0]";

	lights[1] = new Light();
	GraphicsManager::GetInstance()->AddLight("lights[1]", lights[1]);
	lights[1]->type = Light::LIGHT_DIRECTIONAL;
	lights[1]->position.Set(1, 1, 0);
	lights[1]->color.Set(1, 1, 0.5f);
	lights[1]->power = 0.4f;
	lights[1]->name = "lights[1]";

	currProg->UpdateInt("numLights", 1);
	currProg->UpdateInt("textEnabled", 0);
	currProg2->UpdateInt("numLights", 1);
	currProg2->UpdateInt("textEnabled", 0);

	GraphicsManager::GetInstance()->AttachCamera(&camera);

	srand((unsigned int)time(NULL));

	GameInit();
	playerInfo->SetCollider(true);

	GameObjectsInit();

	// Set up the Spatial Partition and pass it to the EntityManager to manage
	lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "GridMeshSize");
	CSpatialPartition::GetInstance()->Init(CLuaInterface::GetInstance()->getFieldFloat("xGridSize"), CLuaInterface::GetInstance()->getFieldFloat("zGridSize"), CLuaInterface::GetInstance()->getFieldFloat("xNumofGrid"), CLuaInterface::GetInstance()->getFieldFloat("zNumofGrid"));
	CSpatialPartition::GetInstance()->SetMeshRenderMode(CGrid::FILL);
	CSpatialPartition::GetInstance()->SetMesh("water");
	CSpatialPartition::GetInstance()->SetCamera(&camera);

	lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "GridLoD");
	CSpatialPartition::GetInstance()->SetLevelOfDetails(CLuaInterface::GetInstance()->getFieldFloat("distance_High2Mid"), CLuaInterface::GetInstance()->getFieldFloat("distance_Mid2Low"));
	EntityManager::GetInstance()->SetSpatialPartition(CSpatialPartition::GetInstance());

	groundEntity = Create::Ground("FLOOR", "FLOOR");
	groundEntity->SetPosition(CLuaInterface::GetInstance()->getVector3Values("GroundPos"));
	groundEntity->SetScale(CLuaInterface::GetInstance()->getVector3Values("GroundScale"));
	groundEntity->SetGrids(CLuaInterface::GetInstance()->getVector3Values("GroundGrids"));
	playerInfo->SetTerrain(groundEntity);

	SkyBoxEntity* theSkyBox = Create::SkyBox("SKYBOX_FRONT", "SKYBOX_BACK", "SKYBOX_LEFT", "SKYBOX_RIGHT", "SKYBOX_TOP", "SKYBOX_BOTTOM", true);



	//Text
	float halfWindowWidth = Application::GetInstance().GetWindowWidth() / 2.0f;
	float halfWindowHeight = Application::GetInstance().GetWindowHeight() / 2.0f;
	float fontSize = 25.0f;
	float halfFontSize = fontSize / 2.0f;
	for (int i = 0; i < 5; ++i)
	{
		textObj[i] = Create::Text2DObject("text", Vector3(-halfWindowWidth, -halfWindowHeight + fontSize * i + halfFontSize, 2.0f), "", Vector3(fontSize, fontSize, fontSize), Color(1.f, 1.f, 1.f), true);
	}
	timertext[0] = Create::Text2DObject("secondstext", Vector3(-halfWindowWidth, 180.f, 2.f), "", Vector3(35.f, 35.f, 35.f), Color(0.f, 1.f, 0.f), true);

	MeshBuilder::GetInstance()->GenerateQuad("ammobar_front", Color(1, 1, 1), 1.f);
	MeshBuilder::GetInstance()->GetMesh("ammobar_front")->textureID = LoadTGA("Image//lifebar_front.tga");

	MeshBuilder::GetInstance()->GenerateQuad("ammobar_back", Color(1, 1, 1), 1.f);
	MeshBuilder::GetInstance()->GetMesh("ammobar_back")->textureID = LoadTGA("Image//lifebar_back.tga");

	//Camera Effects
	theCameraEffects = Create::CameraEffects(false);

	// Health at 70%
	theCameraEffects->SetBloodScreen2(MeshBuilder::GetInstance()->GenerateQuad("CAMERAEFFECTS_BLOODSCREEN2", Color(1.f, 1.f, 1.f), 1.f));
	theCameraEffects->GetBloodScreen2()->textureID = LoadTGA("Image//hpblood70.tga");
	theCameraEffects->SetStatus_BloodScreen2(false);

	// Health at 50%
	theCameraEffects->SetBloodScreen3(MeshBuilder::GetInstance()->GenerateQuad("CAMERAEFFECTS_BLOODSCREEN3", Color(1.f, 1.f, 1.f), 1.f));
	theCameraEffects->GetBloodScreen3()->textureID = LoadTGA("Image//hpblood50.tga");
	theCameraEffects->SetStatus_BloodScreen3(false);

	// Health at 30%
	theCameraEffects->SetBloodScreen4(MeshBuilder::GetInstance()->GenerateQuad("CAMERAEFFECTS_BLOODSCREEN4", Color(1.f, 1.f, 1.f), 1.f));
	theCameraEffects->GetBloodScreen4()->textureID = LoadTGA("Image//hpblood30.tga");
	theCameraEffects->SetStatus_BloodScreen4(false);

	// Health text at 30%
	theCameraEffects->SetTextScreen(MeshBuilder::GetInstance()->GenerateQuad("CAMERAEFFECTS_TEXTSCREEN", Color(1.f, 1.f, 1.f), 1.f));
	theCameraEffects->GetTextScreen()->textureID = LoadTGA("Image//hpblood30text.tga");
	theCameraEffects->SetStatus_TextScreen(false);

	// Scope 
	theCameraEffects->SetScopeScreen(MeshBuilder::GetInstance()->GenerateQuad("CAMERAEFFECTS_SCOPESCREEN", Color(1.f, 1.f, 1.f), 1.f));
	theCameraEffects->GetScopeScreen()->textureID = LoadTGA("Image//scope.tga");
	theCameraEffects->SetStatus_ScopeScreen(false);

	//Inventory
	theInventoryBorderEffects = Create::InventoryEffects(false);

	theInventoryBorderEffects->SetInventoryPistolBorderScreen(MeshBuilder::GetInstance()->GenerateQuad("INVENTORY_BORDER_PISTOL", Color(1.f, 1.f, 1.f), 1.f));
	theInventoryBorderEffects->GetInventoryPistolBorderScreen()->textureID = LoadTGA("Image//Inventory_Border_Pistol.tga");
	theInventoryBorderEffects->SetStatus_InventoryPistolBorderScreen(false);

	theInventoryBorderEffects->SetInventoryRifleBorderScreen(MeshBuilder::GetInstance()->GenerateQuad("INVENTORY_BORDER_RIFLE", Color(1.f, 1.f, 1.f), 1.f));
	theInventoryBorderEffects->GetInventoryRifleBorderScreen()->textureID = LoadTGA("Image//Inventory_Border_Rifle.tga");
	theInventoryBorderEffects->SetStatus_InventoryRifleBorderScreen(false);

	theInventoryBorderEffects->SetInventoryMinigunBorderScreen(MeshBuilder::GetInstance()->GenerateQuad("INVENTORY_BORDER_MINIGUN", Color(1.f, 1.f, 1.f), 1.f));
	theInventoryBorderEffects->GetInventoryMinigunBorderScreen()->textureID = LoadTGA("Image//Inventory_Border_Minigun.tga");
	theInventoryBorderEffects->SetStatus_InventoryMinigunBorderScreen(false);

	theInventoryBorderEffects->SetInventoryShotgunBorderScreen(MeshBuilder::GetInstance()->GenerateQuad("INVENTORY_BORDER_SHOTGUN", Color(1.f, 1.f, 1.f), 1.f));
	theInventoryBorderEffects->GetInventoryShotgunBorderScreen()->textureID = LoadTGA("Image//Inventory_Border_Shotgun.tga");
	theInventoryBorderEffects->SetStatus_InventoryShotgunBorderScreen(false);

	theInventoryBorderEffects->SetInventoryGrenadeBorderScreen(MeshBuilder::GetInstance()->GenerateQuad("INVENTORY_BORDER_GRENADE", Color(1.f, 1.f, 1.f), 1.f));
	theInventoryBorderEffects->GetInventoryGrenadeBorderScreen()->textureID = LoadTGA("Image//Inventory_Border_Grenade.tga");
	theInventoryBorderEffects->SetStatus_InventoryGrenadeBorderScreen(false);
	
	theInventoryBorderEffects->SetInventoryBlankBorderScreen(MeshBuilder::GetInstance()->GenerateQuad("INVENTORY_BORDER_BLANK", Color(1.f, 1.f, 1.f), 1.f));
	theInventoryBorderEffects->GetInventoryBlankBorderScreen()->textureID = LoadTGA("Image//Inventory_Border_Blank.tga");
	theInventoryBorderEffects->SetStatus_InventoryBlankBorderScreen(false);

	MeshBuilder::GetInstance()->GenerateCube("YELLOW", Color(1.0f, 1.0f, 0.0f), 1.0f);
	MeshBuilder::GetInstance()->GetMesh("YELLOW")->textureID = LoadTGA("Image//bullet.tga");
	MeshBuilder::GetInstance()->GenerateCube("CUBE", Color(1.0f, 0.0f, 0.0f), 1.0f);

	MeshBuilder::GetInstance()->GenerateOBJ("BULLET", "OBJ//bullet.obj");
	MeshBuilder::GetInstance()->GetMesh("BULLET")->textureID = LoadTGA("Image//bullet.tga");

	MeshBuilder::GetInstance()->GenerateOBJ("WALL", "OBJ//Wall.obj");
	MeshBuilder::GetInstance()->GetMesh("WALL")->textureID = LoadTGA("Image//Wall.tga");
	
	MeshBuilder::GetInstance()->GenerateOBJ("Target", "OBJ//target.obj");
	MeshBuilder::GetInstance()->GetMesh("Target")->textureID = LoadTGA("Image//target.tga");

	//EnemyInit();
	//WallInit();
	CreateEntities();

	theInventoryBorderEffects->SetTrigger(playerInfo->weaponManager);

	//Hardware Abstractions
	theKeyboard = new CKeyboard();
	theKeyboard->Create(playerInfo);
	theMouse = new CMouse();
	theMouse->Create(playerInfo);
	
	texttimer = 0;
	//Zoom
	zoomValue = 45.0f;
	//Gravity
	m_gravity.Set(0, -4.8f, 0);
	//Particle Count & Max Particle
	m_particleCount = 0;
	MAX_PARTICLE = 1000;

	//Grenade Trigger
	theEventTrigger = Create::EventBox("Grenade", Vector3(-175.0f, 0.f, -175.0f), Vector3(10.f, 10.f, 10.f), true);
	theEventTrigger->DoNotRender = false;
	playerInfo->GrenadeTrigger(theEventTrigger);

	KeySaveGame = CLuaInterface::GetInstance()->getCharValue("SaveGame");
	CLuaInterface::GetInstance()->SetLuaFile("Image//GameSaveData.lua", CLuaInterface::GetInstance()->theLuaState);
	minutes = (float)CLuaInterface::GetInstance()->getIntValue("MinutesLeft");
	seconds = (float)CLuaInterface::GetInstance()->getIntValue("SecondsLeft");
	if ((minutes == 0) && (seconds == 0))
	{
		CLuaInterface::GetInstance()->SetLuaFile("Image//SP3.lua", CLuaInterface::GetInstance()->theLuaState);
		minutes = (float)CLuaInterface::GetInstance()->getIntValue("MinutesLeft");
		seconds = (float)CLuaInterface::GetInstance()->getIntValue("SecondsLeft");
	}
	glEnable(GL_CULL_FACE);
	CSceneGraph::GetInstance()->PrintSelf();
}

void SceneText::Update(double dt)
{
	bool isPaused = false;
	Vector3 tempPlayer = playerInfo->GetPos();
	Vector3 tempTarget = playerInfo->GetTarget();

	if (seconds != 0)
	{
		seconds -= (float)dt;
	}
	if (seconds <= 0 && minutes >= 1)
	{
		minutes = minutes - 1;
		seconds = 60 + seconds;
	}
	if (seconds <= 0 && minutes <= 0)
	{
		minutes = 0;
		seconds = 0;
		SceneManager::GetInstance()->SetActiveScene("GameOverState");
	}

	if (playerInfo->SlowStatus == true)
	{
		EntityManager::GetInstance()->Update(dt * 0.05);
		theKeyboard->Read(dt * 0.05);
		theMouse->Read(dt * 0.05);
	}
	else
	{
		EntityManager::GetInstance()->Update(dt);
		theKeyboard->Read(dt);
		theMouse->Read(dt);
	}

	if (KeyboardController::GetInstance()->IsKeyReleased('V'))
	{
		SceneManager::GetInstance()->SetActiveScene("PauseState");
		isPaused = true;
	}

	if (isPaused == true)
	{
		SaveGame();
	}
	
	if (KeyboardController::GetInstance()->IsKeyReleased(KeySaveGame))
	{
		SaveGame();
	}

	// THIS WHOLE CHUNK TILL <THERE> CAN REMOVE INTO ENTITIES LOGIC! Or maybe into a scene function to keep the update clean
	if (KeyboardController::GetInstance()->IsKeyDown('1'))
		glEnable(GL_CULL_FACE);
	if (KeyboardController::GetInstance()->IsKeyDown('2'))
		glDisable(GL_CULL_FACE);
	if (KeyboardController::GetInstance()->IsKeyDown('3'))
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	if (KeyboardController::GetInstance()->IsKeyDown('4'))
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	//if (KeyboardController::GetInstance()->IsKeyDown('9'))
	//	theCameraEffects->SetStatus_BloodScreen(true);
	if (KeyboardController::GetInstance()->IsKeyDown('B'))
		theCameraEffects->SetStatus_BloodScreen2(true);
	if (KeyboardController::GetInstance()->IsKeyDown('N'))
		theCameraEffects->SetStatus_BloodScreen3(true);
	if (KeyboardController::GetInstance()->IsKeyDown('M'))
		theCameraEffects->SetStatus_BloodScreen4(true);

	if (KeyboardController::GetInstance()->IsKeyReleased('X'))
		playerInfo->ToggleFlight();

	if (KeyboardController::GetInstance()->IsKeyReleased('C'))
		EntityManager::GetInstance()->ClearProjectile();

	// Update the Scene Graph
	CSceneGraph::GetInstance()->Update(dt);

	// Update the Spatial Partition
	CSpatialPartition::GetInstance()->Update();

	// Update our entities
	GraphicsManager::GetInstance()->UpdateLights(dt);
	theCameraEffects->Update((float)dt);
	theInventoryBorderEffects->Update((float)dt);
	InventoryUpdate();
	UpdateParticles(dt); //Particle Update

	// Dot 1 (Removed for now) also mr toh method a bit broke
	std::ostringstream os;
	//float fps = (float)(1.f / dt);
	//if (KeyboardController::GetInstance()->IsKeyDown(VK_TAB))
	{
		std::ostringstream fps;
		fps.precision(5);
		fps.str("");
		fps.clear();
		//os << "FPS: " << fps;
		fps << "FPS: " << CFPSCounter::GetInstance()->GetFrameRate() << endl;
		textObj[0]->SetText(fps.str());
	}
	//else
	//{
		//textObj[0]->SetText(" " );
	//}

	os.str("");
	os.precision(4);
	os << "Score: " << EntityManager::GetInstance()->ScoreInput;
	textObj[3]->SetText(os.str());
	textObj[3]->SetColor(Color(0, 1, 0));

	os.str("");
	os.precision(2);
	os << minutes << ":" << seconds;
	timertext[0]->SetText(os.str());


	if (MouseController::GetInstance()->IsButtonDown(MouseController::RMB))
	{
		zoomValue = 15.0f;
		theCameraEffects->SetStatus_ScopeScreen(true);
	}
	else
	{
		zoomValue = 45.0f;
		theCameraEffects->SetStatus_ScopeScreen(false);
	}

	if (KeyboardController::GetInstance()->IsKeyPressed('6'))
	{ 
		if (EntityManager::GetInstance()->BuyingCheck(1000))
		{
			playerInfo->AddGun(0);
			if (texttimer == 0)
			{
				getgun = true;
				os.str("");
				os << "YOU GOT THE WEP!";
				textObj[2]->SetText(os.str());
				textObj[2]->SetColor(Color(0, 1, 0));
			}
			 
		}
	}
	if (KeyboardController::GetInstance()->IsKeyPressed('7'))
	{
		if (EntityManager::GetInstance()->BuyingCheck(2000))
		{
			playerInfo->AddGun(1);
			if (texttimer == 0)
			{
				getgun = true;
				os.str("");
				os << "YOU GOT THE WEP!";
				textObj[2]->SetText(os.str());
				textObj[2]->SetColor(Color(0, 1, 0));
			}
		}
	}
	if (KeyboardController::GetInstance()->IsKeyPressed('8'))
	{
		if (EntityManager::GetInstance()->BuyingCheck(3000))
		{
			playerInfo->AddGun(2);
			if (texttimer == 0)
			{
				getgun = true;
				os.str("");
				os << "YOU GOT THE WEP!";
				textObj[2]->SetText(os.str());
				textObj[2]->SetColor(Color(0, 1, 0));
			}
		}
	}
	if (KeyboardController::GetInstance()->IsKeyPressed('9'))
	{
		if (EntityManager::GetInstance()->BuyingCheck(4000))
		{
			playerInfo->AddGun(3);
			if (texttimer == 0)
			{
				getgun = true;
				os.str("");
				os << "YOU GOT THE WEP!";
				textObj[2]->SetText(os.str());
				textObj[2]->SetColor(Color(0, 1, 0));
			}
		}
	}
	if (KeyboardController::GetInstance()->IsKeyPressed('0'))
	{
		if (EntityManager::GetInstance()->BuyingCheck(5000))
		{
			playerInfo->AddGun(4);
			if (texttimer == 0)
			{
				getgun = true;
				os.str("");
				os << "YOU GOT THE WEP!";
				textObj[2]->SetText(os.str());
				textObj[2]->SetColor(Color(0, 1, 0));
			}
		}
	}

	if (getgun)
	{
		texttimer += dt;
	}

	if (texttimer >= 3)
	{
		textObj[2]->SetText(" ");
		texttimer = 0;
		getgun = false;
	}

	if (EntityManager::GetInstance()->CheckForCollision(EntityManager::GetInstance()->entityList.begin(), dt) == true)
	{
		playerInfo->SetPos(tempPlayer);
		playerInfo->SetTarget(tempTarget);
	}

	//trigger is -160 to -180 for both x and z
}

void SceneText::PlayerGunRender()
{
	MS& modelStack = GraphicsManager::GetInstance()->GetModelStack();

	switch (playerInfo->ReturnPrimaryWeaponData().GetWeaponType())
	{
		case CWeaponInfo::SEMI_AUTO:
		{
			lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "Pistol");
			string Name = CLuaInterface::GetInstance()->getFieldString("Name");
			Mesh* Pistol = MeshBuilder::GetInstance()->GetMesh(Name);

			modelStack.PushMatrix();
			if (MouseController::GetInstance()->IsButtonDown(MouseController::RMB))
			{
				modelStack.Translate(0.f, -1.35f, -5.f); // Values for Scope mode
			}
			else
			{
				modelStack.Translate(1.75f, -1.5f, -6.f);
			}
			modelStack.Scale(10.75f, 10.75f, 10.75f);
			RenderHelper::RenderMeshGun(Pistol);
			modelStack.PopMatrix();
			break;
		}
		case CWeaponInfo::FULL_AUTO:
		{
			lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "Minigun");
			string Name = CLuaInterface::GetInstance()->getFieldString("Name");
			Mesh* Minigun = MeshBuilder::GetInstance()->GetMesh(Name);

			modelStack.PushMatrix();
			if (MouseController::GetInstance()->IsButtonDown(MouseController::RMB))
			{
				modelStack.Translate(0.f, -1.5f, -6.0f); // Values for Scope mode
			}
			else
			{
				modelStack.Translate(1.75f, -1.5f, -6.0f);
			}
			modelStack.Scale(10.75f, 10.75f, 10.75f);
			RenderHelper::RenderMeshGun(Minigun);
			modelStack.PopMatrix();
			break;
		}
		case CWeaponInfo::BURST:
		{
			lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "Famas");
			string Name = CLuaInterface::GetInstance()->getFieldString("Name");
			Mesh* Rifle = MeshBuilder::GetInstance()->GetMesh(Name);

			modelStack.PushMatrix();
			if (MouseController::GetInstance()->IsButtonDown(MouseController::RMB))
			{
				modelStack.Translate(0.f, -1.7f, -2.f); // Values for Scope mode
			}
			else
			{
				modelStack.Translate(1.75f, -1.5f, -5.0f);
			}
			modelStack.Scale(8.75f, 8.75f, 8.75f);
			RenderHelper::RenderMeshGun(Rifle);
			modelStack.PopMatrix();
			break;
		}
		case CWeaponInfo::SPREAD:
		{
			lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "Shotgun");
			string Name = CLuaInterface::GetInstance()->getFieldString("Name");
			Mesh* Shotgun = MeshBuilder::GetInstance()->GetMesh(Name);

			modelStack.PushMatrix();
			if (MouseController::GetInstance()->IsButtonDown(MouseController::RMB))
			{
				modelStack.Translate(0.f, -1.6f, -2.0f); // Values for Scope mode
			}
			else
			{
				modelStack.Translate(1.25f, -1.7f, -4.0f);
			}
			modelStack.Scale(10.75f, 10.75f, 10.75f);
			RenderHelper::RenderMeshGun(Shotgun);
			modelStack.PopMatrix();
			break;
		}
		case CWeaponInfo::GRENADE:
		{
			lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "Grenade");
			string Name = CLuaInterface::GetInstance()->getFieldString("Name");
			Mesh* Grenade = MeshBuilder::GetInstance()->GetMesh(Name);

			modelStack.PushMatrix();
			modelStack.Translate(1.25f, -1.7f, -4.0f);
			modelStack.Scale(1.f, 1.f, 1.f);
			RenderHelper::RenderMeshGun(Grenade);
			modelStack.PopMatrix();
			break;
		}
		case CWeaponInfo::BLANK:
		{
			break;
		}
		default:
		{
			break;
		}
	}
}

void SceneText::Render()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
	GraphicsManager::GetInstance()->UpdateLightUniforms();
	// Setup 3D pipeline then render 3D
	GraphicsManager::GetInstance()->SetPerspectiveProjection(zoomValue, 4.0f / 3.0f, 0.1f, 10000.0f); //change first number for zoom
	GraphicsManager::GetInstance()->AttachCamera(&camera);

	// Pre RenderMesh
	RenderHelper::PreRenderMesh();
	//Render the required entities
	EntityManager::GetInstance()->Render(); // Dot 4(currently calling scenegraph render after spatial partition will have problem, wireframe mesh does not show up, must call scenegraph render before calling spatial render)
	CSceneGraph::GetInstance()->Render();
	CSpatialPartition::GetInstance()->Render();
	// Post RenderMesh
	RenderHelper::PostRenderMesh();

	// Enable blend mode
	glEnable(GL_BLEND);

	CLuaInterface::GetInstance()->SetLuaFile("Image//SP3.lua", CLuaInterface::GetInstance()->theLuaState);

	MS& modelStack = GraphicsManager::GetInstance()->GetModelStack();
	Mesh* crosshair = MeshBuilder::GetInstance()->GetMesh("crosshair");
	
	modelStack.PushMatrix();
	modelStack.Translate(0, 0, -6.0f);
	modelStack.Scale(0.25, 0.25f, 0.25f);
	RenderHelper::RenderMeshGun(crosshair);
	modelStack.PopMatrix();

	PlayerGunRender();

	 
	for (std::vector<ParticleObject *>::iterator it = particleList.begin(); it != particleList.end(); ++it)
	{
		ParticleObject *particle = (ParticleObject *)*it;
		if (particle->active)
		{
			RenderParticles(particle);
		}
	}

	// Setup 2D pipeline then render 2D
	int halfWindowWidth = Application::GetInstance().GetWindowWidth() / 2;
	int halfWindowHeight = Application::GetInstance().GetWindowHeight() / 2;
	GraphicsManager::GetInstance()->SetOrthographicProjection(-halfWindowWidth, halfWindowWidth, -halfWindowHeight, halfWindowHeight, -10, 10);
	GraphicsManager::GetInstance()->DetachCamera();

	// Pre RenderText
	RenderHelper::PreRenderText();
	//Render the required entites
	EntityManager::GetInstance()->RenderUI();
	// Post RenderText
	RenderHelper::PostRenderText();

	Mesh* ammobar_front = MeshBuilder::GetInstance()->GetMesh("ammobar_front");
	Mesh* ammobar_back = MeshBuilder::GetInstance()->GetMesh("ammobar_back");
	MS& ammobar = GraphicsManager::GetInstance()->GetModelStack();
	ammobar.PushMatrix();
	ammobar.Translate(0.0f - ((1 - playerInfo->ReturnRatioBullet()) * 200.f), -400, 0.0f);
	ammobar.Scale(800.f, 600.f, 100.f);
	ammobar.PushMatrix();
	RenderHelper::RenderMesh(ammobar_front);
	ammobar.PopMatrix();
	ammobar.PopMatrix();

		ammobar.PushMatrix();
		ammobar.Translate(-285.0f, -500.f, 0.0f);
		ammobar.Scale(800.f, 600.f, 100.f);
		ammobar.PushMatrix();
		RenderHelper::RenderMesh(ammobar_back);
		ammobar.PopMatrix();
		ammobar.PopMatrix();

	// Render Camera Effects
	theCameraEffects->RenderUI();
	
	// Render Inventory Border Effects
	theInventoryBorderEffects->RenderUI();

	// Disable blend mode
	glDisable(GL_BLEND);
}

void SceneText::InventoryUpdate()
{
	int ScrollNumber = (int)MouseController::GetInstance()->GetMouseScrollStatus(MouseController::SCROLL_TYPE_YOFFSET);
	switch (ScrollNumber)
	{
	case 0:
	case 1:
	case 2:
	case 3:
	{
		if (theInventoryBorderEffects->GetTimer_InventoryBorderScreen() > 0.f)
		{
			theInventoryBorderEffects->SetStatus_InventoryPistolBorderScreen(true);
			theInventoryBorderEffects->SetStatus_InventoryRifleBorderScreen(true);
			theInventoryBorderEffects->SetStatus_InventoryMinigunBorderScreen(true);
			theInventoryBorderEffects->SetStatus_InventoryShotgunBorderScreen(true);
			theInventoryBorderEffects->SetStatus_InventoryGrenadeBorderScreen(true);
			theInventoryBorderEffects->SetStatus_InventoryBlankBorderScreen(true);
		}
		break;
	}
	}
}

void SceneText::ShaderInit()
{
	try
	{
		currProg = GraphicsManager::GetInstance()->LoadShader("default", "Shader//comg.vertexshader", "Shader//comg.fragmentshader");
		if (!currProg || currProg == nullptr)
		{
			throw "Unable to create a comg Shader.";
		}
	}
	catch (const char* msg)
	{
		cout << "Exception when loading a set of shader files xd: " << msg << endl;
		exit(EXIT_FAILURE);
	}
	try
	{
		currProg2 = GraphicsManager::GetInstance()->LoadShader("default", "Shader//Shadow.vertexshader", "Shader//Shadow.fragmentshader");
		if (!currProg2 || currProg2 == nullptr)
		{
			throw "Unable to create a shadow Shader.";
		}
	}
	catch (const char* msg)
	{
		cout << "Exception when loading a set of shader files xd: " << msg << endl;
		exit(EXIT_FAILURE);
	}
	
	// Tell the shader program to store these uniform locations
	currProg->AddUniform("MVP");
	currProg->AddUniform("MV");
	currProg->AddUniform("MV_inverse_transpose");
	currProg->AddUniform("material.kAmbient");
	currProg->AddUniform("material.kDiffuse");
	currProg->AddUniform("material.kSpecular");
	currProg->AddUniform("material.kShininess");
	currProg->AddUniform("lightEnabled");
	currProg->AddUniform("numLights");
	currProg->AddUniform("lights[0].type");
	currProg->AddUniform("lights[0].position_cameraspace");
	currProg->AddUniform("lights[0].color");
	currProg->AddUniform("lights[0].power");
	currProg->AddUniform("lights[0].kC");
	currProg->AddUniform("lights[0].kL");
	currProg->AddUniform("lights[0].kQ");
	currProg->AddUniform("lights[0].spotDirection");
	currProg->AddUniform("lights[0].cosCutoff");
	currProg->AddUniform("lights[0].cosInner");
	currProg->AddUniform("lights[0].exponent");
	currProg->AddUniform("lights[1].type");
	currProg->AddUniform("lights[1].position_cameraspace");
	currProg->AddUniform("lights[1].color");
	currProg->AddUniform("lights[1].power");
	currProg->AddUniform("lights[1].kC");
	currProg->AddUniform("lights[1].kL");
	currProg->AddUniform("lights[1].kQ");
	currProg->AddUniform("lights[1].spotDirection");
	currProg->AddUniform("lights[1].cosCutoff");
	currProg->AddUniform("lights[1].cosInner");
	currProg->AddUniform("lights[1].exponent");
	currProg->AddUniform("colorTextureEnabled");
	currProg->AddUniform("colorTexture");
	currProg->AddUniform("textEnabled");
	currProg->AddUniform("textColor");

	currProg2->AddUniform("MVP");
	currProg2->AddUniform("MV");
	currProg2->AddUniform("MV_inverse_transpose");
	currProg2->AddUniform("material.kAmbient");
	currProg2->AddUniform("material.kDiffuse");
	currProg2->AddUniform("material.kSpecular");
	currProg2->AddUniform("material.kShininess");
	currProg2->AddUniform("lightEnabled");
	currProg2->AddUniform("numLights");
	currProg2->AddUniform("lights[0].type");
	currProg2->AddUniform("lights[0].position_cameraspace");
	currProg2->AddUniform("lights[0].color");
	currProg2->AddUniform("lights[0].power");
	currProg2->AddUniform("lights[0].kC");
	currProg2->AddUniform("lights[0].kL");
	currProg2->AddUniform("lights[0].kQ");
	currProg2->AddUniform("lights[0].spotDirection");
	currProg2->AddUniform("lights[0].cosCutoff");
	currProg2->AddUniform("lights[0].cosInner");
	currProg2->AddUniform("lights[0].exponent");
	currProg2->AddUniform("lights[1].type");
	currProg2->AddUniform("lights[1].position_cameraspace");
	currProg2->AddUniform("lights[1].color");
	currProg2->AddUniform("lights[1].power");
	currProg2->AddUniform("lights[1].kC");
	currProg2->AddUniform("lights[1].kL");
	currProg2->AddUniform("lights[1].kQ");
	currProg2->AddUniform("lights[1].spotDirection");
	currProg2->AddUniform("lights[1].cosCutoff");
	currProg2->AddUniform("lights[1].cosInner");
	currProg2->AddUniform("lights[1].exponent");
	currProg2->AddUniform("colorTextureEnabled");
	currProg2->AddUniform("colorTexture");
	currProg2->AddUniform("textEnabled");
	currProg2->AddUniform("textColor");

	// Tell the graphics manager to use the shader we just loaded
	GraphicsManager::GetInstance()->SetActiveShader("default");
}

void SceneText::GameInit()
{
	CLuaInterface::GetInstance()->SetLuaFile("Image//GameSaveData.lua", CLuaInterface::GetInstance()->theLuaState);
	// Create the playerinfo instance, which manages all information about the player
	playerInfo = CPlayerInfo::GetInstance();
	playerInfo->Init();
	// Create and attach the camera to the scene
	camera.Init(playerInfo->GetPos(), playerInfo->GetTarget(), playerInfo->GetUp());
	playerInfo->AttachCamera(&camera);
	CLuaInterface::GetInstance()->SetLuaFile("Image//SP3.lua", CLuaInterface::GetInstance()->theLuaState);
	playerInfo->InitKeyInputs();
}

void SceneText::WallInit()
{

}

void SceneText::EnemyInit()
{
	Vector3 Offset(50.0f, 0, 50.0f);
	Vector3 VerticalTarget(1.0f, 10.0f, 10.0f);
	Vector3 HorizontalTarget(10.0f, 10.0f, 1.0f);
	Vector3 VerticalTrigger(10.0f, 20.0f, 45.0f);
	Vector3 HorizontalTrigger(45.0f, 20.0f, 10.0f);
	Vector3 PopUpMin(0, 0, 0);
	Vector3 PopUpMax(0, 20, 0);
	Vector3 MoveVerticalMin(0, 0, -20);
	Vector3 MoveVerticalMax(0, 0, 20);
	Vector3 MoveHorizontalMin(-20, 0, 0);
	Vector3 MoveHorizontalMax(20, 0, 0);

	//Enemy
#pragma region Enemy and Trigger

	CSceneNode* enemyNode, * eventNode;

	tempposListEnemy.push_back(Vector3(25.0f, -5.0f, -200.0f));
	tempposListTarget.push_back(Vector3(75.0f, -20.0f, -175.0f));
	tempposListEnemy.push_back(Vector3(125.0f, -5.0f, -200.0f));
	tempposListTarget.push_back(Vector3(75.0f, -20.0f, -225.0f));
	tempposListEnemy.push_back(Vector3(100.0f, -5.0f, -125.5f));
	tempposListTarget.push_back(Vector3(25.0f, -20.0f, -125.0f));
	tempposListEnemy.push_back(Vector3(25.0f, -5.0f, -100.0f));
	tempposListTarget.push_back(Vector3(75.0f, -20.0f, -57.5f));
	tempposListEnemy.push_back(Vector3(225.0f, -5.0f, 50.0f));
	tempposListTarget.push_back(Vector3(125.0f, -20.0f, 75.0f));
	tempposListEnemy.push_back(Vector3(200.0f, -5.0f, 125.0f));
	tempposListTarget.push_back(Vector3(225.0f, -20.0f, 200.0f));

	for (int i = 0; i < tempposListEnemy.size(); ++i)
	{
		theEventTrigger = Create::EventBox("YELLOW", tempposListEnemy[i], HorizontalTrigger, true);
		theEventTrigger->DoNotRender = true;
		anEnemy3D = Create::Enemy3D("Target", tempposListTarget[i], theEventTrigger->AssociatedTrigger, groundEntity, playerInfo, MoveVerticalMin, MoveVerticalMax, false, VerticalTarget, true);
		enemyNode = CSceneGraph::GetInstance()->AddNode("enemy", anEnemy3D);
		eventNode = enemyNode->AddChild("eventtrigger", theEventTrigger);
	}
	tempposListEnemy.clear();
	tempposListTarget.clear();

#pragma endregion

	//Grenade Trigger
	theEventTrigger = Create::EventBox("Grenade", Vector3(-175.0f, 0.f, -175.0f), Vector3(10.f,10.f,10.f), true);
	theEventTrigger->DoNotRender = false;
	playerInfo->GrenadeTrigger(theEventTrigger);
}

void SceneText::CreateEntities(void)
{
	// Add a cube to act as the torso of the NPC
	GenericEntity* pNPCTorso = Create::Entity("YELLOW", Vector3(0.0f, 0.0f, 0.0f), Vector3(1.0f, 1.0f, 1.0f), false);
	pNPCTorso->SetCollider(true);
	pNPCTorso->SetAABB(Vector3(0.45f, 0.45f, 0.45f), Vector3(-0.45f, -0.45f, -0.45f));

	// Add the pointer to the root of the SceneGraph
	CSceneNode* pNPCSceneNode = CSceneGraph::GetInstance()->AddNode("torso", pNPCTorso);
	pNPCSceneNode->SetTranslate(Vector3(0.0f, 1.0f, -10.0f));
	CUpdateTransformation* aRotateMtx = new CUpdateTransformation();
	aRotateMtx->ApplyUpdate(1.0f, 0.0f, 1.0f, 0.0f);
	aRotateMtx->SetSteps(-60, 60);
	pNPCSceneNode->SetUpdateTransformation(aRotateMtx);

	// Add a sphere to act as the head of the NPC
	GenericEntity* pNPCPart = Create::Entity("BULLET", Vector3(0.0f, 0.0f, 0.0f), Vector3(1.0f, 1.0f, 1.0f), false);
	pNPCPart->SetCollider(true);
	pNPCPart->SetAABB(Vector3(0.45f, 0.45f, 0.45f), Vector3(-0.45f, -0.45f, -0.45f));
	// Add the pointer to the root of the SceneGraph
	CSceneNode* anotherNode = pNPCSceneNode->AddChild("head", pNPCPart);
	anotherNode->SetTranslate(Vector3(0.0f, 1.0f, 0.0f));

	// Add a cube to act as the left leg of the NPC
	pNPCPart = Create::Entity("YELLOW", Vector3(0.0f, 0.0f, 0.0f), Vector3(1.0f, 1.0f, 1.0f), false);
	pNPCPart->SetCollider(true);
	pNPCPart->SetAABB(Vector3(0.45f, 0.45f, 0.45f), Vector3(-0.45f, -0.45f, -0.45f));
	anotherNode = pNPCSceneNode->AddChild("legleft", pNPCPart);
	anotherNode->SetTranslate(Vector3(-0.6f, -1.0f, 0.0f));

	// Add a cube to act as the right leg of the NPC
	pNPCPart = Create::Entity("YELLOW", Vector3(0.0f, 0.0f, 0.0f), Vector3(1.0f, 1.0f, 1.0f), false);
	pNPCPart->SetCollider(true);
	pNPCPart->SetAABB(Vector3(0.45f, 0.45f, 0.45f), Vector3(-0.45f, -0.45f, -0.45f));
	anotherNode = pNPCSceneNode->AddChild("legright", pNPCPart);
	anotherNode->SetTranslate(Vector3(0.6f, -1.0f, 0.0f));

	// Add a cube to act as the left arm of the NPC
	pNPCPart = Create::Entity("YELLOW", Vector3(0.0f, 0.0f, 0.0f), Vector3(1.0f, 1.0f, 1.0f), false);
	pNPCPart->SetCollider(true);
	pNPCPart->SetAABB(Vector3(0.45f, 0.45f, 0.45f), Vector3(-0.45f, -0.45f, -0.45f));
	anotherNode = pNPCSceneNode->AddChild("armleft", pNPCPart);
	anotherNode->SetTranslate(Vector3(-1.2f, 0.0f, 0.0f));

	// Add a cube to act as the right arm of the NPC
	pNPCPart = Create::Entity("YELLOW", Vector3(0.0f, 0.0f, 0.0f), Vector3(1.0f, 1.0f, 1.0f), false);
	pNPCPart->SetCollider(true);
	pNPCPart->SetAABB(Vector3(0.45f, 0.45f, 0.45f), Vector3(-0.45f, -0.45f, -0.45f));
	anotherNode = pNPCSceneNode->AddChild("armright", pNPCPart);
	anotherNode->SetTranslate(Vector3(1.2f, 0.0f, 0.0f));
	aRotateMtx = new CUpdateTransformation();
	aRotateMtx->ApplyUpdate(1.0f, 0.0f, 1.0f, 0.0f);
	aRotateMtx->SetSteps(-60, 60);
	anotherNode->SetUpdateTransformation(aRotateMtx);

	// Create a CEnemy Instance
	//anEnemy3D = Create::Enemy3D("cube",Vector3(0.0f, 0.0f, -20.0f));
	CEnemy3D* testNPC;
	testNPC = Create::Enemy3D("YELLOW", Vector3(10.0f, 0.0f, 0.0f), Vector3(1.0f, 1.0f, 1.0f), false);
	testNPC->Init();
	testNPC->SetSpeed(1.0f);
	testNPC->SetCollider(true);
	testNPC->SetAABB(Vector3(0.5f, 0.5f, 0.5f), Vector3(-0.5f, -0.5f, -0.5f));
	testNPC->SetTerrain(groundEntity);

	// Add the entity into the Spatial Partition
	CSpatialPartition::GetInstance()->Add(testNPC);

	pNPCSceneNode = CSceneGraph::GetInstance()->AddNode("enemy", testNPC);
	pNPCSceneNode->SetTranslate(Vector3(0.0f, 0.0f, 0.0f));
	aRotateMtx = new CUpdateTransformation();
	aRotateMtx->ApplyUpdate(1.0f, 0.0f, 1.0f, 0.0f);
	aRotateMtx->SetSteps(-60, 60);
	pNPCSceneNode->SetUpdateTransformation(aRotateMtx);

	// Add a sphere to act as the head of the NPC
	pNPCPart = Create::Entity("BULLET", Vector3(0.0f, 0.0f, 0.0f), Vector3(1.0f, 1.0f, 1.0f), false);
	pNPCPart->SetCollider(true);
	pNPCPart->SetAABB(Vector3(0.45f, 0.45f, 0.45f), Vector3(-0.45f, -0.45f, -0.45f));
	anotherNode = pNPCSceneNode->AddChild("bullet", pNPCPart);
	anotherNode->SetTranslate(Vector3(0.0f, 1.0f, 0.0f));
	aRotateMtx = new CUpdateTransformation();
	aRotateMtx->ApplyUpdate(1.0f, 1.0f, 0.0f, 0.0f);
	aRotateMtx->SetSteps(-60, 60);
	anotherNode->SetUpdateTransformation(aRotateMtx);
}

void SceneText::SaveGame()
{
	CLuaInterface::GetInstance()->saveVector3Value("PlayerPos", playerInfo->GetPos(), "SaveToGameFile", true);
	CLuaInterface::GetInstance()->saveVector3Value("PlayerTarget", playerInfo->GetTarget(), "SaveToGameFile", false);
	CLuaInterface::GetInstance()->saveIntValue("PlayerWeapon", playerInfo->GetWeapon(), "SaveToGameFile", false);

	CLuaInterface::GetInstance()->saveIntValue("MinutesLeft", (int)minutes, "SaveToGameFile", false);
	CLuaInterface::GetInstance()->saveIntValue("SecondsLeft", (int)seconds, "SaveToGameFile", false);
}

void SceneText::GameObjectsInit()
{
	//Crosshair
	MeshBuilder::GetInstance()->GenerateCrossHair(CLuaInterface::GetInstance()->getStringValue("Crosshair"));

	//Axes
	MeshBuilder::GetInstance()->GenerateAxes(CLuaInterface::GetInstance()->getStringValue("Axes"));

	//Calibri Quad
	lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "quad");
	string Name = CLuaInterface::GetInstance()->getFieldString("Name");
	float Length = CLuaInterface::GetInstance()->getFieldFloat("length");
	string TextureName = CLuaInterface::GetInstance()->getFieldString("Texture");
	string ColorName = CLuaInterface::GetInstance()->getStringValue("quadColor");
	MeshBuilder::GetInstance()->GenerateQuad(Name, Color(ColorName), Length);
	MeshBuilder::GetInstance()->GetMesh(Name)->textureID = LoadTGA(TextureName.c_str());

	//Text Quad
	lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "text");
	Name = CLuaInterface::GetInstance()->getFieldString("Name");
	unsigned int row = (unsigned int)CLuaInterface::GetInstance()->getFieldFloat("row");
	unsigned int col = (unsigned int)CLuaInterface::GetInstance()->getFieldFloat("column");
	TextureName = CLuaInterface::GetInstance()->getFieldString("Texture");
	lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "textMaterialRGB");
	MeshBuilder::GetInstance()->GenerateText(Name, row, col);
	MeshBuilder::GetInstance()->GetMesh(Name)->textureID = LoadTGA(TextureName.c_str());
	MeshBuilder::GetInstance()->GetMesh(Name)->material.kAmbient.Set(CLuaInterface::GetInstance()->getFieldFloat("r"), CLuaInterface::GetInstance()->getFieldFloat("g"), CLuaInterface::GetInstance()->getFieldFloat("b"));
	
	MeshBuilder::GetInstance()->GenerateText("secondstext", 16, 16);
	MeshBuilder::GetInstance()->GetMesh("secondstext")->textureID = LoadTGA("Image//calibri.tga");
	MeshBuilder::GetInstance()->GetMesh("secondstext")->material.kAmbient.Set(1, 0, 0);

	/**********************************************************************************************************************
	Particle Effects
	***********************************************************************************************************************/
	
	MeshBuilder::GetInstance()->GenerateQuad("water", Color(1.0f, 1.0f, 1.0f), 1.0f);
	MeshBuilder::GetInstance()->GetMesh("water")->textureID = LoadTGA("Image//water.tga");

	MeshBuilder::GetInstance()->GenerateQuad("navigation", Color(1.0f, 1.0f, 1.0f), 1.0f);
	MeshBuilder::GetInstance()->GetMesh("navigation")->textureID = LoadTGA("Image//navigation.tga");

	//Floor Tile
	lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "FloorTile");
	Name = CLuaInterface::GetInstance()->getFieldString("Name");
	MeshBuilder::GetInstance()->GenerateQuad(Name, Color(CLuaInterface::GetInstance()->getFieldString("ColorName")));
	MeshBuilder::GetInstance()->GetMesh(Name)->textureID = LoadTGA(CLuaInterface::GetInstance()->getFieldString("Texture").c_str());

	//Skybox
	lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "SkyboxFront");
	Name = CLuaInterface::GetInstance()->getFieldString("Name");
	ColorName = CLuaInterface::GetInstance()->getFieldString("ColorName");
	TextureName = CLuaInterface::GetInstance()->getFieldString("Texture");
	MeshBuilder::GetInstance()->GenerateQuad(Name, Color(ColorName));
	MeshBuilder::GetInstance()->GetMesh(Name)->textureID = LoadTGA(TextureName.c_str());

	lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "SkyboxBack");
	Name = CLuaInterface::GetInstance()->getFieldString("Name");
	ColorName = CLuaInterface::GetInstance()->getFieldString("ColorName");
	TextureName = CLuaInterface::GetInstance()->getFieldString("Texture");
	MeshBuilder::GetInstance()->GenerateQuad(Name, Color(ColorName));
	MeshBuilder::GetInstance()->GetMesh(Name)->textureID = LoadTGA(TextureName.c_str());

	lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "SkyboxLeft");
	Name = CLuaInterface::GetInstance()->getFieldString("Name");
	ColorName = CLuaInterface::GetInstance()->getFieldString("ColorName");
	TextureName = CLuaInterface::GetInstance()->getFieldString("Texture");
	MeshBuilder::GetInstance()->GenerateQuad(Name, Color(ColorName));
	MeshBuilder::GetInstance()->GetMesh(Name)->textureID = LoadTGA(TextureName.c_str());

	lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "SkyboxRight");
	Name = CLuaInterface::GetInstance()->getFieldString("Name");
	ColorName = CLuaInterface::GetInstance()->getFieldString("ColorName");
	TextureName = CLuaInterface::GetInstance()->getFieldString("Texture");
	MeshBuilder::GetInstance()->GenerateQuad(Name, Color(ColorName));
	MeshBuilder::GetInstance()->GetMesh(Name)->textureID = LoadTGA(TextureName.c_str());

	lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "SkyboxTop");
	Name = CLuaInterface::GetInstance()->getFieldString("Name");
	ColorName = CLuaInterface::GetInstance()->getFieldString("ColorName");
	TextureName = CLuaInterface::GetInstance()->getFieldString("Texture");
	MeshBuilder::GetInstance()->GenerateQuad(Name, Color(ColorName));
	MeshBuilder::GetInstance()->GetMesh(Name)->textureID = LoadTGA(TextureName.c_str());

	lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "SkyboxBottom");
	Name = CLuaInterface::GetInstance()->getFieldString("Name");
	ColorName = CLuaInterface::GetInstance()->getFieldString("ColorName");
	TextureName = CLuaInterface::GetInstance()->getFieldString("Texture");
	MeshBuilder::GetInstance()->GenerateQuad(Name, Color(ColorName));
	MeshBuilder::GetInstance()->GetMesh(Name)->textureID = LoadTGA(TextureName.c_str());

	//Grid Mesh
	lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "GridMesh");
	MeshBuilder::GetInstance()->GenerateQuad(CLuaInterface::GetInstance()->getFieldString("Name"), Color(CLuaInterface::GetInstance()->getFieldString("ColorName")), CLuaInterface::GetInstance()->getFieldFloat("length"));

	//Weapons
	lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "Pistol");
	Name = CLuaInterface::GetInstance()->getFieldString("Name");
	string OBJ = CLuaInterface::GetInstance()->getFieldString("Obj");
	TextureName = CLuaInterface::GetInstance()->getFieldString("Texture");
	MeshBuilder::GetInstance()->GenerateOBJ(Name, OBJ);
	MeshBuilder::GetInstance()->GetMesh(Name)->textureID = LoadTGA(TextureName.c_str());
	lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "weaponMaterialRGB");
	MeshBuilder::GetInstance()->GetMesh(Name)->material.kAmbient.Set(CLuaInterface::GetInstance()->getFieldFloat("r"), CLuaInterface::GetInstance()->getFieldFloat("g"), CLuaInterface::GetInstance()->getFieldFloat("b"));

	lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "Famas");
	Name = CLuaInterface::GetInstance()->getFieldString("Name");
	OBJ = CLuaInterface::GetInstance()->getFieldString("Obj");
	TextureName = CLuaInterface::GetInstance()->getFieldString("Texture");
	MeshBuilder::GetInstance()->GenerateOBJ(Name, OBJ);
	MeshBuilder::GetInstance()->GetMesh(Name)->textureID = LoadTGA(TextureName.c_str());
	lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "weaponMaterialRGB");
	MeshBuilder::GetInstance()->GetMesh(Name)->material.kAmbient.Set(CLuaInterface::GetInstance()->getFieldFloat("r"), CLuaInterface::GetInstance()->getFieldFloat("g"), CLuaInterface::GetInstance()->getFieldFloat("b"));

	lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "Minigun");
	Name = CLuaInterface::GetInstance()->getFieldString("Name");
	OBJ = CLuaInterface::GetInstance()->getFieldString("Obj");
	TextureName = CLuaInterface::GetInstance()->getFieldString("Texture");
	MeshBuilder::GetInstance()->GenerateOBJ(Name, OBJ);
	MeshBuilder::GetInstance()->GetMesh(Name)->textureID = LoadTGA(TextureName.c_str());
	lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "weaponMaterialRGB");
	MeshBuilder::GetInstance()->GetMesh(Name)->material.kAmbient.Set(CLuaInterface::GetInstance()->getFieldFloat("r"), CLuaInterface::GetInstance()->getFieldFloat("g"), CLuaInterface::GetInstance()->getFieldFloat("b"));

	lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "Shotgun");
	Name = CLuaInterface::GetInstance()->getFieldString("Name");
	OBJ = CLuaInterface::GetInstance()->getFieldString("Obj");
	TextureName = CLuaInterface::GetInstance()->getFieldString("Texture");
	MeshBuilder::GetInstance()->GenerateOBJ(Name, OBJ);
	MeshBuilder::GetInstance()->GetMesh(Name)->textureID = LoadTGA(TextureName.c_str());
	lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "weaponMaterialRGB");
	MeshBuilder::GetInstance()->GetMesh(Name)->material.kAmbient.Set(CLuaInterface::GetInstance()->getFieldFloat("r"), CLuaInterface::GetInstance()->getFieldFloat("g"), CLuaInterface::GetInstance()->getFieldFloat("b"));

	lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "Grenade");
	Name = CLuaInterface::GetInstance()->getFieldString("Name");
	OBJ = CLuaInterface::GetInstance()->getFieldString("Obj");
	TextureName = CLuaInterface::GetInstance()->getFieldString("Texture");
	MeshBuilder::GetInstance()->GenerateOBJ(Name, OBJ);
	MeshBuilder::GetInstance()->GetMesh(Name)->textureID = LoadTGA(TextureName.c_str());
	lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "weaponMaterialRGB");
	MeshBuilder::GetInstance()->GetMesh(Name)->material.kAmbient.Set(CLuaInterface::GetInstance()->getFieldFloat("r"), CLuaInterface::GetInstance()->getFieldFloat("g"), CLuaInterface::GetInstance()->getFieldFloat("b"));
}

void SceneText::UpdateParticles(double dt)
{

	if (m_particleCount < (int)MAX_PARTICLE)
	{
		ParticleObject* particle = GetParticle();
		particle->type = ParticleObject_TYPE::P_WATER;
		particle->scale.Set(15, 15, 15);
		particle->vel.Set(1, 1, 1);
		particle->rotationSpeed = Math::RandFloatMinMax(20.f, 40.f);
		particle->pos.Set(Math::RandFloatMinMax(-500, 500), 500.0f, Math::RandFloatMinMax(-1000, 500));
	}

	if (m_particleCount < (int)MAX_PARTICLE)
	{
		ParticleObject* particle = GetParticle();
		particle->type = ParticleObject_TYPE::P_NAVIGATION;
		particle->scale.Set(15, 15, 15);
		particle->vel.Set(1, 1, 1);
		particle->rotationSpeed = Math::RandFloatMinMax(20.f, 40.f);
		particle->pos.Set(Math::RandFloatMinMax(-125, -200), 500.0f, Math::RandFloatMinMax(-125, -200));
	}

	for (std::vector<ParticleObject *>::iterator it = particleList.begin(); it != particleList.end(); ++it)
	{
		ParticleObject *particle = (ParticleObject *)*it;
		if (particle->active)
		{
			if (particle->type == ParticleObject_TYPE::P_WATER)
			{
				particle->vel += m_gravity * (float)dt * 10;

				particle->pos += particle->vel * (float)dt * 1.f;

				particle->rotation += particle->rotationSpeed * (float)dt;
			}

			if (particle->type == ParticleObject_TYPE::P_NAVIGATION)
			{
				particle->vel += m_gravity * (float)dt * 20;

				particle->pos += particle->vel * (float)dt * 1.f;

				particle->rotation += particle->rotationSpeed * (float)dt;
			}

			if (particle->pos.y < 0)
			{
				particle->active = false;
				m_particleCount--;
			}
		}
	}
}

void SceneText::RenderParticles(ParticleObject *particle)
{
	MS& modelStack = GraphicsManager::GetInstance()->GetModelStack();
	switch (particle->type)
	{
	case ParticleObject_TYPE::P_WATER:
	{
		Mesh* water = MeshBuilder::GetInstance()->GetMesh("water");
		modelStack.PushMatrix();
		modelStack.Translate(particle->pos.x, particle->pos.y, particle->pos.z);
		modelStack.Scale(particle->scale.x / 4, particle->scale.y / 4, particle->scale.z / 4);
		RenderHelper::RenderMesh(water);
		modelStack.PopMatrix();
	}
	break;
	case ParticleObject_TYPE::P_NAVIGATION:
	{
		Mesh* navigation = MeshBuilder::GetInstance()->GetMesh("navigation");
		modelStack.PushMatrix();
		modelStack.Translate(particle->pos.x, particle->pos.y, particle->pos.z);
		modelStack.Rotate(particle->rotation, 0, 0, 1);
		modelStack.Scale(particle->scale.x / 4, particle->scale.y / 4, particle->scale.z / 4);
		RenderHelper::RenderMesh(navigation);
		modelStack.PopMatrix();
	}
	break;

	default:
		break;
	}
}

ParticleObject* SceneText::GetParticle(void)
{
	for (std::vector<ParticleObject *>::iterator it = particleList.begin(); it != particleList.end(); ++it)
	{
		ParticleObject *particle = (ParticleObject *)*it;
		if (!particle->active)
		{
			particle->active = true;
			m_particleCount++;
			return particle;
		}
		ParticleObject *particle2 = (ParticleObject *)*it;
		if (!particle2->active)
		{
			particle2->active = true;
			m_particleCount++;
			return particle2;
		}
	}
	for (unsigned i = 0; i < 10; ++i)
	{
		ParticleObject *particle = new ParticleObject(ParticleObject_TYPE::P_WATER);
		particleList.push_back(particle);
		ParticleObject *particle2 = new ParticleObject(ParticleObject_TYPE::P_NAVIGATION);
		particleList.push_back(particle2);
	}
	ParticleObject *particle = particleList.back();
	particle->active = true;
	m_particleCount++;
	return particle;
}

Vector3 SceneText::GetVerticalPos(int columnInput, int rowInput)
{
	return Vector3((columnInput - 5) * 50.0f, 0.0f, ((rowInput - 6) * -50.0f) - 25.0f);
}

Vector3 SceneText::GetHorizontalPos(int columnInput, int rowInput)
{
	return Vector3(((columnInput - 6) * 50.0f) + 25.0f, 0.0f, (rowInput - 5) * -50.0f);
}

void SceneText::Exit()
{
	CSpatialPartition::GetInstance()->RemoveCamera();
	CSceneGraph::GetInstance()->Destroy();
	// Detach camera from other entities
	GraphicsManager::GetInstance()->DetachCamera();
	playerInfo->DetachCamera();
	if (playerInfo->DropInstance() == false)
	{
#if _DEBUGMODE==1
		cout << "Unable to drop PlayerInfo class" << endl;
#endif
	}
	EntityManager::GetInstance()->ClearEntities();
	// Delete the lights
	GraphicsManager::GetInstance()->RemoveLight("lights[0]");
	GraphicsManager::GetInstance()->RemoveLight("lights[1]");
}