#pragma once
#include "WeaponInfo.h"

class CBlank : public CWeaponInfo
{
public:
	CBlank();
	virtual ~CBlank();
	// Initialise this instance to default values
	void Init(void);
};