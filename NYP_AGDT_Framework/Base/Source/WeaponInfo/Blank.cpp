#include "Blank.h"

CBlank::CBlank()
{
}

CBlank::~CBlank()
{
}

// Initialise this instance to default values
void CBlank::Init(void)
{
	// Call the parent's Init method
	CWeaponInfo::Init();
	// The number of ammunition in a magazine for this weapon
	magRounds = 0;
	// The maximum number of ammunition for this magazine for this weapon
	maxMagRounds = 0;
	// The current total number of rounds currently carried by this player
	totalRounds = 0;
	// The max total number of rounds currently carried by this player
	maxTotalRounds = 0;
	// The time between shots
	timeBetweenShots = 0.0;
	// The elapsed time (between shots)
	elapsedTime = 0.0;
	// Boolean flag to indicate if weapon can fire now
	bFire = false;
	BulletOBJ = "BULLET";
	WeaponType_Enum = BLANK;
	BulletLife = 0.0f;
	BulletSpeed = 0.0f;
}