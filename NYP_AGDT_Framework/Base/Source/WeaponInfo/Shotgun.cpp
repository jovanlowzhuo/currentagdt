#include "Shotgun.h"

CShotgun::CShotgun()
{
}

CShotgun::~CShotgun()
{
}

void CShotgun::Init(void)
{
	// Call the parent's Init method
	CWeaponInfo::Init();

	// The number of ammunition in a magazine for this weapon
	magRounds = 8;
	// The maximum number of ammunition for this magazine for this weapon
	maxMagRounds = 8;
	// The current total number of rounds currently carried by this player
	totalRounds = 32;
	// The max total number of rounds currently carried by this player
	maxTotalRounds = 32;

	// The time between shots
	timeBetweenShots = 2.5;
	// The elapsed time (between shots)
	elapsedTime = 0.0;
	// Boolean flag to indicate if weapon can fire now
	bFire = true;
	BulletOBJ = "BULLET";
	WeaponType_Enum = SPREAD;
	BulletLife = 0.5f;
	BulletSpeed = 500.0f;
}