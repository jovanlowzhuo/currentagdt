#pragma once

#include "Vector3.h"
#include "../Projectile/Grenade.h"
class CPlayerInfo;

class CWeaponInfo
{
public:

	enum WeaponType
	{
		SEMI_AUTO = 0,
		FULL_AUTO = 1,
		BURST = 2,
		SPREAD = 3,
		GRENADE = 4,
		BLANK = 5
	};

	WeaponType WeaponType_Enum;

	CWeaponInfo();
	virtual ~CWeaponInfo();
protected:
	// The number of ammunition in a magazine for this weapon
	int magRounds;
	// The maximum number of ammunition for this magazine for this weapon
	int maxMagRounds;
	// The current total number of rounds currently carried by this player
	int totalRounds;
	// The max total number of rounds currently carried by this player
	int maxTotalRounds;

	// The time between shots in milliseconds
	double timeBetweenShots;
	// The elapsed time (between shots) in milliseconds
	double elapsedTime;
	// Boolean flag to indicate if weapon can fire now
	bool bFire;

	//OBJ to spawn when shooting
	std::string BulletOBJ;
	//Lifetime
	float BulletLife;
	//bullet speed
	float BulletSpeed;

public:
	// Set the number of ammunition in the magazine for this player
	virtual void SetMagRound(const int magRounds);
	// Set the maximum number of ammunition in the magazine for this weapon
	virtual void SetMaxMagRound(const int magRounds);
	// The current total number of rounds currently carried by this player
	virtual void SetTotalRound(const int totalRounds);
	// The max total number of rounds currently carried by this player
	virtual void SetMaxTotalRound(const int maxTotalRounds);

	// Get the number of ammunition in the magazine for this player
	virtual int GetMagRound(void) const;
	// Get the maximum number of ammunition in the magazine for this weapon
	virtual int GetMaxMagRound(void) const;
	// Get the current total number of rounds currently carried by this player
	virtual int GetTotalRound(void) const;
	// Get the max total number of rounds currently carried by this player
	virtual int GetMaxTotalRound(void) const;

	// Set the time between shots
	virtual void SetTimeBetweenShots(const double timeBetweenShots);
	// Set the firing rate in rounds per min
	virtual void SetFiringRate(const int firingRate);
	// Set the firing flag
	virtual void SetCanFire(const bool bFire);

	// Get the time between shots
	virtual double GetTimeBetweenShots(void) const;
	// Get the firing rate
	virtual int GetFiringRate(void) const;

	virtual double GetElapsedTime(void) const;
	// Get the firing flag
	virtual bool GetCanFire(void) const;

	// Initialise this instance to default values
	virtual void Init(void);
	// Update the elapsed time
	void Update(const double dt);
	// Discharge this weapon
	bool Discharge(Vector3 position, Vector3 target, CPlayerInfo* _source);
	bool DischargeGrenade(Vector3 position, Vector3 target, CPlayerInfo * _source);
	bool DischargeSpread(Vector3 position, Vector3 target, CPlayerInfo * _source);
	// Reload this weapon
	void Reload(void);

	void GetClip(void);


	// Add rounds
	void AddRounds(const int newRounds);

	// Print Self
	void PrintSelf(void);

	CWeaponInfo::WeaponType GetWeaponType();

	virtual float MagRatio(CWeaponInfo & weaponData);

	friend std::ostream& operator<< (std::ostream& os, CWeaponInfo& weaponData);
};
