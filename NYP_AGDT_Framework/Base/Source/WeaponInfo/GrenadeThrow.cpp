#include "GrenadeThrow.h"
#include "../Projectile/Grenade.h"

CGrenadeThrow::CGrenadeThrow()
{
}


CGrenadeThrow::~CGrenadeThrow()
{
}

// Initialise this instance to default values
void CGrenadeThrow::Init(void)
{
	// Call the parent's Init method
	CWeaponInfo::Init();

	// The number of ammunition in a magazine for this weapon
	magRounds = 1;
	// The maximum number of ammunition for this magazine for this weapon
	maxMagRounds = 1;
	// The current total number of rounds currently carried by this player
	totalRounds = 100;
	// The max total number of rounds currently carried by this player
	maxTotalRounds = 100;

	// The time between shots
	timeBetweenShots = 3.0;
	// The elapsed time (between shots)
	elapsedTime = 0.0;
	// Boolean flag to indicate if weapon can fire now
	bFire = true;
	BulletOBJ = "Grenade";
	WeaponType_Enum = GRENADE;
	BulletLife = 3.0f;
	BulletSpeed = 500.f;
}