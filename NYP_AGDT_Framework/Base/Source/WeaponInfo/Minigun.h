#pragma once

#include "WeaponInfo.h"

class CMinigun :
	public CWeaponInfo
{
public:
	CMinigun();
	virtual ~CMinigun();

	// Initialise this instance to default values
	void Init(void);
};

