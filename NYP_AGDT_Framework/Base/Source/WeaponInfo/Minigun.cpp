#include "Minigun.h"

CMinigun::CMinigun()
{
}

CMinigun::~CMinigun()
{
}

void CMinigun::Init(void)
{
	// Call the parent's Init method
	CWeaponInfo::Init();

	// The number of ammunition in a magazine for this weapon
	magRounds = 250;
	// The maximum number of ammunition for this magazine for this weapon
	maxMagRounds = 250;
	// The current total number of rounds currently carried by this player
	totalRounds = 1000;
	// The max total number of rounds currently carried by this player
	maxTotalRounds = 1000;

	// The time between shots
	//timeBetweenShots = 0.3333;e
	timeBetweenShots = 0.1;
	// The elapsed time (between shots)
	elapsedTime = 0.0;
	// Boolean flag to indicate if weapon can fire now
	bFire = true;
	BulletOBJ = "BULLET";
	WeaponType_Enum = FULL_AUTO;
	BulletLife = 0.2f;
	BulletSpeed = 500.0f;
}
