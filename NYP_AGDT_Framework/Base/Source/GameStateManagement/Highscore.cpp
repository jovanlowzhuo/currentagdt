#include <iostream>
#include <sstream>
#include "Highscore.h"
#include "GL\glew.h"
#include "../Application.h"
#include "LoadTGA.h"
#include "GraphicsManager.h"
#include "MeshBuilder.h"
#include "../TextEntity.h"
#include "RenderHelper.h"
#include "../SpriteEntity.h"
#include "../EntityManager.h"
#include "KeyboardController.h"
#include "SceneManager.h"
using namespace std;

CHighScore::CHighScore()
{
}

CHighScore::~CHighScore()
{
}

void CHighScore::Init()
{
	//Create and attach the camera to the scene
	camera.Init(Vector3(0, 0, 10), Vector3(0, 0, 0), Vector3(0, 1, 0));
	GraphicsManager::GetInstance()->AttachCamera(&camera);

	//Load all the meshes
	glDisable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	
	//Init the lua state interface to MenuData.lua
	CLuaInterface::GetInstance()->SetLuaFile("Image//MenuData.lua", CLuaInterface::GetInstance()->theLuaState);

	halfWindowWidth = Application::GetInstance().GetWindowWidth() * .5f;
	halfWindowHeight = Application::GetInstance().GetWindowHeight() * .5f;

	MeshBuilder::GetInstance()->GenerateQuad("HIGHSCORE_BACKGROUND", Color(1, 1, 1), 1.f);
	MeshBuilder::GetInstance()->GetMesh("HIGHSCORE_BACKGROUND")->textureID = LoadTGA("Image//Highscore_Menu.tga");
	HighscoreBackground = Create::Sprite2DObject("HIGHSCORE_BACKGROUND", Vector3(halfWindowWidth, halfWindowHeight, 0.f), Vector3((float)Application::GetInstance().GetWindowWidth(), (float)Application::GetInstance().GetWindowHeight(), 0.f), true);

	MeshBuilder::GetInstance()->GenerateQuad("SAVE_BUTTON", Color(1, 1, 1), 1.f);
	MeshBuilder::GetInstance()->GetMesh("SAVE_BUTTON")->textureID = LoadTGA("Image//Save_Button.tga");
	lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "SAVE_BUTTON");
	Create::Sprite2DObject("SAVE_BUTTON", Vector3(halfWindowWidth + (halfWindowWidth * 0.75f), CLuaInterface::GetInstance()->getFieldFloat("posY"), 0.f), Vector3(CLuaInterface::GetInstance()->getFieldFloat("scaleX"), CLuaInterface::GetInstance()->getFieldFloat("scaleY"), 0.f), true);

	MeshBuilder::GetInstance()->GenerateQuad("BUTTON_BORDER", Color(1, 1, 1), 1.f);
	MeshBuilder::GetInstance()->GetMesh("BUTTON_BORDER")->textureID = LoadTGA("Image//Button_Border.tga");
	ButtonBorder = Create::Sprite2DObject("BUTTON_BORDER", Vector3(halfWindowWidth + (halfWindowWidth * 0.75f), CLuaInterface::GetInstance()->getFieldFloat("posY"), 1.f), Vector3(200.f, 75.f, 0.f), true);

	MeshBuilder::GetInstance()->GenerateQuad("RETURN_BUTTON", Color(1, 1, 1), 1.f);
	MeshBuilder::GetInstance()->GetMesh("RETURN_BUTTON")->textureID = LoadTGA("Image//Return_Button.tga");
	lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "RETURN_BUTTON");
	Create::Sprite2DObject("RETURN_BUTTON",Vector3(halfWindowWidth + (halfWindowWidth * 0.75f), CLuaInterface::GetInstance()->getFieldFloat("posY"), 0.f),Vector3(CLuaInterface::GetInstance()->getFieldFloat("scaleX"), CLuaInterface::GetInstance()->getFieldFloat("scaleY"), 0.f), true);

	ScoreIncrease = CLuaInterface::GetInstance()->getCharValue("ScoreIncrease");
	ScoreDecrease = CLuaInterface::GetInstance()->getCharValue("ScoreDecrease");
	MoveUp = CLuaInterface::GetInstance()->getCharValue("MoveUp");
	MoveDown = CLuaInterface::GetInstance()->getCharValue("MoveDown");

	float fontSize = 25.0f;
	float halfFontSize = fontSize * .5f;

	MeshBuilder::GetInstance()->GenerateText("text", 16, 16);
	MeshBuilder::GetInstance()->GetMesh("text")->textureID = LoadTGA("Image//calibri.tga");
	MeshBuilder::GetInstance()->GetMesh("text")->material.kAmbient.Set(1, 0, 0);
	for (int i = 0; i < 10; ++i)
	{
		textObj[i] = Create::Text2DObject("text", Vector3(halfWindowWidth - halfWindowWidth, Application::GetInstance().GetWindowHeight() - 200.f - fontSize * i + halfFontSize, 0.0f), "", Vector3(fontSize, fontSize, fontSize), Color(0.85f, 0.85f, 0.1f), true);
	}

	CLuaInterface::GetInstance()->SetLuaFile("Image//SP3_Highscore.lua", CLuaInterface::GetInstance()->theLuaState);
	string PlayerStats;

	for (size_t i = 0; i < 10; ++i)
	{
		string PlayerNum = "Player0";
		PlayerNum[6] = static_cast<char>(i) + 48;

		string PlayerName = CLuaInterface::GetInstance()->getStringValue(PlayerNum);
		PlayerStats = PlayerName;

		PlayerStats.append("  >>  ");
		PlayerNum.push_back('N');
		unsigned int PlayerScore = CLuaInterface::GetInstance()->getIntValue(PlayerNum);
		PlayerStats.append(to_string(PlayerScore));

		if (PlayerScore <= 0)
		{
			isHighscoreFull = false;
		}

		textObj[i]->SetText(PlayerStats);

		PlayerNameVector.push_back(PlayerName);
		PlayerScoreVector.push_back(PlayerScore);

		PlayerStats.clear();
	}

	textObj[10] = Create::Text2DObject("text", Vector3(halfWindowWidth - halfWindowWidth, (float)Application::GetInstance().GetWindowHeight() - 150.f), "", Vector3(fontSize, fontSize, fontSize), Color(0.85f, 0.85f, 0.1f), true);

	CLuaInterface::GetInstance()->SetLuaFile("Image//MenuData.lua", CLuaInterface::GetInstance()->theLuaState);

	isHighscoreFull = true;
	ScoreToInput = EntityManager::GetInstance()->ScoreInput;
	buttonState = STATE_SAVESCORE;
}

void CHighScore::Update(double dt)
{
	if (KeyboardController::GetInstance()->IsKeyReleased(ScoreIncrease))
	{
		ScoreToInput += 1000;
	}

	if (KeyboardController::GetInstance()->IsKeyReleased(ScoreDecrease))
	{
		if (ScoreToInput <= 0)
			ScoreToInput -= 1000;
	}

	if (KeyboardController::GetInstance()->IsKeyReleased(MoveUp))
	{
		buttonState = static_cast<ButtonState>(buttonState - 1);
		if (buttonState < 0)
		{
			buttonState = static_cast<ButtonState>(STATES_TOTAL - 1);
		}
		ButtonborderPosSnap();
	}

	if (KeyboardController::GetInstance()->IsKeyReleased(MoveDown))
	{
		buttonState = static_cast<ButtonState>(buttonState + 1);
		if (buttonState >= STATES_TOTAL)
		{
			buttonState = static_cast<ButtonState>(0);
		}
		ButtonborderPosSnap();
	}

	if (KeyboardController::GetInstance()->IsKeyReleased(VK_RETURN))
	{
		switch (buttonState)
		{
		case STATE_SAVESCORE:
		{
			if (ScoreToInput == 0)
			{
				break;
			}

			bool EligibleForHighscore = false;
			string tempPlayerName = "Player" + to_string(rand() % 100 + 1);

			//Check if score should be part of highscore
			for (size_t i = 0; i < PlayerNameVector.size(); ++i)
			{
				if (tempPlayerName == PlayerNameVector[i])
				{
					if (ScoreToInput > PlayerScoreVector[i])
					{
						PlayerScoreVector[i] = ScoreToInput;
						EligibleForHighscore = true;
					}
					else
					{
						break;
					}
				}
				else if (ScoreToInput >= PlayerScoreVector[i])
				{
					PlayerScoreVector.insert(PlayerScoreVector.begin() + i, ScoreToInput);
					PlayerNameVector.insert(PlayerNameVector.begin() + i, tempPlayerName);

					PlayerNameVector.pop_back();
					PlayerScoreVector.pop_back();

					EligibleForHighscore = true;
					break;
				}
			}

			if (!EligibleForHighscore)
			{
				break;
			}

			bool overwrite = true;
			//Save into highscore lua
			for (size_t i = 0; i < PlayerNameVector.size(); ++i)
			{
				string PlayerNum = "Player0";
				PlayerNum[6] = static_cast<char>(i) + 48;

				CLuaInterface::GetInstance()->saveStringValue(PlayerNum, PlayerNameVector[i], "SaveHighscore", overwrite);
				PlayerNum.push_back('N');
				overwrite = false;
				CLuaInterface::GetInstance()->saveIntValue(PlayerNum, PlayerScoreVector[i], "SaveHighscore");
			}

			string PlayerStats;
			CLuaInterface::GetInstance()->SetLuaFile("Image//SP3_Highscore.lua", CLuaInterface::GetInstance()->theLuaState);
			PlayerNameVector.clear();
			PlayerScoreVector.clear();

			//Reload the render text
			for (size_t i = 0; i < 10; ++i)
			{
				string PlayerNum = "Player0";
				PlayerNum[6] = static_cast<char>(i) + 48;

				string PlayerName = CLuaInterface::GetInstance()->getStringValue(PlayerNum);
				PlayerStats = PlayerName;

				PlayerStats.append("  >>  ");
				PlayerNum.push_back('N');
				unsigned int PlayerScore = CLuaInterface::GetInstance()->getIntValue(PlayerNum);
				PlayerStats.append(to_string(PlayerScore));

				if (PlayerScore <= 0)
				{
					isHighscoreFull = false;
				}

				textObj[i]->SetText(PlayerStats);

				PlayerNameVector.push_back(PlayerName);
				PlayerScoreVector.push_back(PlayerScore);

				PlayerStats.clear();
			}

			ScoreToInput = 0;
			CLuaInterface::GetInstance()->SetLuaFile("Image//MenuData.lua", CLuaInterface::GetInstance()->theLuaState);
			break;
		}
		case STATE_RETURNMAIN:
		{
			SceneManager::GetInstance()->SetActiveScene("MainMenuState");
			break;
		}
		default:
			break;
		}
	}

	ostringstream ss;
	ss.precision(5);
	ss << "Final Score: " << ScoreToInput;
	textObj[10]->SetText(ss.str());
	textObj[10]->SetColor(Color(1.f, 0.f, 0.f));
}

void CHighScore::Render()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	// Setup 3D pipeline then render 3D
	GraphicsManager::GetInstance()->SetPerspectiveProjection(45.0f, 4.0f / 3.0f, 0.1f, 10000.0f);
	GraphicsManager::GetInstance()->AttachCamera(&camera);

	// Pre RenderMesh
	RenderHelper::PreRenderMesh();
	//Render the required entities
	EntityManager::GetInstance()->Render();
	// Post RenderMesh
	RenderHelper::PostRenderMesh();

	// Setup 2D pipeline then render 2D
	GraphicsManager::GetInstance()->SetOrthographicProjection(0, Application::GetInstance().GetWindowWidth(), 0, Application::GetInstance().GetWindowHeight(), -10, 10);
	GraphicsManager::GetInstance()->DetachCamera();

	// Pre RenderText
	RenderHelper::PreRenderText();
	//Render the required entites
	EntityManager::GetInstance()->RenderUI();
	// Post RenderText
	RenderHelper::PostRenderText();
}

void CHighScore::Exit()
{
	//Remove the entity from EntitManager
	EntityManager::GetInstance()->RemoveEntity(HighscoreBackground);
	EntityManager::GetInstance()->RemoveEntity(ButtonBorder);
	//Remove the meshes which are specific to CIntroState
	MeshBuilder::GetInstance()->RemoveMesh("HIGHSCORE_BACKGROUND");
	MeshBuilder::GetInstance()->RemoveMesh("SAVE_BUTTON");
	MeshBuilder::GetInstance()->RemoveMesh("BUTTON_BORDER");
	MeshBuilder::GetInstance()->RemoveMesh("RETURN_BUTTON");
	MeshBuilder::GetInstance()->RemoveMesh("text");
	for (size_t i = 0; i < 11; ++i)
	{
		delete textObj[i];
		textObj[i] = nullptr;
	}
	PlayerNameVector.clear();
	PlayerScoreVector.clear();
	// Detach camera from other entities
	GraphicsManager::GetInstance()->DetachCamera();	
	glEnable(GL_DEPTH_TEST);
}

void CHighScore::ButtonborderPosSnap()
{
	//Set button border position
	switch (buttonState)
	{
	case STATE_SAVESCORE:
	{
		lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "SAVE_BUTTON");
		ButtonBorder->SetPosition(Vector3(halfWindowWidth + (halfWindowWidth * 0.75f), CLuaInterface::GetInstance()->getFieldFloat("posY"), 1.f));
		break;
	}
	case STATE_RETURNMAIN:
	{
		lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "RETURN_BUTTON");
		ButtonBorder->SetPosition(Vector3(halfWindowWidth + (halfWindowWidth * 0.75f), CLuaInterface::GetInstance()->getFieldFloat("posY"), 1.f));
		break;
	}

	default:
		break;
	}
}