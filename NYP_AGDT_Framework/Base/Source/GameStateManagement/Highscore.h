#ifndef HIGHSCORE_H
#define HIGHSCORE_H

#include "Scene.h"
#include "Mtx44.h"
#include "Mesh.h"
#include "MatrixStack.h"
#include "../SpriteEntity.h"
#include "../FPSCamera.h"
#include "RenderHelper.h"
#include "../Lua/LuaInterface.h"
#include <vector>

class SceneManager;
class TextEntity;
class CHighScore :public Scene
{
public:
	CHighScore();
	~CHighScore();

	virtual void Init();
	virtual void Update(double dt);
	virtual void Render();
	virtual void Exit();

	unsigned int ScoreToInput;

	std::vector<unsigned int> PlayerScoreVector;
	std::vector<std::string> PlayerNameVector;

private:
	FPSCamera camera;
	SpriteEntity *HighscoreBackground;
	SpriteEntity *ButtonBorder;
	TextEntity* textObj[11];

	float halfWindowWidth;
	float halfWindowHeight;
	char ScoreIncrease, ScoreDecrease;
	char MoveUp, MoveDown;
	bool isHighscoreFull;

	void ButtonborderPosSnap();

	enum ButtonState
	{
		STATE_SAVESCORE,
		STATE_RETURNMAIN,

		STATES_TOTAL
	};
	ButtonState buttonState;
};
#endif