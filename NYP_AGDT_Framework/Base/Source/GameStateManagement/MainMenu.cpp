#include "MainMenu.h"
#include "KeyboardController.h"
#include "SceneManager.h"
#include "MeshBuilder.h"
#include "LoadTGA.h"
#include "../Application.h"
#include "GraphicsManager.h"
#include "EntityBase.h"
#include "../EntityManager.h"
#include "GL\glew.h"

CMainMenuState::CMainMenuState()
{
}

CMainMenuState::~CMainMenuState()
{
}

void CMainMenuState::Init()
{
	camera.Init(Vector3(0, 0, 10), Vector3(0, 0, 0), Vector3(0, 1, 0));
	GraphicsManager::GetInstance()->AttachCamera(&camera);

	glDisable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	
	//Init the lua state interface to MenuData.lua
	CLuaInterface::GetInstance()->SetLuaFile("Image//MenuData.lua", CLuaInterface::GetInstance()->theLuaState);

	halfWindowWidth = Application::GetInstance().GetWindowWidth() / 2.0f;
	halfWindowHeight = Application::GetInstance().GetWindowHeight() / 2.0f;

	MeshBuilder::GetInstance()->GenerateQuad("MENU_BACKGROUND", Color(1, 1, 1), 1.f);
	MeshBuilder::GetInstance()->GetMesh("MENU_BACKGROUND")->textureID = LoadTGA("Image//Main_Menu.tga");
	MenuStateBackground = Create::Sprite2DObject("MENU_BACKGROUND", Vector3(halfWindowWidth, halfWindowHeight, 0.f), Vector3((float)Application::GetInstance().GetWindowWidth(), (float)Application::GetInstance().GetWindowHeight(), 0.f), true);
	 
	MeshBuilder::GetInstance()->GenerateQuad("STATE_PLAY", Color(1, 1, 1), 0.5f);
	MeshBuilder::GetInstance()->GetMesh("STATE_PLAY")->textureID = LoadTGA("Image//Play_Button.tga");
	lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "STATE_PLAY");
	Create::Sprite2DObject("STATE_PLAY", Vector3(halfWindowWidth, CLuaInterface::GetInstance()->getFieldFloat("posY"), 1.f), Vector3(CLuaInterface::GetInstance()->getFieldFloat("scaleX"), CLuaInterface::GetInstance()->getFieldFloat("scaleY"), 0.f), true);

	MeshBuilder::GetInstance()->GenerateQuad("BUTTON_BORDER", Color(1, 1, 1), 0.5f);
	MeshBuilder::GetInstance()->GetMesh("BUTTON_BORDER")->textureID = LoadTGA("Image//Button_Border.tga");
	ButtonBorder = Create::Sprite2DObject("BUTTON_BORDER", Vector3(halfWindowWidth, CLuaInterface::GetInstance()->getFieldFloat("posY"), 1.f), Vector3(200.f, 75.f, 0.f), true);

	MeshBuilder::GetInstance()->GenerateQuad("STATE_HIGHSCORE", Color(1, 1, 1), 0.5f);
	MeshBuilder::GetInstance()->GetMesh("STATE_HIGHSCORE")->textureID = LoadTGA("Image//Highscore_Button.tga");
	lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "STATE_HIGHSCORE");
	Create::Sprite2DObject("STATE_HIGHSCORE", Vector3(halfWindowWidth, CLuaInterface::GetInstance()->getFieldFloat("posY"), 1.f), Vector3(CLuaInterface::GetInstance()->getFieldFloat("scaleX"), CLuaInterface::GetInstance()->getFieldFloat("scaleY"), 0.f), true);

	MeshBuilder::GetInstance()->GenerateQuad("STATE_EXIT", Color(1, 1, 1), 0.5f);
	MeshBuilder::GetInstance()->GetMesh("STATE_EXIT")->textureID = LoadTGA("Image//Exit_Button.tga");
	lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "STATE_EXIT");
	Create::Sprite2DObject("STATE_EXIT", Vector3(halfWindowWidth, CLuaInterface::GetInstance()->getFieldFloat("posY"), 1.f), Vector3(CLuaInterface::GetInstance()->getFieldFloat("scaleX"), CLuaInterface::GetInstance()->getFieldFloat("scaleY"), 0.f), true);
	
	MoveUp = CLuaInterface::GetInstance()->getCharValue("MoveUp");
	MoveDown = CLuaInterface::GetInstance()->getCharValue("MoveDown");

	buttonState = STATE_PLAY;
}

void CMainMenuState::Update(double dt)
{
	if (KeyboardController::GetInstance()->IsKeyReleased(MoveUp))
	{
		buttonState = static_cast<ButtonState>(buttonState - 1);
		if (buttonState < 0)
		{
			buttonState = static_cast<ButtonState>(STATE_TOTAL - 1);
		}

		switch (buttonState)
		{
		case STATE_PLAY:
		{
			lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "STATE_PLAY");
			ButtonBorder->SetPosition(Vector3(halfWindowWidth, CLuaInterface::GetInstance()->getFieldFloat("posY"), 1.f));
			break;
		}
		case STATE_HIGHSCORE:
		{
			lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "STATE_HIGHSCORE");
			ButtonBorder->SetPosition(Vector3(halfWindowWidth, CLuaInterface::GetInstance()->getFieldFloat("posY"), 1.f));
			break;
		}
		case STATE_EXIT:
		{
			lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "STATE_EXIT");
			ButtonBorder->SetPosition(Vector3(halfWindowWidth, CLuaInterface::GetInstance()->getFieldFloat("posY"), 1.f));
			break;
		}
		default:
			break;
		}
	}

	if (KeyboardController::GetInstance()->IsKeyReleased(MoveDown))
	{
		buttonState = static_cast<ButtonState>(buttonState + 1);
		if (buttonState >= STATE_TOTAL)
		{
			buttonState = static_cast<ButtonState>(0);
		}

		switch (buttonState)
		{
		case STATE_PLAY:
		{
			lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "STATE_PLAY");
			ButtonBorder->SetPosition(Vector3(halfWindowWidth, CLuaInterface::GetInstance()->getFieldFloat("posY"), 1.f));
			break;
		}
		case STATE_HIGHSCORE:
		{
			lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "STATE_HIGHSCORE");
			ButtonBorder->SetPosition(Vector3(halfWindowWidth, CLuaInterface::GetInstance()->getFieldFloat("posY"), 1.f));
			break;
		}
		case STATE_EXIT:
		{
			lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "STATE_EXIT");
			ButtonBorder->SetPosition(Vector3(halfWindowWidth, CLuaInterface::GetInstance()->getFieldFloat("posY"), 1.f));
			break;
		}
		default:
			break;
		}
	}

	if (KeyboardController::GetInstance()->IsKeyReleased(VK_RETURN))
	{
		switch (buttonState)
		{
		case STATE_PLAY:
		{
			SceneManager::GetInstance()->SetActiveScene("GameState");
			break;
		}
		case STATE_HIGHSCORE:
		{
			SceneManager::GetInstance()->SetActiveScene("HighScoreState");
			break;
		}
		case STATE_EXIT:
		{
			Application::GetInstance().KillGame();
			break;
		}
		default:
			break;
		}
	}
}

void CMainMenuState::Render()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	//Setup 3D pipeline then render 3D 
	GraphicsManager::GetInstance()->SetPerspectiveProjection(45.f, 4.f / 3.f, 0.1f, 10000.f);
	GraphicsManager::GetInstance()->AttachCamera(&camera);

	// Pre RenderMesh
	RenderHelper::PreRenderMesh();
	//Render the required entities
	EntityManager::GetInstance()->Render();
	// Post RenderMesh
	RenderHelper::PostRenderMesh();

	//Setup 2D pipeline then render 2D
	GraphicsManager::GetInstance()->SetOrthographicProjection(0, Application::GetInstance().GetWindowWidth(), 0, Application::GetInstance().GetWindowHeight(), -10, 10);
	GraphicsManager::GetInstance()->DetachCamera();

	// Pre RenderText
	RenderHelper::PreRenderText();
	//Render the required entites
	EntityManager::GetInstance()->RenderUI();
	// Post RenderText
	RenderHelper::PostRenderText();
}

void CMainMenuState::Exit()
{
	//EntityManager::
	EntityManager::GetInstance()->RemoveEntity(MenuStateBackground);
	EntityManager::GetInstance()->RemoveEntity(ButtonBorder);
	// Remove the meshes which are specific to CMenuState
	MeshBuilder::GetInstance()->RemoveMesh("MENU_BACKGROUND");
	MeshBuilder::GetInstance()->RemoveMesh("STATE_PLAY");
	MeshBuilder::GetInstance()->RemoveMesh("BUTTON_BORDER");
	MeshBuilder::GetInstance()->RemoveMesh("STATE_HIGHSCORE");
	MeshBuilder::GetInstance()->RemoveMesh("STATE_EXIT");
	// Detech camera from other entites
	GraphicsManager::GetInstance()->DetachCamera();
	glEnable(GL_DEPTH_TEST);
}