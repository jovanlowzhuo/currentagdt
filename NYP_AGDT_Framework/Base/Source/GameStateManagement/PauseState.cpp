#include "PauseState.h"
#include "KeyboardController.h"
#include "SceneManager.h"
#include "MeshBuilder.h"
#include "LoadTGA.h"
#include "../Application.h"
#include "GraphicsManager.h"
#include "EntityBase.h"
#include "../EntityManager.h"
#include "GL\glew.h"

CPauseState::CPauseState()
{
}

CPauseState::~CPauseState()
{
}

void CPauseState::Init()
{
	camera.Init(Vector3(0, 0, 10), Vector3(0, 0, 0), Vector3(0, 1, 0));
	GraphicsManager::GetInstance()->AttachCamera(&camera);

	glDisable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);

	//Init the lua state interface to MenuData.lua
	CLuaInterface::GetInstance()->SetLuaFile("Image//MenuData.lua", CLuaInterface::GetInstance()->theLuaState);

	halfWindowWidth = Application::GetInstance().GetWindowWidth() / 2.0f;
	halfWindowHeight = Application::GetInstance().GetWindowHeight() / 2.0f;

	MeshBuilder::GetInstance()->GenerateQuad("PAUSE_BACKGROUND", Color(1, 1, 1), 1.f);
	MeshBuilder::GetInstance()->GetMesh("PAUSE_BACKGROUND")->textureID = LoadTGA("Image//Pause_Menu.tga");
	PauseStateBackground = Create::Sprite2DObject("PAUSE_BACKGROUND", Vector3(halfWindowWidth, halfWindowHeight, 0.f), Vector3((float)Application::GetInstance().GetWindowWidth(), (float)Application::GetInstance().GetWindowHeight(), 0.f), true);
 
	MeshBuilder::GetInstance()->GenerateQuad("RESUME_BUTTON", Color(1, 1, 1), 1.f);
	MeshBuilder::GetInstance()->GetMesh("RESUME_BUTTON")->textureID = LoadTGA("Image//Resume_Button.tga");
	lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "RESUME_BUTTON");
	Create::Sprite2DObject("RESUME_BUTTON", Vector3(halfWindowWidth, CLuaInterface::GetInstance()->getFieldFloat("posY"), 0.f), Vector3(CLuaInterface::GetInstance()->getFieldFloat("scaleX"), CLuaInterface::GetInstance()->getFieldFloat("scaleY"), 0.f), true);

	MeshBuilder::GetInstance()->GenerateQuad("BUTTON_BORDER", Color(1, 1, 1), 1.f);
	MeshBuilder::GetInstance()->GetMesh("BUTTON_BORDER")->textureID = LoadTGA("Image//Button_Border.tga");
	ButtonBorder = Create::Sprite2DObject("BUTTON_BORDER", Vector3(halfWindowWidth, CLuaInterface::GetInstance()->getFieldFloat("posY"), 1.f), Vector3(200.f, 75.f, 0.f), true);

	MeshBuilder::GetInstance()->GenerateQuad("QUIT_BUTTON", Color(1, 1, 1), 1.f);
	MeshBuilder::GetInstance()->GetMesh("QUIT_BUTTON")->textureID = LoadTGA("Image//Quit_Button.tga");
	lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "QUIT_BUTTON");
	Create::Sprite2DObject("QUIT_BUTTON", Vector3(halfWindowWidth, CLuaInterface::GetInstance()->getFieldFloat("posY"), 0.f), Vector3(CLuaInterface::GetInstance()->getFieldFloat("scaleX"), CLuaInterface::GetInstance()->getFieldFloat("scaleY"), 0.f), true);

	MoveUp = CLuaInterface::GetInstance()->getCharValue("MoveUp");
	MoveDown = CLuaInterface::GetInstance()->getCharValue("MoveDown");

	buttonState = RESUME;
}

void CPauseState::Update(double dt)
{
	if (KeyboardController::GetInstance()->IsKeyReleased(MoveUp))
	{
		buttonState = static_cast<ButtonState>(buttonState - 1);
		if (buttonState < 0)
		{
			buttonState = static_cast<ButtonState>(STATES_TOTAL - 1);
		}
		ButtonborderPosSnap();
	}

	if (KeyboardController::GetInstance()->IsKeyReleased(MoveDown))
	{
		buttonState = static_cast<ButtonState>(buttonState + 1);
		if (buttonState >= STATES_TOTAL)
		{
			buttonState = static_cast<ButtonState>(0);
		}
		ButtonborderPosSnap();
	}

	switch (buttonState)
	{
	case RESUME:
		if (KeyboardController::GetInstance()->IsKeyReleased(VK_RETURN))
		{
			SceneManager::GetInstance()->SetActiveScene("GameState");
		}
		break;

	case QUIT_MAIN:
		if (KeyboardController::GetInstance()->IsKeyReleased(VK_RETURN))
		{
			SceneManager::GetInstance()->SetActiveScene("MainMenuState");
		}
	default:
		break;
	}
}

void CPauseState::Render()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	// Setup 3D pipeline then render 3D
	GraphicsManager::GetInstance()->SetPerspectiveProjection(45.0f, 4.0f / 3.0f, 0.1f, 10000.0f);
	GraphicsManager::GetInstance()->AttachCamera(&camera);

	// Pre RenderMesh
	RenderHelper::PreRenderMesh();
	//Render the required entities
	EntityManager::GetInstance()->Render();
	// Post RenderMesh
	RenderHelper::PostRenderMesh();

	// Setup 2D pipeline then render 2D
	GraphicsManager::GetInstance()->SetOrthographicProjection(0, Application::GetInstance().GetWindowWidth(), 0, Application::GetInstance().GetWindowHeight(), -10, 10);
	GraphicsManager::GetInstance()->DetachCamera();
	// Pre RenderText
	RenderHelper::PreRenderText();
	//Render the required entites
	EntityManager::GetInstance()->RenderUI();
	// Post RenderText
	RenderHelper::PostRenderText();
}

void CPauseState::Exit()
{
	//EntityManager::
	EntityManager::GetInstance()->RemoveEntity(PauseStateBackground);
	EntityManager::GetInstance()->RemoveEntity(ButtonBorder);
	// Remove the meshes which are specific to CMenuState
	MeshBuilder::GetInstance()->RemoveMesh("PAUSE_BACKGROUND");
	MeshBuilder::GetInstance()->RemoveMesh("BUTTON_BORDER");
	MeshBuilder::GetInstance()->RemoveMesh("QUIT_BUTTON");
	MeshBuilder::GetInstance()->RemoveMesh("RESUME_BUTTON");
	// Detech camera from other entites
	GraphicsManager::GetInstance()->DetachCamera();
	glEnable(GL_DEPTH_TEST);
}

void CPauseState::ButtonborderPosSnap()
{
	//Set button border position
	switch (buttonState)
	{
	case RESUME:
	{
		lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "RESUME_BUTTON");
		ButtonBorder->SetPosition(Vector3(halfWindowWidth, CLuaInterface::GetInstance()->getFieldFloat("posY"), 1.f));
		break;
	}
	case QUIT_MAIN:
	{
		lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "QUIT_BUTTON");
		ButtonBorder->SetPosition(Vector3(halfWindowWidth, CLuaInterface::GetInstance()->getFieldFloat("posY"), 1.f));
		break;
	}
	default:
		break;
	}
}