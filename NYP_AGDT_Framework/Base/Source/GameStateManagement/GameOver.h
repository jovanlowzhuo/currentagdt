#ifndef GAME_OVER_H
#define GAME_OVER_H

#include "Scene.h"
#include "Mtx44.h"
#include "Mesh.h"
#include "MatrixStack.h"
#include "../SpriteEntity.h"
#include "../FPSCamera.h"
#include "RenderHelper.h"
#include "../Lua/LuaInterface.h"

class SceneManager;
class CGameOverState : public Scene
{
public:
	CGameOverState();
	~CGameOverState();

	virtual void Init();
	virtual void Update(double dt);
	virtual void Render();
	virtual void Exit();

private:
	FPSCamera camera;
	SpriteEntity* GameoverStateBackground;
	SpriteEntity* ButtonBorder;

	float halfWindowWidth;
	float halfWindowHeight;
	char MoveUp, MoveDown;

	enum ButtonState
	{
		STATE_MENU,
		STATE_QUIT,
		STATE_TOTAL
	};
	ButtonState buttonState;
};
#endif