#ifndef PAUSE_H
#define PAUSE_H

#include "Scene.h"
#include "Scene.h"
#include "Mtx44.h"
#include "Mesh.h"
#include "MatrixStack.h"
#include "../SpriteEntity.h"
#include "../FPSCamera.h"
#include "RenderHelper.h"
#include "../PlayerInfo/PlayerInfo.h"

class SceneManager;
class CPauseState : public Scene
{
public:
	CPauseState();
	~CPauseState();

	virtual void Init();
	virtual void Update(double dt);
	virtual void Render();
	virtual void Exit();

private:
	FPSCamera camera;
	SpriteEntity* ButtonBorder;
	SpriteEntity* PauseStateBackground;
	CPlayerInfo* playerInfo;

	float halfWindowWidth;
	float halfWindowHeight;
	char MoveUp, MoveDown;

	void ButtonborderPosSnap();

	enum ButtonState
	{
		RESUME,
		QUIT_MAIN,
		STATES_TOTAL
	};
	ButtonState buttonState;
};
#endif