#ifndef MAIN_MENU_H
#define MAIN_MENU_H

#include "Scene.h"
#include "Mtx44.h"
#include "Mesh.h"
#include "MatrixStack.h"
#include "../SpriteEntity.h"
#include "../FPSCamera.h"
#include "RenderHelper.h"
#include "../Lua/LuaInterface.h"

class SceneManager;
class CMainMenuState : public Scene
{
public:
	CMainMenuState();
	~CMainMenuState();

	virtual void Init();
	virtual void Update(double dt);
	virtual void Render();
	virtual void Exit();

private:
	FPSCamera camera;
	SpriteEntity* MenuStateBackground;
	SpriteEntity* ButtonBorder;

	float halfWindowWidth;
	float halfWindowHeight;
	char MoveUp, MoveDown;

	enum ButtonState
	{
		STATE_PLAY,
		STATE_HIGHSCORE,
		STATE_EXIT,
		STATE_TOTAL
	};
	ButtonState buttonState;
};
#endif