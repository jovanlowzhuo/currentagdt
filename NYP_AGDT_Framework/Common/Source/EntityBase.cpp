#include "EntityBase.h"

EntityBase::EntityBase()
	: position(0.0f, 0.0f, 0.0f)
	, scale(1.0f, 1.0f, 1.0f)
	, isDone(false)
	, m_bCollider(false)
	, updateBool(true)
	, AssociatedTrigger(NULL)
	, directionVector(Vector3(0, 0, 0))
	, LastFrameUpdate(false)
	, MidFrameUpdate(0.0)
	, DoNotRender(false)
{
}

EntityBase::~EntityBase()
{
}

void EntityBase::Update(double _dt)
{
}

void EntityBase::Render()
{
}

void EntityBase::RenderUI()
{
}

bool EntityBase::IsDone()
{
	return isDone;
}

void EntityBase::SetIsDone(bool _value)
{
	isDone = _value;
}

void EntityBase::SetDirectionSpeed(const Vector3 & _directionValue, const float speedValue)
{
	directionVector = _directionValue;
	speedMultiplier = speedValue;
}

// Check if this entity has a collider class parent
bool EntityBase::HasCollider(void) const
{
	return m_bCollider;
}

// Set the flag to indicate if this entity has a collider class parent
void EntityBase::SetCollider(const bool _value)
{
	m_bCollider = _value;
}

bool EntityBase::IsLastFrame()
{
	return LastFrameUpdate;
}

void EntityBase::SetLastFrame(const bool _value)
{
	LastFrameUpdate = _value;
}

double EntityBase::GetMidFrame()
{
	return MidFrameUpdate;
}

void EntityBase::SetMidFrame(double midDouble)
{
	MidFrameUpdate = midDouble;
}

bool EntityBase::GetUpdate()
{
	return updateBool;
}

void EntityBase::SetUpdate(bool _value)
{
	updateBool = _value;
}

EntityBase::EntityType EntityBase::getEntityType()
{
	return EntityTypeEnum;
}