#ifndef ENTITY_BASE_H
#define ENTITY_BASE_H

#include "Vector3.h"
#include "EventTrigger.h"

class EntityBase
{
public:
	enum EntityType
	{
		GROUND_TYPE,		//Disregard atm
		SKYBOX_TYPE,		//Disregard atm
		PLAYER_TYPE,
		ENEMY_TYPE,			//With Projectile
		FURNITURE_TYPE,		//With Enemy,Projectile
		EVENT_TYPE,
		
		PLAYER_PROJECTILE_TYPE,
		ENEMY_PROJECTILE_TYPE,
		GRENADE_TYPE,
		TEXT_TYPE,
		ENTITY_TYPE,
		ASSET_TYPE,
		GENERIC_TYPE
	};
	EntityType EntityTypeEnum;

	CEventTrigger* AssociatedTrigger;

	EntityBase();
	virtual ~EntityBase();

	virtual void Update(double _dt);
	virtual void Render();
	virtual void RenderUI();

	inline void SetPosition(const Vector3& _value){ position = _value; };
	inline Vector3 GetPosition(){ return position; };

	inline void SetScale(const Vector3& _value){ scale = _value; };
	inline Vector3 GetScale(){ return scale; };
	bool DoNotRender;
	bool IsDone();
	void SetIsDone(const bool _value);

	void SetDirectionSpeed(const Vector3& _directionValue, const float speedValue = 1.0f);
	Vector3 GetDirection() { return directionVector; };
	float GetSpeed() { return speedMultiplier; };

	// Check if this entity has a collider class parent
	virtual bool HasCollider(void) const;
	// Set the flag to indicate if this entity has a collider class parent
	virtual void SetCollider(const bool _value);

	bool IsLastFrame();
	void SetLastFrame(const bool _value);

	double GetMidFrame();
	void SetMidFrame(double midDouble);

	bool GetUpdate();
	void SetUpdate(bool _value);



	EntityBase::EntityType getEntityType();

protected:
	Vector3 position;
	Vector3 scale;
	Vector3 directionVector;
	float speedMultiplier;

	bool isDone;
	bool m_bCollider;
	bool updateBool;
	bool LastFrameUpdate;
	double MidFrameUpdate;
};
#endif // ENTITY_BASE_H